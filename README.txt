Visual Studio 2013 Setup instructions:

This guide assumes a 64-bit system. If you're still using a 32-bit system, it's
time to visit Canada Computers :)

1) Install the pre-requisites to a common location with no spaces in the path: 

1.1) OpenCV 2.4.9 - download from http://opencv.org/downloads.html. Install and
add OPENCV_DIR=<installation dir>/build and PATH=%OPENCV_DIR%/bin;%PATH% to the 
system path.

1.2) Download Qt from https://download.qt.io/official_releases/online_installers/qt-unified-windows-x86-online.exe.mirrorlist
(e.g. qt-unified-windows-x86-2.0.X-online). Install to common location. Installer takes care of everything. Set
the QTDIR environment variable (eg. QTDIR = <installation
dir>\qt-5.3.1-x64-msvc2012-opengl)

1.3) Qt Visual Studio Plugin - download
from http://download.qt-project.org/official_releases/vsaddin/qt-vs-addin-1.2.3-opensource.exe
under the Other Downloads section at the bottom. Install to common location again. Start VS and configure Qt by going to
the QT5->Options menu and pointing it to the location where the library was
installed in step 1.2.

1.4) Open Visual Studio 2013 and go to QT5->Qt Options.
Add msvc2013_64 (Path=$QT_ROOTDIR\5.7\msvc2013_64). 
Confirm the solution uses the correct version of Qt
(right click on solution and choose "Change Solution's Qt Version").

1.5) Download pre-built Boost Library v1.55 from:
http://sourceforge.net/projects/boost/files/boost-binaries/1.55.0/
(boost_1_55_0-msvc-12.0-64.exe):
Set BOOST_DIR=<boost unzip directory>.

Alternately, download sources from:
http://www.boost.org/users/history/version_1_55_0.html and unzip to common
location. Go to $BOOST_DIR and enter at command
prompt: bjam --toolset=msvc-12.0 address-model=64 --build-type=complete 
 
1.6) Download and install NVidia Cuda 7.5 library from
https://developer.nvidia.com/cuda-75-downloads-archive
and Visual Studio Plugin - from NVidia site, straightforward install to common location.

1.7) Download PointGrey FlyCapture 2 Viewer and SDK 64-bit. Install both to
common location. Set $FLYCAP_DIR=<installation root folder>.

1.8) OpenSSL library v1.0.2 - The precompiled OpenSSL v1.0.2 was found on:
https://slproweb.com/products/Win32OpenSSL.html. Download "Win64 OpenSSL v1.0.2j"
from the website and link it against VS. Alternatively, go to https://www.openssl.org/
and grab the source of the same version. A good reference for compiling OpenSSL can be
found here: http://developer.covenanteyes.com/building-openssl-for-visual-studio/
Add OPENSSL_DIR="directory of installed OpenSSL" to system's environment variable.

2) Clone this repo.

3) Open the HDR solution in VS. All projects should link to libraries relative
to the environment variables set during installation steps.

4) Press F7 to build solution.

5) To run the console app, make sure to right click on hdr-kernel-console and
select "Set as Startup Project"

NOTE: If program fails to run due to GPUAssert error, then double check the compute
capability here https://developer.nvidia.com/cuda-gpus, and update the project
property settings of hdr-kernel under CUDA C/C++->Device->Code Generation

NOTE: Confirm you have set these environment variables - OPENCV_DIR, BOOST_DIR,
QTDIR, FLYCAP_DIR; they are used in the Visual Studio Project.