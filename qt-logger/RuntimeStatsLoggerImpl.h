#pragma once

#include <QTimer>
#include <QDateTime>

#include "windows.h"
#include "psapi.h"

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class RuntimeStatsLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    RuntimeStatsLoggerImpl(const QString, QObject * parent = nullptr);
    ~RuntimeStatsLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private slots:
    void m_log();

private:
    QList<LogMessage> m_messages;
    QString m_pathToLog;

    QTimer * m_timer;

    QsLoggerImpl * m_logger;
};
