#pragma once

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class InfoLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    InfoLoggerImpl(const QString, QObject * parent = nullptr);
    ~InfoLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private:
    QString m_pathToLog;

    QsLoggerImpl * m_logger;
};
