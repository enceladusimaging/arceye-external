#include "ErrorLoggerImpl.h"

ErrorLoggerImpl::ErrorLoggerImpl(const QString pathToLog, QObject *parent)
    : QObject(parent)
    , m_pathToLog(pathToLog)
    ,m_logger(new QsLoggerImpl(pathToLog, this))
{
    connect(this, SIGNAL(logLevelChanged(ILogger::LogLevel)),
                         static_cast<QsLoggerImpl *>(m_logger), SLOT(changeLogLevel(ILogger::LogLevel)));

    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + START_MESSAGE, LogMessage::Type::Info));
    setLogLevel(ILogger::LogLevel::Error);

}

ErrorLoggerImpl::~ErrorLoggerImpl()
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + END_MESSAGE, LogMessage::Type::Info));
}

void ErrorLoggerImpl::log(const LogMessage & msg)
{
    if (msg.type() == LogMessage::Type::Error) {
        m_logger->log(msg);
    }
}

void ErrorLoggerImpl::setLogLevel(ILogger::LogLevel level)
{
    m_logLevel = level;
    emit logLevelChanged(level);
}

QString ErrorLoggerImpl::type()
{
    return QString("ErrorLoggerImpl");
}
