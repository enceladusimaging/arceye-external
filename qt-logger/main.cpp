#include "QsLog/QsLog.h"

#include "tests/tst_LogService.h"
#include "tests/tst_QsLoggerImpl.h"
#include "tests/tst_LatencyLoggerImpl.h"
#include "tests/tst_SystemInfoLoggerImpl.h"
#include "tests/tst_RuntimeStatsLoggerImpl.h"
#include "tests/tst_ErrorLoggerImpl.h"
#include "tests/tst_WarningLoggerImpl.h"
#include "tests/tst_TraceLoggerImpl.h"
#include "tests/tst_InfoLoggerImpl.h"

#include <QApplication>
#include <QtTest/QTest>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    tst_LogService tst_logService;
    QTest::qExec(&tst_logService);

    tst_QsLoggerImpl tst_QsLoggerImpl;
    QTest::qExec(&tst_QsLoggerImpl);

    tst_InfoLoggerImpl tst_InfoLoggerImpl;
    QTest::qExec(&tst_InfoLoggerImpl);

    tst_LatencyLoggerImpl tst_LatencyLoggerImpl;
    QTest::qExec(&tst_LatencyLoggerImpl);

    tst_RuntimeStatsLoggerImpl tst_RuntimeStatsLoggerImpl;
    QTest::qExec(&tst_RuntimeStatsLoggerImpl);

    tst_SystemInfoLoggerImpl tst_SystemInfoLoggerImpl;
    QTest::qExec(&tst_SystemInfoLoggerImpl);

    tst_ErrorLoggerImpl tst_ErrorLoggerImpl;
    QTest::qExec(&tst_ErrorLoggerImpl);

    tst_WarningLoggerImpl tst_WarningLoggerImpl;
    QTest::qExec(&tst_WarningLoggerImpl);

    tst_TraceLoggerImpl tst_TraceLoggerImpl;
    QTest::qExec(&tst_TraceLoggerImpl);

    return 0;
}
