#pragma once

#include "qtlogger_global.h"

#include <iostream>

#include <QFile>
#include <QDir>

#include "QsLog/QsLog.h"
#include "QsLog/QsLogDest.h"

#include "ILogger.h"

class QsLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
     QsLoggerImpl(const QString pathToLog, QObject * parent = nullptr);
    ~QsLoggerImpl();

    void log(const LogMessage &);

    QString type();

public slots:
    void changeLogLevel(ILogger::LogLevel);

private:
    static QsLogging::MaxSizeBytes m_logFileByteSize;
    static QsLogging::MaxOldLogCount m_oldLogsToKeep;

    void m_setLogFilePath(const QString);
    QString m_logFilePath() const;
    void m_setLogLevelForQsLog();

    static QList<QString> * m_files;

    QsLogging::Logger & m_logger;
    QString m_pathToLog;
};
