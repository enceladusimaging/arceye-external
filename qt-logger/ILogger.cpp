#include "ILogger.h"

void ILogger::setLogLevel(ILogger::LogLevel level)
{
    m_logLevel = level;
}

ILogger::LogLevel ILogger::logLevel()
{
    return m_logLevel;
}

void ILogger::setInterval(int msec)
{
    m_interval = msec;
}

int ILogger::interval()
{
   return m_interval;
}

const ILogger::LogLevel ILogger::OFF(ILogger::LogLevel::Off);

const QString ILogger::START_MESSAGE(QString(": log start"));

const QString ILogger::END_MESSAGE(QString(": log end"));
