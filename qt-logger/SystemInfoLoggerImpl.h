#pragma once

#include <QTimer>
#include <QDateTime>

#include "windows.h"
#include "psapi.h"
#include <math.h>

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class SystemInfoLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    SystemInfoLoggerImpl(const QString, QObject * parent = nullptr);
    ~SystemInfoLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private slots:
    void m_log();

private:

    QString m_ramUsage();
    QString m_cpuUsage();
    QString m_hardDriveUsage();

    QList<LogMessage> m_messages;
    QString m_pathToLog;

    QTimer * m_timer;

    QsLoggerImpl * m_logger;
};
