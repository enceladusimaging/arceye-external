#include "LogService.h"
#include <QDebug>

LogService * LogService::m_instance = nullptr;

LogService::LogService(QObject * parent)
    : QObject(parent)
{

}

QVector<ILogger *> LogService::m_loggers;

LogService::~LogService()
{
    qDebug() << "destroyed." << endl;
}

bool LogService::OFF(false);

LogService & LogService::instance(QObject * parent)
{
    if (!m_instance) {
        m_instance = new LogService(parent);
    }
    return * m_instance;
}

void LogService::log(const LogMessage & msg)
{
    if (!on()) {
        return;
    }

    bool loggerIsOn = false;
    for(ILogger * logger : m_loggers) {
        if (logger != nullptr) {
            if (logger->logLevel() != ILogger::LogLevel::Off) {
                logger->log(msg);
                loggerIsOn = true;
            }
        }
    }
    if (loggerIsOn) {
        emit m_instance->sendMessage(msg);
    }
}

void LogService::addLogger(ILogger * logger)
{
    m_loggers.append(logger);
}

QVector<ILogger *> LogService::loggers()
{
    return m_loggers;
}

void LogService::setOn()
{
    OFF = true;
}

void LogService::setOff()
{
    OFF = false;
}

bool LogService::on()
{
    return !OFF;
}
