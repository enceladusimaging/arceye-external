#pragma once

#include <QtCore/QtGlobal>

#if defined(QTLOGGER_LIBRARY)
#  define QTLOGGER_EXPORT Q_DECL_EXPORT
#else
#  define QTLOGGER_EXPORT Q_DECL_IMPORT
#endif
