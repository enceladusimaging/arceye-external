#include "RuntimeStatsLoggerImpl.h"

RuntimeStatsLoggerImpl::RuntimeStatsLoggerImpl(
        const QString pathToLog, QObject * parent)
    : QObject(parent)
    , m_pathToLog(pathToLog)
    , m_timer(new QTimer(this))
    , m_logger(new QsLoggerImpl(pathToLog, this))
{
    connect(this, SIGNAL(logLevelChanged(ILogger::LogLevel)),
                         static_cast<QsLoggerImpl *>(m_logger), SLOT(changeLogLevel(ILogger::LogLevel)));
    connect(m_timer, SIGNAL(timeout()), this, SLOT(m_log()));

    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + START_MESSAGE, LogMessage::Type::Info));

    this->setInterval(600);
    m_timer->setInterval(this->interval());
}

RuntimeStatsLoggerImpl::~RuntimeStatsLoggerImpl()
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + END_MESSAGE, LogMessage::Type::Info));
}

void RuntimeStatsLoggerImpl::log(const LogMessage & msg)
{
    if (msg.type() == LogMessage::Type::RuntimeStats) {
        if (!m_timer->isActive()) {
            m_timer->start(this->interval());
        }

        if (!m_messages.contains(msg)) {
            m_messages.prepend(msg);
        }
    }
}

void RuntimeStatsLoggerImpl::m_log()
{
    foreach(LogMessage msg, m_messages) {
        m_logger->log(msg);
    }
}

void RuntimeStatsLoggerImpl::setLogLevel(ILogger::LogLevel level)
{
    m_logLevel = level;
    emit logLevelChanged(level);
}

QString RuntimeStatsLoggerImpl::type()
{
    return QString("RuntimeStatsLoggerImpl");
}
