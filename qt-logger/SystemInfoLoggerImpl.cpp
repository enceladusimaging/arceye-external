#include "SystemInfoLoggerImpl.h"

SystemInfoLoggerImpl::SystemInfoLoggerImpl(
        const QString pathToLog, QObject * parent)
    : QObject(parent)
    , m_timer(new QTimer(this))
    , m_pathToLog(pathToLog)
    , m_logger(new QsLoggerImpl(pathToLog, this))
{
    connect(this, SIGNAL(logLevelChanged(ILogger::LogLevel)),
                         static_cast<QsLoggerImpl *>(m_logger), SLOT(changeLogLevel(ILogger::LogLevel)));
    connect(m_timer, SIGNAL(timeout()), this, SLOT(m_log()));

    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + START_MESSAGE, LogMessage::Type::Info));

    this->setInterval(6000);
    m_timer->setInterval(this->interval());
}

SystemInfoLoggerImpl::~SystemInfoLoggerImpl()
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + END_MESSAGE, LogMessage::Type::Info));
}

void SystemInfoLoggerImpl::log(const LogMessage & msg)
{
    if (msg.type() == LogMessage::Type::SystemInfo) {
        if (!m_timer->isActive()) {
            m_timer->start(m_interval);
        }

        if (!m_messages.contains(msg)) {
            m_messages.prepend(msg);
        }
    }
}

void SystemInfoLoggerImpl::m_log() {
    // usage stats help from: http://tinyurl.com/3oysj44
    QString ramUsage = m_ramUsage();
    QString cpuUsage = m_cpuUsage();
    QString hdUsage = m_hardDriveUsage();

    QString nln = QString::fromUtf8("\n");
    QString tab = QString::fromUtf8("\t");
    QString sysInfo = tab + ramUsage + nln +
            tab + cpuUsage + nln +
            tab + hdUsage;

    foreach(LogMessage msg, m_messages) {
        QString content = msg.content() + nln + sysInfo;
        m_logger->log(LogMessage(content, LogMessage::Type::SystemInfo));
    }
}

void SystemInfoLoggerImpl::setLogLevel(ILogger::LogLevel level)
{
    m_logLevel = level;
    emit logLevelChanged(level);
}

QString SystemInfoLoggerImpl::type()
{
    return QString("SystemInfoLoggerImpl");
}

QString SystemInfoLoggerImpl::m_ramUsage()
{
    SIZE_T ramUsage;
    PROCESS_MEMORY_COUNTERS pmc;

    GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
    ramUsage = pmc.WorkingSetSize;
    SIZE_T ramUsageInMB = ramUsage / pow(10, 6);

    QString ramUsageFormatted = "RAM Usage: " + QString::number(ramUsageInMB) + " MB";

    return ramUsageFormatted;
}

QString SystemInfoLoggerImpl::m_cpuUsage()
{
    ULARGE_INTEGER lastCPU;
    int numProcessors;
    HANDLE self;

    FILETIME ftime, fsys, fuser;
    ULARGE_INTEGER now, sys, user;

    SYSTEM_INFO sysInfo;

    int percent;

    GetSystemInfo(&sysInfo);
    numProcessors = sysInfo.dwNumberOfProcessors;

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&lastCPU, &ftime, sizeof(FILETIME));

    self = GetCurrentProcess();
    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&now, &ftime, sizeof(FILETIME));

    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&sys, &fsys, sizeof(FILETIME));
    memcpy(&user, &fuser, sizeof(FILETIME));

    double totalCpu = now.QuadPart / pow(10, 9);
    if (totalCpu > 0) {
        double thisProcessCpu = sys.QuadPart + user.QuadPart;
        double processCpuFraction = thisProcessCpu / totalCpu;
        double processCpu = processCpuFraction / numProcessors;

        percent = processCpu * 100;
    } else {
        percent = 0;
    }

    QString cpuUsageFormatted = "CPU Usage: " + QString::number(percent) + "%";
    return cpuUsageFormatted;
}

QString SystemInfoLoggerImpl::m_hardDriveUsage()
{
    HANDLE self;
    PIO_COUNTERS IoCounters = new IO_COUNTERS;

    self = GetCurrentProcess();

    GetProcessIoCounters(self, IoCounters);

    ULONGLONG hdUsage = IoCounters->ReadTransferCount + IoCounters->WriteTransferCount;
    delete IoCounters;

    int hdUsageInMB = hdUsage / pow(10, 6);

    QString hdUsageFormatted = "Hard Drive Usage: " + QString::number(hdUsageInMB) + " MB";
    return hdUsageFormatted;
}
