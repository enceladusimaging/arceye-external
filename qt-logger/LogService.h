#pragma once

#include "qtlogger_global.h"

#include <QObject>
#include <QVector>

#include "LogMessage.h"
#include "ILogger.h"

class LogService : public QObject
{
    Q_OBJECT

public:
    ~LogService();

    static LogService & instance(QObject * parent = nullptr);

    static void log(const LogMessage &);

    static void addLogger(ILogger *);
    static QVector<ILogger *> loggers();

    static void setOn();
    static void setOff();
    static bool on();

signals:
    void sendMessage(const LogMessage &);

private:
    static bool OFF;

    LogService(QObject * parent);

    static LogService * m_instance;

    static QVector<ILogger *> m_loggers;
};
