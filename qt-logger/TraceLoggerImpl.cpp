#include "TraceLoggerImpl.h"

TraceLoggerImpl::TraceLoggerImpl(const QString pathToLog, QObject *parent)
    : QObject(parent)
    , m_pathToLog(pathToLog)
    , m_logger(new QsLoggerImpl(pathToLog, this))
{
    connect(this, SIGNAL(logLevelChanged(ILogger::LogLevel)),
                         static_cast<QsLoggerImpl *>(m_logger), SLOT(changeLogLevel(ILogger::LogLevel)));

    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + START_MESSAGE, LogMessage::Type::Info));
    setLogLevel(ILogger::LogLevel::Trace);

}

TraceLoggerImpl::~TraceLoggerImpl()
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + END_MESSAGE, LogMessage::Type::Info));
}

void TraceLoggerImpl::log(const LogMessage & msg)
{
    if (msg.type() == LogMessage::Type::Trace) {
        m_logger->log(msg);
    }
}

QString TraceLoggerImpl::type()
{
    return QString("TraceLoggerImpl");
}

void TraceLoggerImpl::setLogLevel(ILogger::LogLevel level)
{
    m_logLevel = level;
    emit logLevelChanged(level);
}
