#include "LatencyLoggerImpl.h"

LatencyLoggerImpl::LatencyLoggerImpl(const QString pathToLog, QObject * parent)
    : QObject(parent)
    , m_pathToLog(pathToLog)
    , m_logger(new QsLoggerImpl(pathToLog, this))
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + START_MESSAGE, LogMessage::Type::Info));
}

LatencyLoggerImpl::~LatencyLoggerImpl()
{
    setLogLevel(ILogger::LogLevel::Info);
    m_logger->log(LogMessage(type() + END_MESSAGE, LogMessage::Type::Info));
}

const int LatencyLoggerImpl::INVALID_TIME(0);

void LatencyLoggerImpl::log(const LogMessage & msg)
{
    if (msg.type() == LogMessage::Type::LatencyStart
            || msg.type() == LogMessage::Type::LatencyEnd) {

        int timeElapsed = m_resetTimerTrackers(msg) / 1000;

        if (timeElapsed > INVALID_TIME) {
            QString content = msg.content() + " " + QString::number(timeElapsed) + "us";
            m_logger->log(LogMessage(content, LogMessage::Type::LatencyEnd));
        }
    }
}

QString LatencyLoggerImpl::m_logFilePath() const
{
    return m_pathToLog;
}

double LatencyLoggerImpl::m_resetTimerTrackers(const LogMessage & msg)
{
    QString content = msg.content();
    LogMessage::Type type = msg.type();
    double timeElapsed = INVALID_TIME;
    if (type == LogMessage::Type::LatencyEnd) {
        if (m_messages.contains(content)) {
            int index = m_messages.indexOf(content);

            if (m_timers[index].isValid()) {
               timeElapsed = m_timers[index].nsecsElapsed();
            }
        }
    }
    int i = 0;
    foreach(QString c, m_messages) {
        if (c == content) {
            m_timers.removeAt(i);
        }
    }
    m_messages.removeAll(content);

    m_timers.prepend(QElapsedTimer());
    m_timers.first().start();
    m_messages.prepend(content);

    return timeElapsed;
}

QString LatencyLoggerImpl::type()
{
    return QString("LatencyLoggerImpl");
}
