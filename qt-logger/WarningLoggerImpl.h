#pragma once

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class WarningLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    WarningLoggerImpl(const QString, QObject * parent = nullptr);
    ~WarningLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private:
    QString m_pathToLog;

    QsLoggerImpl * m_logger;
};
