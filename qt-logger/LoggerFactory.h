#pragma once

#include "qtlogger_global.h"

#include "ILogger.h"
#include "LatencyLoggerImpl.h"
#include "SystemInfoLoggerImpl.h"
#include "RuntimeStatsLoggerImpl.h"
#include "ErrorLoggerImpl.h"
#include "WarningLoggerImpl.h"
#include "TraceLoggerImpl.h"
#include "InfoLoggerImpl.h"

class LoggerFactory
{

public:
    enum Type {
        InfoLoggerImpl,
        LatencyLoggerImpl,
        SystemInfoLoggerImpl,
        RuntimeStatsLoggerImpl,
        ErrorLoggerImpl,
        WarningLoggerImpl,
        TraceLoggerImpl
    };

    static ILogger * createLogger(
            Type,
            QString,
            ILogger::LogLevel = ILogger::LogLevel::Info,
            QObject * parent = nullptr);
};
