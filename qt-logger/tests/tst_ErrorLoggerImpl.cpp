#include "tst_ErrorLoggerImpl.h"

void tst_ErrorLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::ErrorLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Error,
                this);
}

void tst_ErrorLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Error;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_ErrorLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);

    m_logger->setLogLevel(ILogger::LogLevel::Error);
}

void tst_ErrorLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Error test";
    LogMessage errorMsg(content, LogMessage::Type::Error);
    m_logger->log(errorMsg);

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_ErrorLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_ErrorLoggerImpl::cleanupTestCase()
{

}
