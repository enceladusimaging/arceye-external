#pragma once

#include "../ILogger.h"

class MockLogger : public QObject, public ILogger
{
    Q_OBJECT

public:
    MockLogger(QObject * parent = nullptr);
    ~MockLogger();

    void log(const LogMessage &);

    QString type();

    LogMessage::Type getMessageType();
    QString getMessageContent();

private:
    LogMessage * m_logMessage;
    QString m_logFilePath;
};
