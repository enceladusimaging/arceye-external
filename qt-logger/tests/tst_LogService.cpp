#include "tst_LogService.h"

void tst_LogService::initTestCase()
{
    LogService::instance(this);
    m_logger = new MockLogger(this);

    LogService::addLogger(m_logger);
}

void tst_LogService::getAndSetLogger()
{
    try {
        QVERIFY(LogService::loggers()[0] == m_logger);
    } catch (...) {
        QFAIL("Unimplemented");
    }
}

void tst_LogService::log()
{
    try {
        LogMessage msg = LogMessage("Test error.", LogMessage::Type::Error);

        LogService::log(msg);

        // Tests
        QVERIFY(m_logger->getMessageContent() == msg.content()
                && m_logger->getMessageType() == msg.type()); // right message got logged
    } catch(...) {
        QFAIL("Unimplemented");
    }
}

void tst_LogService::cleanupTestCase()
{

}

