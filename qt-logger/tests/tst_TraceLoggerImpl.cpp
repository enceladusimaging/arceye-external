#include "tst_TraceLoggerImpl.h"

void tst_TraceLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::TraceLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Trace,
                this);
}

void tst_TraceLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Trace;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_TraceLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);

    m_logger->setLogLevel(ILogger::LogLevel::Trace);
}

void tst_TraceLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Trace test";
    LogMessage errorMsg(content, LogMessage::Type::Trace);
    m_logger->log(errorMsg);

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_TraceLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_TraceLoggerImpl::cleanupTestCase()
{

}
