#include "tst_LatencyLoggerImpl.h"

void tst_LatencyLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::LatencyLoggerImpl, m_pathToFile, ILogger::LogLevel::Info, this);
}

void tst_LatencyLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_LatencyLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_LatencyLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Latency test";
    LogMessage latencyMsg1(content, LogMessage::Type::LatencyStart);
    LogMessage latencyMsg2(content, LogMessage::Type::LatencyEnd);

    m_logger->log(latencyMsg1);
    QTest::qWait(20);
    m_logger->log(latencyMsg2);

    // tests
    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_LatencyLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_LatencyLoggerImpl::cleanupTestCase()
{

}
