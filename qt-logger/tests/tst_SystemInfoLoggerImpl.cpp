#include "tst_SystemInfoLoggerImpl.h"

void tst_SystemInfoLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::SystemInfoLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Info,
                this);
}

void tst_SystemInfoLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_SystemInfoLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_SystemInfoLoggerImpl::log()
{
    m_logger->setInterval(10);

    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "System info test";
    LogMessage errorMsg(content, LogMessage::Type::SystemInfo);
    m_logger->log(errorMsg);

    QEventLoop loop;
    QTimer::singleShot(10, &loop, SLOT(quit()));
    loop.exec();

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_SystemInfoLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_SystemInfoLoggerImpl::cleanupTestCase()
{

}
