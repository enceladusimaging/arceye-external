#pragma once

#include <QtTest/QtTest>

#include "../LoggerFactory.h"

class tst_ErrorLoggerImpl: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();

    void logLevel();
    void setLogLevel();

    void log();

    void cleanupTestCase();

private:
    ILogger * m_logger;
    QString m_pathToFile;

    bool m_findInFile(const QString & s, QFile * f);
};
