#include "tst_RuntimeStatsLoggerImpl.h"

void tst_RuntimeStatsLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::RuntimeStatsLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Info,
                this);
}

void tst_RuntimeStatsLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_RuntimeStatsLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_RuntimeStatsLoggerImpl::log()
{
    m_logger->setInterval(10);

    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Runtime Stats test";
    LogMessage errorMsg(content, LogMessage::Type::RuntimeStats);
    m_logger->log(errorMsg);

    QEventLoop loop;
    QTimer::singleShot(10, &loop, SLOT(quit()));
    loop.exec();

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_RuntimeStatsLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_RuntimeStatsLoggerImpl::cleanupTestCase()
{

}
