#include "MockLogger.h"

MockLogger::MockLogger(QObject * parent)
    : QObject(parent)
    , m_logMessage(nullptr)
{
    setLogLevel(ILogger::LogLevel::Info);
}

MockLogger::~MockLogger()
{

}

void MockLogger::log(const LogMessage & msg)
{
    if (m_logMessage != nullptr) {
        m_logMessage->setContent(msg.content());
        m_logMessage->setType(msg.type());
    } else {
        m_logMessage = new LogMessage(msg.content(), msg.type(), this);
    }
}

QString MockLogger::type()
{
    return QString("MockLogger");
}

LogMessage::Type MockLogger::getMessageType()
{
    if (m_logMessage != nullptr) {
        return m_logMessage->type();
    }
    return LogMessage::Type::Info;

}

QString MockLogger::getMessageContent()
{
    if (m_logMessage != nullptr) {
        return m_logMessage->content();
    }
    return QString("");
}
