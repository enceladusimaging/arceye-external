#include "tst_WarningLoggerImpl.h"

void tst_WarningLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::WarningLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Warning,
                this);
}

void tst_WarningLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Warning;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_WarningLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);

    m_logger->setLogLevel(ILogger::LogLevel::Warning);
}

void tst_WarningLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Warning test";
    LogMessage errorMsg(content, LogMessage::Type::Warning);
    m_logger->log(errorMsg);

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_WarningLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_WarningLoggerImpl::cleanupTestCase()
{

}
