#pragma once

#include <QtTest/QtTest>

#include "../LogService.h"
#include "MockLogger.h"

class tst_LogService: public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();

    void getAndSetLogger();
    void log();

    void cleanupTestCase();

private:
    MockLogger *m_logger;
};
