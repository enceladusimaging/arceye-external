#include "tst_QsLoggerImpl.h"

void tst_QsLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = new QsLoggerImpl(m_pathToFile, this);
}

void tst_QsLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_QsLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Trace;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_QsLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "QsLogger test";
    LogMessage traceMsg(content, LogMessage::Type::Trace);
    m_logger->log(traceMsg);
    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_QsLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_QsLoggerImpl::cleanupTestCase()
{

}
