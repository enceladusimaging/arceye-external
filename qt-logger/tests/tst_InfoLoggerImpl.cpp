#include "tst_InfoLoggerImpl.h"

void tst_InfoLoggerImpl::initTestCase()
{
    m_pathToFile = "log.txt";
    m_logger = LoggerFactory::createLogger(
                LoggerFactory::Type::InfoLoggerImpl,
                m_pathToFile,
                ILogger::LogLevel::Info,
                this);
}

void tst_InfoLoggerImpl::logLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Info;
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);
}

void tst_InfoLoggerImpl::setLogLevel()
{
    ILogger::LogLevel expectedLevel = ILogger::LogLevel::Error;
    m_logger->setLogLevel(expectedLevel);
    ILogger::LogLevel actualLevel = m_logger->logLevel();

    // tests
    QCOMPARE(actualLevel, expectedLevel);

    m_logger->setLogLevel(ILogger::LogLevel::Info);
}

void tst_InfoLoggerImpl::log()
{
    QString content;
    QFile * f = new QFile(m_pathToFile, this);
    bool found;

    content = "Info test";
    LogMessage errorMsg(content, LogMessage::Type::Info);
    m_logger->log(errorMsg);

    found = m_findInFile(content, f);
    QCOMPARE(found, true);
}

bool tst_InfoLoggerImpl::m_findInFile(const QString & s, QFile * f)
{
    bool found = false;
    if (f->open(QIODevice::ReadOnly)) {
        QString ln;
        QTextStream stream(f);
         while (!stream.atEnd()) {
            ln = stream.readLine();
            if (ln.contains(s)) {
                found = true;
                break;
            }
        }
        f->close();
    }
    return found;
}

void tst_InfoLoggerImpl::cleanupTestCase()
{

}
