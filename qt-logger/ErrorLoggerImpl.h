#pragma once

#include "qtlogger_global.h"

#include "ILogger.h"
#include "LoggerFactory.h"

class ErrorLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    ErrorLoggerImpl(const QString, QObject * parent = nullptr);
    ~ErrorLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private:
    QString m_pathToLog;

    ILogger * m_logger;
};
