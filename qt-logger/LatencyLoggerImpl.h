#pragma once

#include <QElapsedTimer>

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class LatencyLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    LatencyLoggerImpl(const QString, QObject * parent = nullptr);
    ~LatencyLoggerImpl();

    void log(const LogMessage &);

    QString type();

private:
    static const int INVALID_TIME;

    QString m_pathToLog;

    QList<QString> m_messages;
    QList<QElapsedTimer> m_timers;

    QString m_logFilePath() const;
    double m_resetTimerTrackers(const LogMessage &);

    QsLoggerImpl * m_logger;
};
