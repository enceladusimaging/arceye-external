#include "LogMessage.h"

LogMessage::LogMessage(const LogMessage & msg, QObject * parent)
    : QObject(parent)
{
    m_content = msg.content();
    m_type = msg.type();
}

LogMessage::LogMessage(const QString & content, Type type, QObject * parent)
    : QObject(parent)
    , m_content(content)
    , m_type(type)
{

}

LogMessage::~LogMessage()
{

}

QString LogMessage::content() const
{
    return m_content;
}

void LogMessage::setContent(const QString content)
{
    m_content = content;
}

LogMessage::Type LogMessage::type() const
{
    return m_type;
}

void LogMessage::setType(const LogMessage::Type type)
{
    m_type = type;
}

bool LogMessage::operator==(const LogMessage & msg)
{
    if (m_content == msg.content() && m_type == msg.type()) {
        return true;
    }
    return false;
}


