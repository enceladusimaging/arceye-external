#pragma once

#include "qtlogger_global.h"

#include "QsLoggerImpl.h"

class TraceLoggerImpl : public QObject, public ILogger
{
    Q_OBJECT

public:
    TraceLoggerImpl(const QString, QObject * parent = nullptr);
    ~TraceLoggerImpl();

    void log(const LogMessage &);

    QString type();

    void setLogLevel(ILogger::LogLevel);

signals:
    void logLevelChanged(ILogger::LogLevel);

private:
    QString m_pathToLog;

    QsLoggerImpl * m_logger;
};
