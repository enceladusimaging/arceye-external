#pragma once

#include "qtlogger_global.h"

#include "LogMessage.h"

class ILogger
{

public:
    enum LogLevel { Warning, Error, Trace, Info, Off };

    virtual ~ILogger() { }

    virtual void log(const LogMessage &) = 0;

    virtual QString type() = 0;

    virtual void setLogLevel(LogLevel);
    virtual LogLevel logLevel();

    virtual void setInterval(int msec);
    virtual int interval();

protected:
    static const LogLevel OFF;
    static const QString START_MESSAGE;
    static const QString END_MESSAGE;

    LogLevel m_logLevel;
    int m_interval;
};
