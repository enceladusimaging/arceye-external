#pragma once

#include "qtlogger_global.h"

#include <QObject>

class LogMessage : public QObject
{
    Q_OBJECT

public:
    enum Type { Info,
                Warning,
                Error,
                LatencyStart,
                LatencyEnd,
                Trace,
                SystemInfo,
                RuntimeStats };

    LogMessage() {}
    LogMessage(const LogMessage &, QObject *parent = nullptr);
    LogMessage(const QString &, Type type = LogMessage::Type::Info, QObject *parent = nullptr);
    ~LogMessage();

    bool operator==(const LogMessage &);

    QString content() const;
    void setContent(const QString);

    Type type() const;
    void setType(const Type);

private:
    QString m_content;
    Type m_type;
};
