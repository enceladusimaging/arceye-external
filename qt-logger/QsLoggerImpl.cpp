#include "QsLoggerImpl.h"

QsLoggerImpl::QsLoggerImpl(const QString pathToLog, QObject * parent)
    : QObject(parent)
    , m_pathToLog(pathToLog)
    , m_logger(QsLogging::Logger::instance())
{
    setLogLevel(ILogger::LogLevel::Info);
    m_setLogFilePath(m_pathToLog);
}

QsLoggerImpl::~QsLoggerImpl()
{

}

QList<QString> * QsLoggerImpl::m_files(new QList<QString>());

void QsLoggerImpl::log(const LogMessage & msg)
{
    // QsLog levels (least to most restrictive):
    // TraceLevel, DebugLevel, InfoLevel, WarnLevel,
    // ErrorLevel, FatalLevel, OffLevel

    m_setLogLevelForQsLog();

    if (m_logLevel != OFF) {
        const QString & content = msg.content();
        LogMessage::Type type = msg.type();
        switch(type)
        {
        case LogMessage::Type::LatencyEnd :
            QLOG_INFO() << content;
            break;
        case LogMessage::Type::RuntimeStats :
            QLOG_INFO() << content;
            break;
        case LogMessage::Type::Error :
            QLOG_ERROR() << content;
            break;
        case LogMessage::Type::Warning :
            QLOG_WARN() << content;
            break;
        case LogMessage::Type::Trace :
            QLOG_TRACE() << content;
            break;
        case LogMessage::Type::SystemInfo :
            QLOG_INFO() << content;
            break;
        case LogMessage::Type::Info :
            QLOG_INFO() << content;
            break;
        default:
            return;
        }
    }
}

void QsLoggerImpl::changeLogLevel(ILogger::LogLevel level)
{
    this->setLogLevel(level);
}

void QsLoggerImpl::m_setLogLevelForQsLog()
{
    switch(m_logLevel)
    {
        case LogLevel::Warning :
            m_logger.setLoggingLevel(QsLogging::Level::WarnLevel);
            break;
        case LogLevel::Error :
            m_logger.setLoggingLevel(QsLogging::Level::ErrorLevel);
            break;
        case LogLevel::Trace :
            m_logger.setLoggingLevel(QsLogging::Level::TraceLevel);
            break;
        default:
            m_logger.setLoggingLevel(QsLogging::Level::InfoLevel);
    }
}

QsLogging::MaxSizeBytes QsLoggerImpl::m_logFileByteSize(QsLogging::MaxSizeBytes(10000000));

QsLogging::MaxOldLogCount QsLoggerImpl::m_oldLogsToKeep(QsLogging::MaxOldLogCount(10));

void QsLoggerImpl::m_setLogFilePath(const QString pathToFile)
{
    if (m_files->contains(pathToFile)) {
        return;
    }

    m_files->append(pathToFile);

    QsLogging::DestinationPtr fileDestination(
         QsLogging::DestinationFactory::MakeFileDestination(
                    pathToFile,
                    QsLogging::LogRotationOption::EnableLogRotation,
                    m_logFileByteSize,
                    m_oldLogsToKeep));

    m_logger.addDestination(fileDestination);
}

QString QsLoggerImpl::m_logFilePath() const
{
    return m_pathToLog;
}

QString QsLoggerImpl::type()
{
    return QString("QsLoggerImpl");
}
