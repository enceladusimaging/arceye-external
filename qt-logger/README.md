# README #

qt-logger

### What is this repository for? ###

A comprehensive logger for Qt C++ projects. Utilizes QsLog (https://bitbucket.org/razvanpetru/qslog/).

Logs:

* Trace messages
* Errors and warnings
* Latency
* On-timer messages
* Periodic system information
* General information logging 

### How do I get set up? ###

* I use Qt Creator 3.2.1 on Windows 8.1 (x64) with MSVC 12 compiler 
* Download the QsLog from the link above and add it to the project directory 
* Cut and paste the parent directory of the QsLog source code into your project directory and rename it to "QsLog"