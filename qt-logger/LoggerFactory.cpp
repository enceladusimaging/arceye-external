#include "LoggerFactory.h"

ILogger * LoggerFactory::createLogger(
        Type type, QString destinationPath, ILogger::LogLevel level, QObject * parent)
{
    ILogger * logger;
    switch(type)
    {
    case InfoLoggerImpl:
        logger = new ::InfoLoggerImpl(destinationPath, parent);
        break;
    case LatencyLoggerImpl:
        logger = new ::LatencyLoggerImpl(destinationPath, parent);
        break;
    case SystemInfoLoggerImpl:
        logger = new ::SystemInfoLoggerImpl(destinationPath, parent);
        break;
    case RuntimeStatsLoggerImpl:
        logger = new ::RuntimeStatsLoggerImpl(destinationPath, parent);
        break;
    case ErrorLoggerImpl:
        logger = new ::ErrorLoggerImpl(destinationPath, parent);
        break;
    case WarningLoggerImpl:
        logger = new ::WarningLoggerImpl(destinationPath, parent);
        break;
    case TraceLoggerImpl:
        logger = new ::TraceLoggerImpl(destinationPath, parent);
        break;
    }

    logger->setLogLevel(level);
    return logger;
}
