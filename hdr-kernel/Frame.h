#pragma once

#include "hdrDllExports.h"

#include <memory>

enum frameFormat_t { RGB_8 };

struct DLL_EXPORT frameParams_t {
    size_t width, height, index;
    frameFormat_t pixelFormat;

    size_t depth() const {
        size_t depth = 0;

        switch (pixelFormat) {
        case RGB_8: depth = 3;
        }

        return depth;
    }

    size_t sizeBytes() const {
        return width * height * depth();
    }
};

class DLL_EXPORT Frame {

public:

    Frame();
    ~Frame();

    // disable copy and assignment
    Frame(const Frame &) = delete;
    Frame & operator=(const Frame &) = delete;
    
    Frame(Frame && other);
    Frame& operator=(Frame&& other);

    virtual const unsigned char * getHostData() const;
    virtual const unsigned char * getDeviceData() const;

    // allocates data to the dimensions given on both host and GPU
    virtual void allocate(const frameParams_t dimensions);
    // copies data at src to the respective host or device pointer
    virtual void copyFrom(const unsigned char * src, const size_t size, bool toHostPtr = true, void * stream = nullptr);
    // copies data pointed to by host pointer to GPU
    virtual void upload(void * stream = nullptr);
    // copies data pointed to by device pointer to host
    virtual void download(void * stream = nullptr);
    // copies device data to host pointer provided
    virtual void downloadTo(unsigned char * dest, void * stream = nullptr);
    // frees managed data
    virtual void purge();

private:

#pragma warning(push)
#pragma warning(disable: 4251)
    class FrameImpl;
    std::unique_ptr<FrameImpl> p_impl_;
#pragma warning(pop)

};