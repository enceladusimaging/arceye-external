#pragma once

#include "hdrDllExports.h"
#include "Frame.h"

#include <list>
#include <vector>

class DLL_EXPORT IndexGenerator {
public:
    virtual ~IndexGenerator() {}
    virtual size_t getNext(size_t const shutterVal = 0)=0;
};

class DLL_EXPORT SequentialIndexGenerator : public IndexGenerator {

public:

    SequentialIndexGenerator(bool const countUp, size_t max, size_t const initialVal)
        : countUp_(countUp)
        , max_(max)
        , val_(initialVal)
    {}

    virtual ~SequentialIndexGenerator() {}

    virtual size_t getNext(size_t const shutterVal) override {
        // this ignores the shutter value and returns a wrapping count up or down
        val_ = countUp_ ? nextCountUp(val_, max_) : nextCountDown(val_, max_);

        return val_;
    }

private:
    inline unsigned int nextCountUp(const unsigned int count, const unsigned int max) {
        return (count < max) ? count + 1 : 0;
    }

    inline unsigned int nextCountDown(const unsigned int count, const unsigned int max) {
        return (count > 0) ? count - 1 : max;
    }

    bool countUp_;
    size_t max_, val_;

};

class DLL_EXPORT PositionalIndexGenerator : public IndexGenerator {

public:
    PositionalIndexGenerator(size_t const nPositions)
        : shutters_()
        , nShutters(nPositions)
    {}

    virtual ~PositionalIndexGenerator() {}

    virtual size_t getNext(size_t const shutterVal) override {
        shutters_.push_back(shutterVal);

        if (shutters_.size() > nShutters) shutters_.pop_front();

        size_t nSmaller = 0;
        for each (size_t elem in shutters_) {
            if (elem < shutterVal) ++nSmaller;
        }

        return (shutters_.size() - 1) - nSmaller; // returns 0-based index
    }

private:

    std::list<size_t> shutters_;
    size_t nShutters;
};

struct DLL_EXPORT responseFunc_t {
    float scalar, power, max, gamma, min;
};

struct DLL_EXPORT hdrSpec_t {
    size_t width, height, colDepth, nFrames, streamId;
    std::shared_ptr<IndexGenerator> indexGenerator;
    responseFunc_t cameraResponse;
    Table *ccrf, *ppem, *fastPpem, *ppemLimiter, *converter;
	float ev;
	size_t ccrf_length, ppem_length;

    size_t getImgSizeBytes() const { return width * height * colDepth; }
};

class DLL_EXPORT HdrContext {

public:
	enum cameraType_t {CANON, SONY};
    enum tableType_t { CCRF, PPEM, FAST_PPEM, CONVERTER };
    enum byteBufferType_t { INPUT_FRAME, INPUT_FRAME_TRANSPOSED, OUTPUT_FRAME };
	enum intBufferType_t { HISTOGRAM_GPU, HISTOGRAM_CPU, PARTIAL_HISTOGRAM, UINT_IMAGE };
    enum floatBufferType_t { LIGHT, LIGHT_TRANSPOSED, PLUS, MINUS, G1, G2, G3, LUMA, LUMA_TRANSPOSED, RECONSTRUCTED, RECONSTRUCTED_TRANSPOSED, NORMALIZED };
    
    struct kernelDims_t {
        int blocksPerGroup;
        int threadsPerBlock;
    };

    explicit HdrContext(const hdrSpec_t & spec);
    ~HdrContext();

    HdrContext(const HdrContext &) = delete;
    HdrContext & operator=(const HdrContext &) = delete;

    HdrContext(HdrContext && other);
    HdrContext & operator=(HdrContext && other);

    void allocate(const size_t ccrfSizeBytes, const size_t fastPpemSizeBytes, const size_t converterSizeBytes);
    void copyTable(const float * data, const size_t sizeBytes, tableType_t destTable);

    hdrSpec_t getSpec() const;

    kernelDims_t getKernelDims() const;

    unsigned char * getByteBuffer(byteBufferType_t bufferType) const;
	unsigned int * getIntBuffer(intBufferType_t bufferType) const;
    float * getFloatBuffer(floatBufferType_t bufferType) const;
    float * getTable(tableType_t tableType) const;

    unsigned char * getInputFrameBufferLocation(const byteBufferType_t buffer, const size_t index) const;

    void * getCublasHandle() const;

    void * getStream() const;

	static responseFunc_t getCameraResponse(const cameraType_t cam);

	static std::unique_ptr<float[]> responseConversion(const cameraType_t src, const cameraType_t dst, const float q_scalar, const unsigned int length);

private:

#pragma warning(push)
#pragma warning(disable: 4251)
    class HdrContextImpl;
    std::unique_ptr<HdrContextImpl> p_impl_;
#pragma warning(pop)

};