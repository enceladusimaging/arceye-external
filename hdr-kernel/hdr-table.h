#pragma once

#include <algorithm>
#include <cassert>
#include <fstream>
#include <memory>

#include "hdrDllExports.h"

template class DLL_EXPORT std::unique_ptr < float[] > ;

class DLL_EXPORT Table {

public:
    explicit Table(const std::string & fileName, const size_t nDimW, const size_t nDimH);
    Table(std::unique_ptr<float[]> data, const size_t nDimW, const size_t nDimH);

    Table(const Table &) = delete;
    Table& operator= (const Table &) = delete;

    Table(Table &&);
    Table & operator= (Table &&);
    
    float & operator[](size_t idx);
    const float & operator[](size_t idx) const;

    const float* data() const { return data_.get(); }
    size_t width() const { return w_; }
    size_t height() const { return h_; }
    size_t sizeBytes() const { return w_ * h_ * sizeof(float); }

private:
    std::unique_ptr<float[]> fileLoad(const std::string & fileName, const size_t width, const size_t height);

    std::unique_ptr<float[]> data_;
    size_t w_, h_;
};