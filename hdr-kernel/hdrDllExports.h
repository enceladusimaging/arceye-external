#pragma once

#ifdef KERNEL_API_EXPORTS
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

class HdrContext;

extern "C"{
    DLL_EXPORT void frameCopyToDevice(const HdrContext & context, const unsigned char * data, const size_t nBytes, const size_t index);
    DLL_EXPORT void frameCopyToHost(const HdrContext & context, unsigned char* output, const size_t nBytes);
	DLL_EXPORT void createCompositingTableGPU(const HdrContext & context);
	DLL_EXPORT void createFastPPEMTableGPU(const HdrContext & context);
    DLL_EXPORT void renderHDR(const HdrContext & context, const size_t lastFrameIndex, float delta_ev);
};

class DLL_EXPORT Table;
DLL_EXPORT void constrainCcrfVal(Table & target);
