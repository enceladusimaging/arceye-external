#include "PointGreyCtl.h"

#include "ui_PointGreyCtl.h"

PointGreyCtl::PointGreyCtl(SettingsManager * settingsManager,  QWidget *parent) 
    : QDialog(parent)
	, ui(new Ui::PointGreyCtl)
    , m_settingsManager(settingsManager)
    , GAIN_LABEL_TEMPLATE("%1 dB")
    , SHUTTER_LABEL_TEMPLATE("%1 ms")
    , FPS_LABEL_TEMPLATE("Maximum: %1 fps")
    , WINDOW_TITLE_TEMPLATE("Settings - %1 Profile")
    , TRIGGER_STROBE_ENABLED_NAME("strobeGpio%1Chk")
    , TRIGGER_STROBE_POLARITY_NAME("strobeGpio%1PolarityCombo")
    , TRIGGER_STROBE_DELAY_NAME("strobeGpio%1DelaySpin")
    , TRIGGER_STROBE_DURATION_NAME("strobeGpio%1DurationSpin")
{
	ui->setupUi(this);

    m_shutterLabels.push_back(ui->hdrShutter1IntLabel);
    m_shutterLabels.push_back(ui->hdrShutter2IntLabel);
    m_shutterLabels.push_back(ui->hdrShutter3IntLabel);
    m_shutterLabels.push_back(ui->hdrShutter4IntLabel);

    m_gainLabels.push_back(ui->hdrGain1Label);
    m_gainLabels.push_back(ui->hdrGain2Label);
    m_gainLabels.push_back(ui->hdrGain3Label);
    m_gainLabels.push_back(ui->hdrGain4Label);

    m_shutterSpins.push_back(ui->hdrShutterSpin1);
    m_shutterSpins.push_back(ui->hdrShutterSpin2);
    m_shutterSpins.push_back(ui->hdrShutterSpin3);
    m_shutterSpins.push_back(ui->hdrShutterSpin4);

    m_gainSpins.push_back(ui->hdrGainSpin1);
    m_gainSpins.push_back(ui->hdrGainSpin2);
    m_gainSpins.push_back(ui->hdrGainSpin3);
    m_gainSpins.push_back(ui->hdrGainSpin4);

    // Settings manager profile changed event
    connect(m_settingsManager, SIGNAL(profileChanged()), this, SLOT(activeProfileChanged()));

    // main camera settings
    connect(ui->mainShutterSpin, SIGNAL(valueChanged(double)), this, SLOT(mainShutterChanged(double)));

    // HDR enabled checkboxes
    connect(ui->hdrCameraChk, SIGNAL(toggled(bool)), this, SLOT(hdrCameraToggled(bool)));

    //HDR shutter labels
    connect(ui->hdrShutterSpin1, SIGNAL(valueChanged(int)), this, SLOT(shutterChanged(int)));
    connect(ui->hdrShutterSpin2, SIGNAL(valueChanged(int)), this, SLOT(shutterChanged(int)));
    connect(ui->hdrShutterSpin3, SIGNAL(valueChanged(int)), this, SLOT(shutterChanged(int)));
    connect(ui->hdrShutterSpin4, SIGNAL(valueChanged(int)), this, SLOT(shutterChanged(int)));
    
    //HDR gain labels
    connect(ui->hdrGainSpin1, SIGNAL(valueChanged(int)), this, SLOT(gainChanged(int)));
    connect(ui->hdrGainSpin2, SIGNAL(valueChanged(int)), this, SLOT(gainChanged(int)));
    connect(ui->hdrGainSpin3, SIGNAL(valueChanged(int)), this, SLOT(gainChanged(int)));
    connect(ui->hdrGainSpin4, SIGNAL(valueChanged(int)), this, SLOT(gainChanged(int)));

    connect(ui->lockSlidersChk, SIGNAL(toggled(bool)), this, SLOT(sliderLockToggled(bool)));
	connect(ui->initShutterSpin, SIGNAL(valueChanged(double)), this, SLOT(linkedHdrControlsValueChanged(double)));
    connect(ui->deltaEvSpin, SIGNAL(valueChanged(double)), this, SLOT(linkedHdrControlsValueChanged(double)));

    // trigger controls
    connect(ui->triggeredCaptureChk, SIGNAL(toggled(bool)), this, SLOT(triggerFunctionalityEnabled(bool)));
    connect(ui->triggeredRecordingChk, SIGNAL(toggled(bool)), this, SLOT(triggerFunctionalityEnabled(bool)));
    connect(ui->triggeredCapturePinCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(triggeredCapturePinChanged(int)));
    connect(ui->triggeredRecordingCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(triggeredRecordPinChanged(int)));
    connect(ui->triggerCaptureDelaySlider, SIGNAL(valueChanged(int)), this, SLOT(triggerDelaySliderValueChanged(int)));
    connect(ui->triggerCaptureDelaySpin, SIGNAL(valueChanged(double)), this, SLOT(triggerDelaySpinValueChanged(double)));

    // record controls
    connect(ui->recordFolderButton, SIGNAL(clicked()), this, SLOT(recordFolderClicked()));

    // Button box behaviour
    connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(saveProfile()));
    connect(ui->saveAsButton, SIGNAL(clicked()), this, SLOT(saveProfileAs()));
    connect(ui->removeButton, SIGNAL(clicked()), this, SLOT(removeProfileClicked()));
    connect(ui->applyButton, SIGNAL(clicked()), this, SLOT(applySettings()));

    //Load option values into UI
    activeProfileChanged();
}

PointGreyCtl::~PointGreyCtl() {
	delete ui;
};

void PointGreyCtl::activeProfileChanged()
{
	LogService::log(LogMessage("PointGreyCtl::activeProfileChanged", LogMessage::Type::Trace));

    ProfileSettings ps = m_settingsManager->currentSettings();
    const HdrSettings hs = ps.hdrSettings;
    const CameraSettings cs = ps.cameraSettings;

    ui->hdrCameraChk->setChecked(cs.hdrSettings.enabled);
    ui->hdrProcessingChk->setChecked(hs.enabled);

    ui->mainShutterSpin->setValue(cs.basicSettings.shutter);
    ui->mainGainSpin->setValue(cs.basicSettings.gain);

    ui->initShutterSpin->setValue(cs.initialShutter);
    ui->deltaEvSpin->setValue(cs.deltaEv);

    QVector<unsigned int> shutters = cs.hdrSettings.shutters;
    QVector<unsigned int> gains = cs.hdrSettings.gains;
    setupHdrControls(shutters, gains);

    // triggered capture
    ui->triggeredCaptureChk->setChecked(cs.triggerSettings.triggeredCapture.enabled);
    ui->triggeredCapturePinCombo->setCurrentText(QString::number(cs.triggerSettings.triggeredCapture.source));
    ui->triggeredCaptureModeCombo->setCurrentText(QString::number(cs.triggerSettings.triggeredCapture.mode));
    ui->triggeredCapturePolarityCombo->setCurrentIndex(cs.triggerSettings.triggeredCapture.polarity);
    ui->triggerCaptureDelaySpin->setValue(cs.triggerSettings.triggeredCapture.delayS * 1000.0); //slider will set delay spin box

    // triggered recording
    ui->triggeredRecordingCombo->setCurrentText(QString::number(cs.triggerSettings.reservedInputPins[0]));

    // strobe controls
    foreach (TriggerStrobe strobeSetting, cs.triggerSettings.strobeSettings) {
        unsigned int pin = strobeSetting.source;

        QCheckBox * enabled = findChild<QCheckBox *>(TRIGGER_STROBE_ENABLED_NAME.arg(pin));
        if (enabled) enabled->setChecked(strobeSetting.enabled);

        QComboBox * polarity = findChild<QComboBox *>(TRIGGER_STROBE_POLARITY_NAME.arg(pin));
        if (polarity) polarity->setCurrentIndex(strobeSetting.polarity);

        QDoubleSpinBox * delay = findChild<QDoubleSpinBox *>(TRIGGER_STROBE_DELAY_NAME.arg(pin));
        if (delay) delay->setValue(strobeSetting.delayMs);

        QDoubleSpinBox * duration = findChild<QDoubleSpinBox *>(TRIGGER_STROBE_DURATION_NAME.arg(pin));
        if (duration) duration->setValue(strobeSetting.durationMs);
    }

    // set the strobe pin states after recording and capture have claimed pins
    adjustStrobePinStates();

    // set the frame rate after all other changes have been applied
    ui->frameRateSpin->setValue(cs.basicSettings.frameRate);
    
    // record controls
    RecordSettings rs = m_settingsManager->currentSettings().recordSettings;
    ui->recordFolderLabel->setText(rs.targetDir);
    ui->recordStopThresholdSpin->setValue(rs.diskFullPercentThreshold);
    if (rs.codec.compare(tr("lossy")) == 0) {
        ui->recordCompressionCombo->setCurrentIndex(0);
    } else if (rs.codec.compare(tr("lossless")) == 0) {
        ui->recordCompressionCombo->setCurrentIndex(1);
    } else {
        ui->recordCompressionCombo->setCurrentIndex(2);
    }

    ui->recordFrameRateSpin->setValue(rs.frameRate);
    ui->recordFrameRateSpin->setEnabled(rs.frameRateOverride);
    ui->recordFrameRateOverrideChk->setChecked(rs.frameRateOverride);
	ui->videoLengthDoubleSpinBox->setValue(rs.timeInterval);
    
    // triggered recording settings
    ui->triggeredRecordingChk->setChecked(rs.arcOnRecording);
    ui->triggeredRecordingCombo->setCurrentText(QString::number(rs.arcOnRecordingPin));

    ui->removeButton->setEnabled(m_settingsManager->currentSettings().removable);

    setWindowTitle(WINDOW_TITLE_TEMPLATE.arg(m_settingsManager->currentSettings().name) 
		+ tr(" - Camera Serial: ")
		+ QString::number(m_settingsManager->currentSettings().cameraSettings.serialNumber));
}

void PointGreyCtl::setupHdrControls(const QVector<unsigned int> & shutters, const QVector<unsigned int> & gains) 
{
	LogService::log(LogMessage("PointGreyCtl::setupHdrControls", LogMessage::Type::Trace));

    // Set the four HDR configs
    for(int i = 0; i < 4; i++) {
        QSpinBox * shutterSpin = m_shutterSpins[i];
        shutterSpin->setValue(shutters.at(i));
        m_gainSpins.at(i)->setValue(gains.at(i));
    }

    clampFpsOnMaxShutter();
}

void PointGreyCtl::hdrCameraToggled(bool enabled)
{
	LogService::log(LogMessage("PointGreyCtl::hdrCameraToggled", LogMessage::Type::Trace));

    // Disable lock control
    ui->lockSlidersChk->setChecked(false);
}

void PointGreyCtl::sliderLockToggled(bool enabled)
{
	LogService::log(LogMessage("PointGreyCtl::sliderLockToggled", LogMessage::Type::Trace));

    if (enabled) {
        linkedHdrControlsValueChanged();
    }
}

void PointGreyCtl::linkedHdrControlsValueChanged(double value)
{
	LogService::log(LogMessage("PointGreyCtl::linkedHdrControlsValueChanged", LogMessage::Type::Trace));

    Q_UNUSED(value);

    double shutterMs = ui->initShutterSpin->value()
        , deltaEv = ui->deltaEvSpin->value();

    // update all HDR controls
    QVector<unsigned int> shutters, gains;
    for (int i = 3; i >= 0; i--) {
        shutters << integralShutterFromMs(shutterMs * pow(2, deltaEv * i));
    }

	for (int i = 0; i < 4; i++){
		gains << m_gainSpins.at(i)->value();
	}
    
    setupHdrControls(shutters, gains);
}

void PointGreyCtl::mainShutterChanged(double shutterMs)
{
	LogService::log(LogMessage("PointGreyCtl::mainShutterChanged", LogMessage::Type::Trace));

    clampFpsOnMaxShutter();
}

void PointGreyCtl::shutterChanged(int integralShutter)
{
	LogService::log(LogMessage("PointGreyCtl::shutterChanged", LogMessage::Type::Trace));

    QSpinBox * shutterSpin = qobject_cast<QSpinBox *>(sender());
    int shutterIdx = m_shutterSpins.indexOf(shutterSpin);
    QLabel * shutterLabel = m_shutterLabels[shutterIdx];
    
    double shutterMs = msShutterFromIntegral(integralShutter);
    shutterLabel->setText(SHUTTER_LABEL_TEMPLATE.arg(shutterMs));

    clampFpsOnMaxShutter();
}

void PointGreyCtl::gainChanged(int integralGain)
{
	LogService::log(LogMessage("PointGreyCtl::gainChanged", LogMessage::Type::Trace));

    QSpinBox * gainSpin = qobject_cast<QSpinBox *>(sender());
    int gainIdx = m_gainSpins.indexOf(gainSpin);
    QLabel * gainLabel = m_gainLabels[gainIdx];

    gainLabel->setText(GAIN_LABEL_TEMPLATE.arg(dbGainFromIntegral(gainSpin->value())));
}

void PointGreyCtl::clampFpsOnMaxShutter()
{
	LogService::log(LogMessage("PointGreyCtl::clampFpsOnMaxShutter", LogMessage::Type::Trace));

    double largestShutterMs = ui->mainShutterSpin->value();

    if (ui->hdrCameraChk->isChecked()) {
        int largestShutter = 0;
        foreach(QSpinBox * shutterSpin, m_shutterSpins) {
            if (shutterSpin->value() > largestShutter) largestShutter = shutterSpin->value();
        }

        largestShutterMs = msShutterFromIntegral(largestShutter);
    }

    double maxFpsBasedOnThisShutter = maxFpsFromShutterMs(largestShutterMs);
	ui->frameRateSpin->setMaximum(maxFpsBasedOnThisShutter);
    ui->maxFpsLabel->setText(FPS_LABEL_TEMPLATE.arg(maxFpsBasedOnThisShutter));
}

// Trigger controls
void PointGreyCtl::triggerFunctionalityEnabled(bool enabled) 
{
	LogService::log(LogMessage("PointGreyCtl::triggerFunctionalityEnabled", LogMessage::Type::Trace));

    Q_UNUSED(enabled);

    adjustStrobePinStates();
}

void PointGreyCtl::triggeredCapturePinChanged(int capturePinIdx)
{
	LogService::log(LogMessage("PointGreyCtl::triggeredCapturePinChanged", LogMessage::Type::Trace));

    switch(capturePinIdx) {
    case 0: 
        if (ui->triggeredRecordingCombo->currentIndex() == capturePinIdx) {
            ui->triggeredRecordingCombo->setCurrentIndex(1);
        }
        break;

    case 1: 
        if (ui->triggeredRecordingCombo->currentIndex() == capturePinIdx) {
            ui->triggeredRecordingCombo->setCurrentIndex(2);
        }
        break;

    case 2: 
        if (ui->triggeredRecordingCombo->currentIndex() == capturePinIdx) {
            ui->triggeredRecordingCombo->setCurrentIndex(1);
        }
        break;
    }

    adjustStrobePinStates();
}

void PointGreyCtl::triggeredRecordPinChanged(int recordPinIdx)
{
	LogService::log(LogMessage("PointGreyCtl::triggeredRecordPinChanged", LogMessage::Type::Trace));

    switch(recordPinIdx) {
    case 0: 
        if (ui->triggeredCapturePinCombo->currentIndex() == recordPinIdx) {
            ui->triggeredCapturePinCombo->setCurrentIndex(1);
        }
        break;

    case 1: 
        if (ui->triggeredCapturePinCombo->currentIndex() == recordPinIdx) {
            ui->triggeredCapturePinCombo->setCurrentIndex(2);
        }
        break;

    case 2: 
        if (ui->triggeredCapturePinCombo->currentIndex() == recordPinIdx) {
            ui->triggeredCapturePinCombo->setCurrentIndex(1);
        }
        break;
    }

    adjustStrobePinStates();
}

void PointGreyCtl::adjustStrobePinStates()
{
	LogService::log(LogMessage("PointGreyCtl::adjustStrobePinStates", LogMessage::Type::Trace));

    bool captureTriggerEnabled = ui->triggeredCaptureChk->isChecked()
       , recordTriggerEnabled = ui->triggeredRecordingChk->isChecked();

    int captureTriggerPin = ui->triggeredCapturePinCombo->currentText().toInt()
        , recordTriggerPin = ui->triggeredRecordingCombo->currentText().toInt();
    
    bool pin2Used = (captureTriggerEnabled && (captureTriggerPin == 2)) || (recordTriggerEnabled && (recordTriggerPin == 2))
       , pin3Used = (captureTriggerEnabled && (captureTriggerPin == 3)) || (recordTriggerEnabled && (recordTriggerPin == 3));
    
    ui->strobeGpio2Chk->setEnabled(!pin2Used);
    ui->strobeGpio2Chk->setChecked(!pin2Used);
    ui->strobeGpio3Chk->setEnabled(!pin3Used);
    ui->strobeGpio3Chk->setChecked(!pin3Used);
}

void PointGreyCtl::triggerDelaySliderValueChanged(int sliderVal) {
	LogService::log(LogMessage("PointGreyCtl::triggerDelaySliderValueChanged", LogMessage::Type::Trace));

    double spinVal = static_cast<double>(sliderVal) / 10.0;

    ui->triggerCaptureDelaySpin->setValue(spinVal);
}

void PointGreyCtl::triggerDelaySpinValueChanged(double spinVal) {
	LogService::log(LogMessage("PointGreyCtl::triggerDelaySpinValueChanged", LogMessage::Type::Trace));

    int sliderVal = qRound(spinVal * 10.0);

    ui->triggerCaptureDelaySlider->setValue(sliderVal);
}

void PointGreyCtl::recordFolderClicked()
{
	LogService::log(LogMessage("PointGreyCtl::recordFolderClicked", LogMessage::Type::Trace));

    QString currPath = ui->recordFolderLabel->text();
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Select Folder..."), currPath, QFileDialog::ShowDirsOnly);
    if (!fileName.isEmpty()) {
        ui->recordFolderLabel->setText(fileName + QString::fromLocal8Bit("/"));
    }
}

void PointGreyCtl::removeProfileClicked()
{
	LogService::log(LogMessage("PointGreyCtl::removeProfileClicked", LogMessage::Type::Trace));

	QMessageBox::StandardButton click;
	QString title = "About to delete profile!";
	QString msg = "Are you sure you want to delete this profile?";
	click = QMessageBox::question(this, title, msg, QMessageBox::Yes|QMessageBox::Cancel);
	if (click == QMessageBox::Yes)
		m_settingsManager->removeProfile();
}

void PointGreyCtl::recordStateToggled(bool started)
{
	LogService::log(LogMessage("PointGreyCtl::recordStateToggled", LogMessage::Type::Trace));

    ui->frameRateSpin->setEnabled(!started);
}

void PointGreyCtl::saveProfile()
{
	LogService::log(LogMessage("PointGreyCtl::saveProfile", LogMessage::Type::Trace));

    applySettings();
    m_settingsManager->save();
}

void PointGreyCtl::saveProfileAs()
{
	LogService::log(LogMessage("PointGreyCtl::saveProfileAs", LogMessage::Type::Trace));

    QString profileName = QInputDialog::getText(this, "Save to New Profile", tr("Enter profile name:"));
    
    if (!profileName.isEmpty()) {
        bool canSave = true;

		if (m_settingsManager->profileNames().contains(profileName)) {
			QString title = "Profile exists with name: '" + profileName + "'!";
			QString msg = "Are you sure you want to overwrite existing profile: '" + profileName + "'?";
			QMessageBox::StandardButton click = QMessageBox::question(this, title, msg, QMessageBox::Yes | QMessageBox::Cancel);
            canSave = (click == QMessageBox::Yes);
		}

        if (canSave) {
			applySettings();
			m_settingsManager->save(profileName);
		}
    }
}

void PointGreyCtl::applySettings()
{
	LogService::log(LogMessage("PointGreyCtl::applySettings", LogMessage::Type::Trace));

    ProfileSettings ps = m_settingsManager->currentSettings();

    ps.cameraSettings.hdrSettings.enabled = ui->hdrCameraChk->isChecked();

    ps.cameraSettings.basicSettings.frameRate = ui->frameRateSpin->value();
    // Map the first slider set to the camera shutter/gain combo
    ps.cameraSettings.basicSettings.shutter = ui->mainShutterSpin->value();
    ps.cameraSettings.basicSettings.gain = ui->mainGainSpin->value();

    // trigger settings
    ps.cameraSettings.triggerSettings.triggeredCapture.enabled = ui->triggeredCaptureChk->isChecked();
    ps.cameraSettings.triggerSettings.triggeredCapture.mode = ui->triggeredCaptureModeCombo->currentText().toInt();
    ps.cameraSettings.triggerSettings.triggeredCapture.source = ui->triggeredCapturePinCombo->currentText().toInt();
    ps.cameraSettings.triggerSettings.triggeredCapture.polarity = ui->triggeredCapturePolarityCombo->currentIndex();
    ps.cameraSettings.triggerSettings.triggeredCapture.delayEnabled = ui->triggerCaptureDelaySpin->value() > 0.0;
    ps.cameraSettings.triggerSettings.triggeredCapture.delayS = ui->triggerCaptureDelaySpin->value() / 1000.0;

    ps.cameraSettings.triggerSettings.reservedInputPins.clear();
    ps.cameraSettings.triggerSettings.reservedInputPins << ui->triggeredRecordingCombo->currentText().toInt();

    ps.cameraSettings.triggerSettings.reservedInputPins.clear();
    ps.cameraSettings.triggerSettings.reservedInputPins << ui->triggeredRecordingCombo->currentText().toInt();

    QVector<TriggerStrobe> strobes;

    foreach (TriggerStrobe strobeSetting, ps.cameraSettings.triggerSettings.strobeSettings) {
        unsigned int pin = strobeSetting.source;

        QCheckBox * enabled = findChild<QCheckBox *>(TRIGGER_STROBE_ENABLED_NAME.arg(pin));
        if (enabled) strobeSetting.enabled = enabled->isChecked();

        QComboBox * polarity = findChild<QComboBox *>(TRIGGER_STROBE_POLARITY_NAME.arg(pin));
        if (polarity) strobeSetting.polarity = polarity->currentIndex();

        QDoubleSpinBox * delay = findChild<QDoubleSpinBox *>(TRIGGER_STROBE_DELAY_NAME.arg(pin));
        if (delay) strobeSetting.delayMs = delay->value();

        QDoubleSpinBox * duration = findChild<QDoubleSpinBox *>(TRIGGER_STROBE_DURATION_NAME.arg(pin));
        if (duration) strobeSetting.durationMs = duration->value();

        strobes << strobeSetting;
    }

    ps.cameraSettings.triggerSettings.strobeSettings = strobes;

    // HDR gain settings
    ps.cameraSettings.hdrSettings.gains.clear();
    foreach (QSpinBox *gainSpin, m_gainSpins) {
        ps.cameraSettings.hdrSettings.gains.push_back(gainSpin->value());
    }
    
    // HDR shutter settings
    ps.cameraSettings.hdrSettings.shutters.clear();
    foreach (QSpinBox *shutterSpin, m_shutterSpins) {
        ps.cameraSettings.hdrSettings.shutters.push_back(shutterSpin->value());
    }

    ps.cameraSettings.initialShutter = ui->initShutterSpin->value();
    ps.cameraSettings.deltaEv = ui->deltaEvSpin->value();

    ps.hdrSettings.enabled = ui->hdrProcessingChk->isChecked();
    ps.hdrSettings.shutters.clear();
    ps.hdrSettings.shutters = ps.cameraSettings.hdrSettings.shutters;

	ps.hdrSettings.gains.clear();
	ps.hdrSettings.gains = ps.cameraSettings.hdrSettings.gains;

    RecordSettings rs = m_settingsManager->currentSettings().recordSettings;
    rs.targetDir = ui->recordFolderLabel->text();
    rs.diskFullPercentThreshold = ui->recordStopThresholdSpin->value();

    switch(ui->recordCompressionCombo->currentIndex()) {
        case 0: rs.codec = "lossy"; break;
        case 1: rs.codec = "lossless"; break;
        default: rs.codec = "raw";
    }
    rs.frameRateOverride = ui->recordFrameRateOverrideChk->isChecked();
    rs.frameRate = ui->recordFrameRateSpin->value();
    rs.arcOnRecording = ui->triggeredRecordingChk->isChecked();
    rs.arcOnRecordingPin = ui->triggeredRecordingCombo->currentText().toInt();
	rs.timeInterval = ui->videoLengthDoubleSpinBox->value();

    ps.recordSettings = rs;

    m_settingsManager->setCurrentSettings(ps);
}