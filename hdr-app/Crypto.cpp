#include "Crypto.h"


Crypto::Crypto(QString & name, QObject * parent)
	: QObject(parent)
	, m_fileName("." + name + QString("_auth"))
{
}


Crypto::~Crypto()
{
}

QString Crypto::fileName()
{
	return m_fileName;
}

void Crypto::createHashStorageFile(QString & serial)
{
	QFile file(fileName());
	if (file.exists()) {
		return;
	}

	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QString hash = createHash(serial);

		QTextStream out(&file);
		out << hash << endl;
		out.flush();
	}
}

bool Crypto::authenticate(QString & serial)
{
	bool authenticated = false;

	QString storedHash = "";

	QFile file(fileName());
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream stream(&file);
		storedHash = stream.readLine();
	}

	QString hash = createHash(serial);
	if (hash == storedHash) {
		authenticated = true;
	}

	return authenticated;
}

QString Crypto::createHash(QString & serial)
{
	QByteArray dataByteArray = serial.toUtf8();
	const char* dataCharArray = dataByteArray.constData();

	unsigned char digest[SHA256_DIGEST_LENGTH];

	SHA256((unsigned char*)dataCharArray, strlen(dataCharArray), digest);

	char mdString[SHA256_DIGEST_LENGTH * 2 + 1];

	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
		sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);
	}

	QString hash = QString::fromUtf8(mdString);

	return hash;
}

