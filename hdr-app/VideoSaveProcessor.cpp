#include "VideoSaveProcessor.h"

// windows
#include <Windows.h>

#define TIMED_VIDEO

//#define PRINT

VideoSaveProcessor * VideoSaveProcessor::createVideoSave(const RecordSettings & recSettings, QObject * parent)
{
	LogService::log(LogMessage("VideoSaveProcessor::createVideoSave", LogMessage::Type::Trace));

	return new VideoSaveProcessor(recSettings, parent);
}

VideoSaveProcessor::VideoSaveProcessor(const RecordSettings & settings, QObject * parent)
: FrameProcessor(parent)
, m_settings(settings)
, m_vw()
, m_worker(ActiveQThread::createActiveQThread(10)) // hardcoded at 10 frames for now
, m_frameRateTimer(100, this)
, m_recordDest()
, m_flipped(false)
{
}

void VideoSaveProcessor::setState(FrameProcessor::State state)
{
	LogService::log(LogMessage("VideoSaveProcessor::setState", LogMessage::Type::Trace));

	FrameProcessor::State previousState = this->state(), nextState;

	if (previousState == state)
		return;

	try {
		if (previousState == FrameProcessor::UnloadedState) {
#ifdef TIMED_VIDEO
#else
			openWriter();
#endif
			nextState = FrameProcessor::LoadedState;

			if (state == FrameProcessor::ActiveState) {
				nextState = FrameProcessor::ActiveState;

				emit recordStarted(m_recordDest);
			}
		}
		else if (previousState == FrameProcessor::LoadedState) {
			if (state == FrameProcessor::UnloadedState) {
				closeWriter();
			}
			else if (state == FrameProcessor::ActiveState) {
				// nothing to do
			}

			nextState = state;
		}
		else if (previousState == FrameProcessor::ActiveState) {
			if (state == FrameProcessor::UnloadedState) {
				closeWriter();
			}
			else if (state == FrameProcessor::LoadedState) {
				// nothing to do
			}

			nextState = state;
		}

		changeState(nextState);
	}
	catch (std::runtime_error err) {
		QString msg = tr("Video save error: ") + tr(err.what());
		emit error(FrameProcessor::CameraError, msg);

		LogService::log(LogMessage(msg, LogMessage::Type::Error));
	}
}

void VideoSaveProcessor::sceneFlipped(bool flipped)
{
	LogService::log(LogMessage("VideoSaveProcessor::sceneFlipped", LogMessage::Type::Trace));

	m_flipped = flipped;
}

void VideoSaveProcessor::configWriter(QString & record_dest,
									  int & fourcc,
									  double & frame_rate,
									  cv::Size & size){

	//get frame rate info
	frame_rate = m_settings.frameRate;
	LogService::log(LogMessage(QString("Frame Rate: ") + QString::number(frame_rate), LogMessage::Type::RuntimeStats));
	if (!m_settings.frameRateOverride && m_frameRateTimer.isValid()) {
		frame_rate = 1.0 / m_frameRateTimer.lastIntervalAverage();
	}

	//set record_dest
	recordDest(record_dest);
	const std::string videoSaveFile = record_dest.toStdString();

	//get codec
	fourcc = videoWriteCodec();

	//get size
	size.width = m_settings.width;
	size.height = m_settings.height;

	qDebug() << __FUNCTION__ 
			 << " : destination = " 
			 << videoSaveFile.c_str() 
			 << ", fourcc = " 
			 << fourcc 
			 << ", frameRate = " 
			 << frame_rate;
}

int VideoSaveProcessor::videoWriteCodec(){
	int fourcc = CV_FOURCC('F', 'M', 'P', '4'); //lossy
	if (m_settings.codec.compare("lossless") == 0) {
		fourcc = CV_FOURCC('H', 'F', 'Y', 'U');
	}
	else if (m_settings.codec.compare("raw") == 0) {
		fourcc = CV_FOURCC('I', 'Y', 'U', 'V'); //raw
	}
	return fourcc;
}

QString current_time(bool year, bool month, bool day, bool hour, bool minute, bool second, bool msec){
	QString dest = "";

	bool prefix = false;

	QDate date = QDateTime::currentDateTime().date();
	if (year)
		dest += QString::number(date.year());

	prefix |= year;
	if (month)
		dest += ((prefix ? "-" : "") + QDate::shortMonthName(date.month()));

	prefix |= month;
	if (day)
		dest += ((prefix ? "-" : "") + QString::number(date.day()));

	QTime time = QDateTime::currentDateTime().time();

	prefix |= day;
	if (hour){
		dest += ((prefix ? "-" : "") + QString::number(time.hour()));
		dest += "h";
	}

	prefix |= hour;
	if (minute){
		dest += ((prefix ? "-" : "") + QString::number(time.minute()));
		dest += "m";
	}

	prefix |= minute;
	if (second){
		dest += ((prefix ? "-" : "") + QString::number(time.second()));
		dest += "s";
	}

	prefix |= second;
	if (msec){
		dest += ((prefix ? "-" : "") + QString::number(time.msec()));
		dest += "ms";
	}

	return dest;
}

void VideoSaveProcessor::recordDest(QString & dest){
	std::string folder_string;

	QString year_d = m_settings.targetDir 
		+ current_time(true, false, false, false, false, false, false);

	QString month_d = year_d 
		+ tr("/") + current_time(false, true, false, false, false, false, false);

	QString day_d = month_d
		+ tr("/") + current_time(false, false, true, false, false, false, false);

	folder_string = year_d.toStdString();
	if (CreateDirectoryA(folder_string.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		folder_string = month_d.toStdString();
		if (CreateDirectoryA(folder_string.c_str(), NULL) ||
			ERROR_ALREADY_EXISTS == GetLastError())
		{
			folder_string = day_d.toStdString();
			if (CreateDirectoryA(folder_string.c_str(), NULL) ||
				ERROR_ALREADY_EXISTS == GetLastError())
			{
				dest = day_d
					+ tr("/")
					+ tr("cap")
					+ tr("_") + current_time(true, true, true, true, true, true, false)
					+ tr(".avi");
			}
		}
	}

}

//NOTE: for Record Button
void VideoSaveProcessor::openWriter()
{
	LogService::log(LogMessage("VideoSaveProcessor::openWriter", LogMessage::Type::Trace));

	this->changeStatus(FrameProcessor::LoadingStatus);

	if (atDiskLimit()) {
		QString msg = tr("Cannot start capture. Target drive ") + QString::number(m_settings.diskFullPercentThreshold) + tr("% full.");
		LogService::log(LogMessage(msg, LogMessage::Type::Error));
		closeWriter();
		throw std::runtime_error(msg.toStdString().c_str());
	}

	m_worker->send(std::bind(&VideoSaveProcessor::openWriterTask, this, std::ref(m_vw)));
}

//NOTE: for Record Button
void VideoSaveProcessor::openWriterTask(cv::VideoWriter & vw)
{
	#ifdef PRINT
	printf("Task: opening writer\n");
	#endif

	int fourcc;
	double frameRate;
	cv::Size size;
	configWriter(m_recordDest, fourcc, frameRate, size);

	const std::string videoSaveFile = m_recordDest.toStdString();

	#ifdef PRINT
	printf("opening vw\n");
	#endif
	vw.open(videoSaveFile, fourcc, frameRate, size);

	#ifdef PRINT
	printf("opened vw\n");
	#endif

	if (!vw.isOpened()) {
		QString msg = tr("Unable to open capture for: ") + tr(videoSaveFile.c_str());
		LogService::log(LogMessage(msg, LogMessage::Type::Error));

		this->changeStatus(FrameProcessor::UnloadedStatus);

		throw std::runtime_error(msg.toStdString().c_str());
	}

	#ifdef PRINT
	printf("openWriterTask: ");
	#endif

	this->changeStatus(FrameProcessor::LoadedStatus);
}

//NOTE: for Record Button
void VideoSaveProcessor::closeWriter()
{
	LogService::log(LogMessage("VideoSaveProcessor::closeWriter", LogMessage::Type::Trace));

	#ifdef PRINT
	printf("closeWriter: ");
	#endif

	this->changeStatus(FrameProcessor::UnloadingStatus);

	// close the writer
	m_worker->send(std::bind(&VideoSaveProcessor::closeWriterTask, this, std::ref(m_vw)));
}

//NOTE: for Record Button
void VideoSaveProcessor::closeWriterTask(cv::VideoWriter & vw)
{
	LogService::log(LogMessage("VideoSaveProcessor::closeWriterTask", LogMessage::Type::Trace));

	#ifdef PRINT
	printf("closeWriterTask: ");
	#endif

	this->changeStatus(FrameProcessor::UnloadingStatus);

	vw.release();

	#ifdef PRINT
	printf("closeWriterTask: ");
	#endif

	this->changeStatus(FrameProcessor::UnloadedStatus);

	#ifdef PRINT
	printf("closeWriterTask: ");
	#endif

	this->changeState(FrameProcessor::UnloadedState);

	emit recordStopped();
}

void VideoSaveProcessor::resetWriter()
{
	LogService::log(LogMessage("VideoSaveProcessor::resetWriter", LogMessage::Type::Trace));

	//if (m_worker->ready())
	#ifdef PRINT
	printf("resetWriter: waiting for VideoSaveProc m_worker...\n");
	#endif

	while (!m_worker->ready()){}

	#ifdef PRINT
	printf("resetWriter: m_worker ready to send resetWriterTask...\n");
	#endif

	if (state() == FrameProcessor::ActiveState)
		m_worker->send(std::bind(&VideoSaveProcessor::resetWriterTask, this, std::ref(m_vw)));
}

void VideoSaveProcessor::resetWriterTask(cv::VideoWriter & vw)
{
	#ifdef PRINT
	printf("resetWriterTask: ");
	#endif

	#ifdef PRINT
	printf("releasing vw\n");
	#endif

	vw.release();

	#ifdef PRINT
	printf("released vw\n");
	#endif

	int fourcc;
	double frameRate;
	cv::Size size;
	configWriter(m_recordDest, fourcc, frameRate, size);

	const std::string videoSaveFile = m_recordDest.toStdString();

	#ifdef PRINT
	printf("opening vw\n");
	#endif

	vw.open(videoSaveFile, fourcc, frameRate, size);

	#ifdef PRINT
	printf("opened vw\n");
	#endif

	if (!vw.isOpened()) {
		QString msg = tr("Unable to open capture for: ") + tr(videoSaveFile.c_str());
		LogService::log(LogMessage(msg, LogMessage::Type::Error));
		this->changeStatus(FrameProcessor::UnloadedStatus);
		throw std::runtime_error(msg.toStdString().c_str());
	}

	#ifdef PRINT
	printf("resetWriterTask: ");
	#endif
}

void VideoSaveProcessor::writeFrameTask(cv::VideoWriter & vw, AnnotatedFrame frame)
{
	LogService::log(LogMessage("VideoSaveProcessor::writeFrameTask", LogMessage::Type::Trace));

	QImage qimg = frame.getFrame().rgbSwapped();

	if (m_flipped) qimg = qimg.mirrored();

	cv::Mat cvimg(qimg.height(), qimg.width(), CV_8UC3, qimg.bits(), qimg.bytesPerLine());

	if (state() == FrameProcessor::ActiveState && vw.isOpened())
	{
#ifdef PRINT
		printf("Before write\n");
#endif
		vw.write(cvimg);
#ifdef PRINT
		printf("After write\n");
#endif
	}
}

void VideoSaveProcessor::process(AnnotatedFrame frame)
{
	LogService::log(LogMessage("VideoSaveProcessor::process", LogMessage::Type::Trace));

	m_frameRateTimer.eventOccurred();

	if (status() == FrameProcessor::UnloadingStatus) {
		return;
	}

	if (state() == FrameProcessor::ActiveState) {
		if (atDiskLimit()) {
			changeState(FrameProcessor::UnloadedState);
			LogService::log(LogMessage(tr("Stopped capture. Target drive ") + QString::number(m_settings.diskFullPercentThreshold) + tr("% full."), LogMessage::Type::Error));
			closeWriter();
			emit error(FrameProcessor::CameraError, tr("Stopped capture. Target drive ") + QString::number(m_settings.diskFullPercentThreshold) + tr("% full."));
			return;
		}

		// if we are in arc-on mode and our pin went low, stop the recording
		if (m_settings.arcOnRecording && !arcOnRecordingActive(frame.metadata())) {
			setState(FrameProcessor::UnloadedState);
			return;
		}

		if (m_worker->ready()) {
			m_worker->trySend(std::bind(&VideoSaveProcessor::writeFrameTask, this, std::ref(m_vw), frame));
		}
		else {
			LogService::log(LogMessage("Frame dropped in recording.", LogMessage::Type::Warning));
			emit warning(FrameProcessor::UnableToProcessFrameWarning, "Frame dropped in recording.");
		}
	}
	else {
		if (m_settings.arcOnRecording && arcOnRecordingActive(frame.metadata())) {
			setState(FrameProcessor::ActiveState);
		}
	}
}

bool VideoSaveProcessor::arcOnRecordingActive(const AnnotatedFrame::FrameMetadata & metadata)
{
	LogService::log(LogMessage("VideoSaveProcessor::arcOnRecordingActive", LogMessage::Type::Trace));

	return metadata.gpioPinStates[m_settings.arcOnRecordingPin];
}

bool VideoSaveProcessor::atDiskLimit() 
{
	LogService::log(LogMessage("VideoSaveProcessor::atDiskLimit", LogMessage::Type::Trace));

	// physical drive threshold
	boost::filesystem::path p(m_settings.targetDir.toStdString());
	boost::filesystem::space_info s = boost::filesystem::space(p);

	return (static_cast<double>(s.available) / s.capacity) <= ((100.0 - m_settings.diskFullPercentThreshold) / 100.0);
}

void VideoSaveProcessor::settingsChanged(const RecordSettings & settings)
{
	LogService::log(LogMessage("VideoSaveProcessor::settingsChanged", LogMessage::Type::Trace));

	m_settings = settings;
}