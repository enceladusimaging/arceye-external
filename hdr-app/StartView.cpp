#include "StartView.h"
#include "ui_StartView.h"

StartView::StartView(QWidget *parent) 
    : QWidget(parent) 
    , ui(new Ui::StartView)
{ 
    // Setup UI
    ui->setupUi(this);
    connect(ui->startButton, SIGNAL(clicked()), this, SIGNAL(startClicked()));
}

void StartView::usbConnected(bool isConnected) 
{
	LogService::log(LogMessage("StartView::usbConnected", LogMessage::Type::Trace));
	
	ui->usbLabel->setEnabled(isConnected);
	ui->lightLabel->setEnabled(isConnected);
#ifdef BYPASS_MODE
	ui->startButton->setEnabled(true);
#else
	ui->startButton->setEnabled(isConnected);
#endif
}
