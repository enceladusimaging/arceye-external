#pragma once

#include <stdexcept>

#include <QImage>
#include <QObject>
#include <QPointer>
#include <QString>

#include "AnnotatedFrame.h"
#include "FrameProcessor.h"
#include "Logger.h"
#include "WorkerSettings.h"

enum WorkerState { Uninitialized, Ready, Started, Stopped };

class FrameProcessorWorker : public QObject
{
	Q_OBJECT

public:
	FrameProcessorWorker(QPointer<FrameProcessor> processor);

	inline WorkerState getState() const { return state; }; 

private:
	QPointer<FrameProcessor> frameProc;
	WorkerState state;
    Logger &log;

public slots:
	void initialize(const WorkerSettings &);
	void start();
	void stop();
	void changeSettings(const WorkerSettings &);
	void produce();
	void process(const AnnotatedFrame &);

signals:
	void initialized();
	void initializationError(const QString &);
	void started();
	void startError(const QString &);
	void stopped();
	void stopError(const QString &);
	void settingsChanged(const WorkerSettings &);
	void settingsChangeError(const QString &);
	void processingStarted();
	void processingError(const QString &);
	void processingEnded(const AnnotatedFrame &);
};

