#include "CaptureProcessor.h"

#include "AltVideo.h"

// windows
#include <Windows.h>

CaptureProcessor * CaptureProcessor::createCapture(const FlyCapture2::PGRGuid &camGuid, const CameraSettings & settings, QObject * parent)
{
	LogService::log(LogMessage("CaptureProcessor::createCapture", LogMessage::Type::Trace));

	return new CaptureProcessor(camGuid, settings, parent);
}

static void pgImageCapturedCallback(FlyCapture2::Image *pImage, const void *pCallbackData)
{
	// Check that this argument is set when the callback is assigned
	CaptureProcessor *session = static_cast<CaptureProcessor*>(const_cast<void *>(pCallbackData));

	if (session->state() != FrameProcessor::ActiveState) {
		return;
	}
	
	FlyCapture2::Image rgbImg;

	// convert to RGB
	FlyCapture2::Error err = pImage->Convert(FlyCapture2::PIXEL_FORMAT_RGB, &rgbImg);

	if (err != FlyCapture2::PGRERROR_OK) {
		qWarning() << "Error converting camera image: " << err.GetDescription() << endl;

		//We are in a worker thread here so emit signal inside the main thread
		QMetaObject::invokeMethod(session, "checkAndThrow", Qt::QueuedConnection,
			Q_ARG(FlyCapture2::Error, err));

		return;
	}

	FlyCapture2::ImageMetadata metadata = pImage->GetMetadata();

	session->pgFrameAvailable(rgbImg, metadata);
}

CaptureProcessor::CaptureProcessor(const FlyCapture2::PGRGuid & camGuid, const CameraSettings & settings, QObject *parent)
: FrameProcessor(parent)
, m_cam(new FlyCapture2::Camera)
, m_camGuid(camGuid)
, m_settings(settings)
, m_cameraStatsUpdateTimer(new QTimer(this))
, m_time(0)
, m_reset_interval(10000)
, m_timer_active(false)
, m_alt(nullptr)
{
	connect(m_cameraStatsUpdateTimer, SIGNAL(timeout()), this, SLOT(cameraTemperature()));
}

void CaptureProcessor::setState(FrameProcessor::State state)
{
	LogService::log(LogMessage("CaptureProcessor::setState", LogMessage::Type::Trace));

	FrameProcessor::State previousState = this->state(), nextState = this->state();

	if (previousState == state)
		return;

	try {
		if (previousState == FrameProcessor::UnloadedState) {
			openCamera();
			nextState = FrameProcessor::LoadedState;

			applyConfiguration();

			if (state == FrameProcessor::ActiveState) {
				nextState = FrameProcessor::ActiveState;
				startCapture();
			}
		}
		else if (previousState == FrameProcessor::LoadedState) {
			nextState = state;

			if (nextState == FrameProcessor::UnloadedState) {
				closeCamera();
			}
			else if (nextState == FrameProcessor::ActiveState) {
				startCapture();
			}
		}
		else if (previousState == FrameProcessor::ActiveState) {
			nextState = state;

			stopCapture();

			if (nextState == FrameProcessor::UnloadedState) {
				closeCamera();
			}
		}

		this->changeState(nextState);
	}
	catch (std::runtime_error e) {
#ifdef BYPASS_MODE
		if (previousState == FrameProcessor::UnloadedState) {
			nextState = FrameProcessor::LoadedState;

			if (state == FrameProcessor::ActiveState) {
				nextState = FrameProcessor::ActiveState;
				startCapture();
			}
		}
		this->changeState(nextState);
#else
		emit error(FrameProcessor::CameraError, e.what());
#endif
	}
}

bool CaptureProcessor::isError(const FlyCapture2::Error & err) const
{
	LogService::log(LogMessage("CaptureProcessor::isError", LogMessage::Type::Trace));

	return (err != FlyCapture2::PGRERROR_OK);
}

void CaptureProcessor::checkAndThrow(const FlyCapture2::Error & err)
{
	LogService::log(LogMessage("CaptureProcessor::checkAndThrow", LogMessage::Type::Trace));

	if (isError(err)) {
#ifdef _DEBUG
		err.PrintErrorTrace();
#endif

		QString msg = tr("Camera error: ") + tr(err.GetDescription());
		std::cout << msg.toStdString() << endl;
		throw std::runtime_error(msg.toStdString().c_str());
	}
}

void CaptureProcessor::checkThrowChangeStatus(const FlyCapture2::Error & err, const FrameProcessor::Status status)
{
	LogService::log(LogMessage("CaptureProcessor::checkThrowChangeStatus", LogMessage::Type::Trace));

	if (isError(err)) {
#ifdef _DEBUG
		err.PrintErrorTrace();
#endif
		this->changeStatus(status);

		QString msg = tr("Camera error: ") + tr(err.GetDescription());
		throw std::runtime_error(msg.toStdString().c_str());
	}

}

void CaptureProcessor::cameraTemperature()
{
	LogService::log(LogMessage("CaptureProcessor::cameraTemperature", LogMessage::Type::Trace));

	if (m_cam->IsConnected()) {
		try {
			FlyCapture2::CameraStats stats;
			checkAndThrow(m_cam->GetStats(&stats));

			emit temperatureChanged((stats.temperature / 10.0 - 273.0));
		}
		catch (const std::runtime_error & re) {
			emit error(FrameProcessor::CameraError, QString::fromLocal8Bit("Error reading temperature from camera."));
		}
	}
}

void CaptureProcessor::pgFrameAvailable(FlyCapture2::Image img, FlyCapture2::ImageMetadata pgMetadata)
{
	// fish the data out of the frame
	// copy the frame to local memory as PG will release it once we return from the callback context
	unsigned int size = img.GetDataSize(), height = img.GetRows(), width = img.GetCols();
	std::shared_ptr<unsigned char> imgData(new unsigned char[size]);
	std::memcpy(imgData.get(), img.GetData(), size);

	// copy the relevant PG metadata to our structure
	AnnotatedFrame::FrameMetadata metadata;
	metadata.shutter = pgMetadata.embeddedShutter & 0xFFFF;
	metadata.gpioPinStates << (0x80000000 & pgMetadata.embeddedGPIOPinState); // pin 0
	metadata.gpioPinStates << (0x40000000 & pgMetadata.embeddedGPIOPinState); // pin 1
	metadata.gpioPinStates << (0x20000000 & pgMetadata.embeddedGPIOPinState); // pin 2
	metadata.gpioPinStates << (0x10000000 & pgMetadata.embeddedGPIOPinState); // pin 3

	m_lastFrame = AnnotatedFrame(imgData, size, width, height, metadata);

	if (m_timer_active){
		qint64 current_time = QDateTime::currentMSecsSinceEpoch();
		if (abs(current_time - m_time) > m_reset_interval){
			m_time = current_time;
			emit resetVideoSaveProcessor();
		}
	}
	else{
		m_time = 0;
	}

	emit frameAvailable(m_lastFrame);
}

void CaptureProcessor::convertFrame(const cv::Mat& img){
	AnnotatedFrame::FrameMetadata metadata;
	unsigned int size = img.rows * img.cols * img.channels(), height = img.rows, width = img.cols;
	std::shared_ptr<unsigned char> imgData(new unsigned char[size]);
	std::memcpy(imgData.get(), img.data, size);
	m_lastFrame = AnnotatedFrame(imgData, size, width, height, metadata);

	emit frameAvailable(m_lastFrame);
}

void CaptureProcessor::settingsChanged(const RecordSettings& settings){
	unsigned int value = settings.timeInterval;
	if (value > 0){
		m_reset_interval = (qint64)(value * 60 * 1000);
	}
}

void CaptureProcessor::recordStateToggled(const bool toggled){
	m_timer_active = toggled;
}

void CaptureProcessor::openCamera()
{
	LogService::log(LogMessage("CaptureProcessor::openCamera", LogMessage::Type::Trace));

	this->changeStatus(FrameProcessor::LoadingStatus);

	// Connect to a camera
	checkThrowChangeStatus(m_cam->Connect(&m_camGuid), FrameProcessor::UnloadedStatus);

	// Get the camera information
	FlyCapture2::CameraInfo camInfo;
	checkThrowChangeStatus(m_cam->GetCameraInfo(&camInfo), FrameProcessor::UnloadedStatus);

	printCameraInfo(camInfo);

	this->changeStatus(FrameProcessor::LoadedStatus);
}

void CaptureProcessor::startCapture()
{
	LogService::log(LogMessage("CaptureProcessor::startCapture", LogMessage::Type::Trace));

	m_cameraStatsUpdateTimer->start(m_statsUpdateIntervalMs);

	this->changeStatus(FrameProcessor::StartingStatus);

#ifdef BYPASS_MODE
	if (m_alt == nullptr){
		m_alt = new AltVideo(this);
	}

	if (m_alt != nullptr){
		connect(m_alt, SIGNAL(frameArrived(const cv::Mat&)), this, SLOT(convertFrame(const cv::Mat&)), Qt::QueuedConnection);
		m_alt->start();
	}
#else
	checkThrowChangeStatus(m_cam->StartCapture(pgImageCapturedCallback, this), FrameProcessor::StoppingStatus);
#endif

	this->changeStatus(FrameProcessor::ActiveStatus);
}

void CaptureProcessor::stopCapture()
{
	LogService::log(LogMessage("CaptureProcessor::stopCapture", LogMessage::Type::Trace));

	if (m_cam->IsConnected()) {
		this->changeStatus(FrameProcessor::StoppingStatus);

		// Don't check error code as this may fail when camera is removed
		m_cameraStatsUpdateTimer->stop();
#ifdef BYPASS_MODE
		m_alt->stop();
#else
		m_cam->StopCapture();
#endif

		this->changeStatus(FrameProcessor::LoadedStatus);
	}
}

void CaptureProcessor::closeCamera()
{
	LogService::log(LogMessage("CaptureProcessor::closeCamera", LogMessage::Type::Trace));

	if (m_cam->IsConnected()) {
		this->changeStatus(FrameProcessor::UnloadingStatus);

		toggleHDRMode(false);

		stopCapture();

		// Disconnect the camera
		checkThrowChangeStatus(m_cam->Disconnect(), FrameProcessor::UnloadedStatus);

		this->changeStatus(FrameProcessor::UnloadedStatus);
	}
}

void CaptureProcessor::settingsChanged(const CameraSettings & settings)
{
	LogService::log(LogMessage("CaptureProcessor::settingsChanged", LogMessage::Type::Trace));

	m_settings = settings;

	try {
		bool restartCapture = (FrameProcessor::ActiveState == state());

		if (restartCapture) setState(FrameProcessor::LoadedState);

		applyConfiguration();

		if (restartCapture) setState(FrameProcessor::ActiveState);
	}
	catch (std::runtime_error e) {
		emit error(FrameProcessor::CameraError, e.what());
	}
}

void CaptureProcessor::applyConfiguration()
{
	LogService::log(LogMessage("CaptureProcessor::applyConfiguration", LogMessage::Type::Trace));

	// Bug in the CMOSIS CMV4000 Grasshoper 3: enabling the trigger resets the camera fps. We need to set the FPS after
	// altering the trigger settings

	// Camera Settings
	applyCameraProperties();

	// Custom Video Modes
	applyF7Settings();

	// Advanced Camera Settings
	applyEmbeddedInfoSettings();

	// HDR Settings
	applyRegisterSettings();

	// Trigger Settings
	applyTriggerSettings();
}

void CaptureProcessor::applyCameraProperties()
{
	LogService::log(LogMessage("CaptureProcessor::applyCameraProperties", LogMessage::Type::Trace));

	CameraBasicSettings cbs = m_settings.basicSettings;
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::BRIGHTNESS, cbs.brightness)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::AUTO_EXPOSURE, cbs.exposure, cbs.exposureAuto, cbs.exposureEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::SHARPNESS, cbs.sharpness, cbs.sharpnessAuto, cbs.sharpnessEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::HUE, cbs.hue, false, cbs.hueEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::SATURATION, cbs.saturation, cbs.saturationAuto, cbs.saturationEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::GAMMA, cbs.gamma, false, cbs.gammaEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::SHUTTER, cbs.shutter, cbs.shutterAuto, cbs.shutterEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::GAIN, cbs.gain, cbs.gainAuto, cbs.gainEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::FRAME_RATE, cbs.frameRate, cbs.frameRateAuto, cbs.frameRateEnabled)));
	checkAndThrow(m_cam->SetProperty(&createProperty(FlyCapture2::WHITE_BALANCE, cbs.whiteBalRed, cbs.whiteBalBlue, cbs.whiteBalAuto, cbs.whiteBalEnabled)));

}

//TO DO FORMAT 7 AAAAAAAAAAAAAAAAAAAAAA
void CaptureProcessor::applyF7Settings()
{
	LogService::log(LogMessage("CaptureProcessor::applyF7Settings", LogMessage::Type::Trace));

	// Save the current settings
	FlyCapture2::VideoMode currVideoMode;
	FlyCapture2::FrameRate currFrameRate;
	FlyCapture2::Format7ImageSettings currFmt7Settings;
	unsigned int currPacketSize = 0;

	checkAndThrow(m_cam->GetVideoModeAndFrameRate(&currVideoMode, &currFrameRate));

	if (currVideoMode == FlyCapture2::VIDEOMODE_FORMAT7) {
		// Get the current Format 7 settings
		float percentage; // Don't need to keep this
		checkAndThrow(m_cam->GetFormat7Configuration(&currFmt7Settings, &currPacketSize, &percentage));
	}

	// Populate a new FMT7 settings object with our settings
	FlyCapture2::Format7ImageSettings newFmt7Settings;
	// default to mode 0 until further modes are supported, regardless of what is passed in
	newFmt7Settings.mode = FlyCapture2::MODE_7;//FlyCapture2::MODE_0;
	// default to RGB 8 pixel mode regardless of what is passed in
	newFmt7Settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_RGB8;
	newFmt7Settings.offsetX = m_settings.videoMode.left;
	newFmt7Settings.offsetY = m_settings.videoMode.top;
	newFmt7Settings.width = m_settings.videoMode.width;
	newFmt7Settings.height = m_settings.videoMode.height;

	bool settingsAreValid;
	FlyCapture2::Format7PacketInfo fmt7PacketInfo;
	checkAndThrow(m_cam->ValidateFormat7Settings(&newFmt7Settings, &settingsAreValid, &fmt7PacketInfo));

	if (settingsAreValid) {
		// Break our usual flow to set the Format7 settings
		// Attempt to apply new settings and if they fail, revert. Then throw the error that occurred.

		FlyCapture2::Error error = m_cam->SetFormat7Configuration(&newFmt7Settings, fmt7PacketInfo.recommendedBytesPerPacket);

		if (error != FlyCapture2::PGRERROR_OK) {
			if (currVideoMode == FlyCapture2::VIDEOMODE_FORMAT7) {
				// The camera was in Format7, so set it back to Format7
				error = m_cam->SetFormat7Configuration(&currFmt7Settings, currPacketSize);
			}
			else {
				// Set the camera back to original DCAM video mode and frame rate
				error = m_cam->SetVideoModeAndFrameRate(currVideoMode, currFrameRate);
			}

			if (error != FlyCapture2::PGRERROR_OK) {
				checkAndThrow(error);
			}
		}
	}
}

void CaptureProcessor::applyEmbeddedInfoSettings()
{
	LogService::log(LogMessage("CaptureProcessor::applyEmbeddedInfoSettings", LogMessage::Type::Trace));

	FlyCapture2::EmbeddedImageInfo embeddedInfo;

	// read existing configuration - this will tell us if a value is supported
	checkAndThrow(m_cam->GetEmbeddedImageInfo(&embeddedInfo));

	CameraAdvancedSettings as = m_settings.advancedSettings;

	if (embeddedInfo.timestamp.available) {
		embeddedInfo.timestamp.onOff = as.embeddedTimestamp;
	}

	if (embeddedInfo.gain.available) {
		embeddedInfo.gain.onOff = as.embeddedGain;
	}

	if (embeddedInfo.shutter.available) {
		embeddedInfo.shutter.onOff = as.embeddedShutter;
	}

	if (embeddedInfo.brightness.available) {
		embeddedInfo.brightness.onOff = as.embeddedBrightness;
	}

	if (embeddedInfo.exposure.available) {
		embeddedInfo.exposure.onOff = as.embeddedExposure;
	}

	if (embeddedInfo.whiteBalance.available) {
		embeddedInfo.whiteBalance.onOff = as.embeddedWhiteBal;
	}

	if (embeddedInfo.frameCounter.available) {
		embeddedInfo.frameCounter.onOff = as.embeddedFrameCnt;
	}

	if (embeddedInfo.strobePattern.available) {
		embeddedInfo.strobePattern.onOff = as.embeddedStrobe;
	}

	if (embeddedInfo.GPIOPinState.available) {
		embeddedInfo.GPIOPinState.onOff = as.embeddedGpioPinState;
	}

	if (embeddedInfo.ROIPosition.available) {
		embeddedInfo.ROIPosition.onOff = as.embeddedRoiPos;
	}

	checkAndThrow(m_cam->SetEmbeddedImageInfo(&embeddedInfo));
}

void CaptureProcessor::applyRegisterSettings()
{
	LogService::log(LogMessage("CaptureProcessor::applyRegisterSettings", LogMessage::Type::Trace));

	toggleHDRMode(m_settings.hdrSettings.enabled);

	checkAndThrow(m_cam->WriteRegister(k_HDRShutter1, m_settings.hdrSettings.shutters.at(0)));
	checkAndThrow(m_cam->WriteRegister(k_HDRShutter2, m_settings.hdrSettings.shutters.at(1)));
	checkAndThrow(m_cam->WriteRegister(k_HDRShutter3, m_settings.hdrSettings.shutters.at(2)));
	checkAndThrow(m_cam->WriteRegister(k_HDRShutter4, m_settings.hdrSettings.shutters.at(3)));

	checkAndThrow(m_cam->WriteRegister(k_HDRGain1, m_settings.hdrSettings.gains.at(0)));
	checkAndThrow(m_cam->WriteRegister(k_HDRGain2, m_settings.hdrSettings.gains.at(1)));
	checkAndThrow(m_cam->WriteRegister(k_HDRGain3, m_settings.hdrSettings.gains.at(2)));
	checkAndThrow(m_cam->WriteRegister(k_HDRGain4, m_settings.hdrSettings.gains.at(3)));
}

void CaptureProcessor::applyTriggerSettings()
{
	LogService::log(LogMessage("CaptureProcessor::applyTriggerSettings", LogMessage::Type::Trace));

	// default pin directions - 0 for input, 1 for output
	// all output except for pin0 which is a dedicated input pin
	QVector<unsigned int> gpioDirections;
	gpioDirections << 0 << 1 << 1 << 1;

	// flip the reserved input pins
	foreach(unsigned int pin, m_settings.triggerSettings.reservedInputPins) {
		gpioDirections[pin] = 0;
	}

	bool asyncCaptureEnabled = m_settings.triggerSettings.triggeredCapture.enabled;
	unsigned int asyncCapturePin = m_settings.triggerSettings.triggeredCapture.source;
	FlyCapture2::TriggerMode triggerMode;
	triggerMode.onOff = asyncCaptureEnabled;
	triggerMode.mode = m_settings.triggerSettings.triggeredCapture.mode;
	triggerMode.parameter = m_settings.triggerSettings.triggeredCapture.param;
	triggerMode.source = asyncCapturePin; //0, 2, 3 for external trigger, 7 for software
	triggerMode.polarity = m_settings.triggerSettings.triggeredCapture.polarity; // 0 for low, 1 for high

	checkAndThrow(m_cam->SetTriggerMode(&triggerMode));

	// set trigger pin to input if trigger enabled
	if (asyncCaptureEnabled) gpioDirections[asyncCapturePin] = 0;

	FlyCapture2::TriggerDelay triggerDelay;
	triggerDelay.type = FlyCapture2::TRIGGER_DELAY;
	triggerDelay.absControl = true;
	triggerDelay.absValue = m_settings.triggerSettings.triggeredCapture.delayS;
	triggerDelay.onOff = m_settings.triggerSettings.triggeredCapture.enabled;

	checkAndThrow(m_cam->SetTriggerDelay(&triggerDelay));

	// strobe can only be set for pins 1-3
	QVector<TriggerStrobe> strobeSettings = m_settings.triggerSettings.strobeSettings;
	FlyCapture2::StrobeControl strobeControl;
	FlyCapture2::StrobeInfo strobeInfo;
	foreach(TriggerStrobe strobe, strobeSettings) {
		unsigned int pin = strobe.source;
		strobeInfo.source = pin;
		checkAndThrow(m_cam->GetStrobeInfo(&strobeInfo));

		// set the strobe control if it's supported on the current pin
		if (strobeInfo.present) {
			strobeControl.onOff = strobe.enabled && gpioDirections[pin];
			strobeControl.source = pin;
			strobeControl.polarity = strobe.polarity;
			strobeControl.delay = strobe.delayMs;
			strobeControl.duration = strobe.durationMs;

			// mark the pin as output
			if (strobeSettings[pin].enabled) gpioDirections[pin] = 1;

			checkAndThrow(m_cam->SetStrobe(&strobeControl));
		}
	}

	// Pin direction is determined by the state of the async capture and strobe
	for (int i = 0; i < gpioDirections.size(); i++) {
		checkAndThrow(m_cam->SetGPIOPinDirection(i, gpioDirections[i]));
	}
}

FlyCapture2::Property CaptureProcessor::createProperty(FlyCapture2::PropertyType prop, double val, bool autoManual, bool enabled)
{
	LogService::log(LogMessage("CaptureProcessor::createProperty", LogMessage::Type::Trace));

	FlyCapture2::Property p;

	p.type = prop;
	p.onePush = false;
	p.onOff = enabled;
	p.autoManualMode = autoManual;
	p.absControl = true;
	p.absValue = val;

	return p;
}

FlyCapture2::Property CaptureProcessor::createProperty(FlyCapture2::PropertyType prop, unsigned int val, bool autoManual, bool enabled)
{
	LogService::log(LogMessage("CaptureProcessor::createProperty", LogMessage::Type::Trace));

	FlyCapture2::Property p;

	p.type = prop;
	p.onePush = false;
	p.onOff = enabled;
	p.autoManualMode = autoManual;
	p.absControl = false;
	p.valueA = val;

	return p;
}

FlyCapture2::Property CaptureProcessor::createProperty(FlyCapture2::PropertyType prop, unsigned int value_a, unsigned int value_b, bool autoManual, bool enabled)
{
	LogService::log(LogMessage("CaptureProcessor::createProperty", LogMessage::Type::Trace));

	FlyCapture2::Property p;

	p.type = prop;
	p.onePush = false;
	p.onOff = enabled;
	p.autoManualMode = autoManual;
	p.absControl = false;
	p.valueA = value_a;
	p.valueB = value_b;

	return p;
}

void CaptureProcessor::toggleHDRMode(bool hdrOn)
{
	LogService::log(LogMessage("CaptureProcessor::toggleHDRMode", LogMessage::Type::Trace));

	checkAndThrow(m_cam->WriteRegister(k_HDRCtrl, hdrOn ? k_HDROn : k_HDROff));
}

void CaptureProcessor::printCameraInfo(const FlyCapture2::CameraInfo & pCamInfo)
{
	LogService::log(LogMessage("CaptureProcessor::printCameraInfo", LogMessage::Type::Trace));

#ifdef _DEBUG
	qDebug() <<
		"\n*** CAMERA INFORMATION ***\n"
		"Serial number -" << pCamInfo.serialNumber << "\n"
		"Camera model - " << pCamInfo.modelName << "\n"
		"Camera vendor - " << pCamInfo.vendorName << "\n"
		"Sensor - " << pCamInfo.sensorInfo << "\n"
		"Resolution - " << pCamInfo.sensorResolution << "\n"
		"Firmware version - " << pCamInfo.firmwareVersion << "\n"
		"Firmware build time - " << pCamInfo.firmwareBuildTime << "\n\n";
#endif
}