#include "ActiveQThread.h"
#include "Settings.h"

#include <QObject>

#include "boost/filesystem.hpp"

class StopWatch : public QObject
{
	Q_OBJECT

public:
	StopWatch(QObject* parent);

signals :
	void reset();

	public slots:
	void startTimer();
	void stopTimer();
	void resetTimer();
	void startStop(const bool);
	void settingsChanged(const RecordSettings&);

private:
	void runTimer();

	qint64 m_time;
	qint64 m_reset_interval;
	bool m_active;
	std::unique_ptr<ActiveQThread> m_worker;
};