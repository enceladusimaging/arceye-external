#include "MainWindowStatusBar.h"
#include "ui_MainWindowStatusBar.h"

MainWindowStatusBar::MainWindowStatusBar(CaptureProcessor * cap, const CameraSettings & cameraSettings, QWidget * parent)
    : QWidget(parent)
    , ui(new Ui::MainWindowStatusBar)
    , m_cap(cap)
{
    ui->setupUi(this);

    updateFromCameraSettings(cameraSettings);
}

void MainWindowStatusBar::displayError(const QString & msg)
{
	LogService::log(LogMessage("MainWindowStatusBar::displayError", LogMessage::Type::Trace));

    message(msg, ui->messageLabel, LedState::Critical);
    QTimer::singleShot(2000, this, SLOT(clearError()));
}

void MainWindowStatusBar::displayWarning(const QString & msg)
{
	LogService::log(LogMessage("MainWindowStatusBar::displayWarning", LogMessage::Type::Trace));

    message(msg, ui->messageLabel, LedState::Warning);
    QTimer::singleShot(2000, this, SLOT(clearError()));
}

void MainWindowStatusBar::updateFromCameraSettings(const CameraSettings & cs)
{
	LogService::log(LogMessage("MainWindowStatusBar::updateFromCameraSettings", LogMessage::Type::Trace));

    const QString fps = QString::number(cs.basicSettings.frameRate) + QString::fromLocal8Bit(" FPS");
    message(fps, ui->fpsLabel);

    const QString resolution = QString::number(cs.videoMode.width) + QString::fromLocal8Bit("x") + QString::number(cs.videoMode.height);
    message(resolution, ui->resolutionLabel);

    const QString triggerState = QString::fromLocal8Bit("Trigger: ") + (cs.triggerSettings.triggeredCapture.enabled ? QString::fromLocal8Bit("ON") : QString::fromLocal8Bit("OFF"));
    message(triggerState, ui->triggerStateLabel);

    QString shutters = QString::fromLocal8Bit("Shutter: ");
    if (cs.hdrSettings.enabled) {
        shutters += QString::number(cs.hdrSettings.shutters[0]) 
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.shutters[1])
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.shutters[2])
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.shutters[3]);
    } else {
        shutters += QString::number(cs.basicSettings.shutter);
    }
    message(shutters, ui->shutterLabel);

    QString gains = QString::fromLocal8Bit("Gain: ");
    if (cs.hdrSettings.enabled) {
        gains += QString::number(cs.hdrSettings.gains[0]) 
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.gains[1])
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.gains[2])
            + QString::fromLocal8Bit(", ") + QString::number(cs.hdrSettings.gains[3]);
    } else {
        gains += QString::number(cs.basicSettings.gain);
    }
    message(gains, ui->gainLabel);
}

void MainWindowStatusBar::message(const QString & msg, QLabel * targetLabel, LedState state)
{
	LogService::log(LogMessage("MainWindowStatusBar::message", LogMessage::Type::Trace));

    switch(state) {
    case LedState::Normal: ui->ledLabel->setPixmap(QPixmap(QString::fromUtf8(":/StatusBar/Resources/led-green.png"))); break;
    case LedState::Warning: ui->ledLabel->setPixmap(QPixmap(QString::fromUtf8(":/StatusBar/Resources/led-red.png"))); break;
    case LedState::Critical: ui->ledLabel->setPixmap(QPixmap(QString::fromUtf8(":/StatusBar/Resources/led-green.png"))); break;
    }
    targetLabel->setText(msg);
}

void MainWindowStatusBar::updateFromCameraTemperature(const double tempC)
{
	LogService::log(LogMessage("MainWindowStatusBar::updateFromCameraTemperature", LogMessage::Type::Trace));

	const double warningThresholdC = 55;
	const double tempF = tempC * 9 / 5 + 32;
	QString tempMsg = QString::number(tempC) + QString::fromLocal8Bit("�C") + tr("/") + QString::number(tempF) + QString::fromLocal8Bit("�F");

	message(tempMsg, ui->tempLabel, (tempC > warningThresholdC) ? LedState::Warning : LedState::Normal);
	LogService::log(LogMessage(QString("Temperature: ") + QString::number(tempC), LogMessage::Type::RuntimeStats));
}

void MainWindowStatusBar::clearError()
{
	LogService::log(LogMessage("MainWindowStatusBar::clearError", LogMessage::Type::Trace));

    message(QString::fromLocal8Bit("System OK"), ui->messageLabel);
}

void MainWindowStatusBar::receiveLogMessage(LogMessage msg)
{
	LogService::log(LogMessage("MainWindowStatusBar::receiveLogMessage", LogMessage::Type::Trace));

	QString content = msg.content();
	switch(msg.type())
	{
		case LogMessage::Type::Error :
			displayError(content);
			break;
		case LogMessage::Type::Warning :
			displayWarning(content);
			break;
		default: // normal
			message(QString::fromLocal8Bit("System OK"), ui->messageLabel);
	}
}

