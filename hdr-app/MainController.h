#pragma once
#include <QObject>
#include <QErrorMessage>

#include "MainWindow.h"
#include "StartView.h"
#include "CameraBusManager.h"
#include "CameraView.h"
#include "StartView.h"
#include "SettingsManager.h"
#include "CaptureProcessor.h"
#include "HdrProcessor.h"
#include "VideoSaveProcessor.h"
#include "MainWindowStatusBar.h"
#include "WeldInfoView.h"
#include "StopWatch.h"
#include "ViewControl.h"

#include "../qt-logger/LogService.h"

class MainController :
	public QObject
{

    Q_OBJECT
public:
	MainController(SettingsManager *);
	~MainController(void);

	void show(FlyCapture2::PGRGuid);
	void stop();
	bool isRunning();
	void changeUsbConnectionStatus(bool);
	PointGreyCtl * settingsView();

signals:
	void usbConnectionChanged(bool);
	void sendMessage(const LogMessage &);

private:
	MainWindow * m_mainWindow;

	StartView * m_startView;
	CameraView * m_cameraView;
	SettingsManager * m_settingsManager;

	QErrorMessage m_errorMsgbox;

	CaptureProcessor * m_cameraProcessor;
	HdrProcessor * m_hdrProcessor;
    VideoSaveProcessor * m_videoSaveProcessor;

	PointGreyCtl * m_settingsView;

	WeldInfoView * m_weldInfoView;

	FlyCapture2::PGRGuid m_camGuid;

	VideoMetadataRecorder * m_metadataRecorder;

	QString m_baseProfileName;

	MainWindowStatusBar * m_statusBar;

	StopWatch* m_stopwatch;

	ViewProcessor* m_viewProcessor;

	bool m_running;

	void buildSettingsMenu();
	void setMainView(QWidget * view, bool showToolbars);

	

private slots:
	void hdrWarning(FrameProcessor::Warning, const QString &);

	// controller slots
	void processorError(FrameProcessor::Error, const QString &);
	void closeEvent(QCloseEvent *);
	void recordStateToggled(bool);
	void recordError(FrameProcessor::Error, const QString &);
	void settingsInvoked();
	void weldInfoInvoked();
	void settingsProfileSelected();
    void settingsProfilesChanged();

	//generalized dropdown menu for profile settings
	void build_drop_down_menu(QAction* profile_action);
	void set_profile_to_checked();

	// view slots
    void start();
	void handleCameraViewCloseEvent();
};

