#include "AltVideo.h"

AltVideo::AltVideo(QObject* parent)
:QObject(parent)
, m_worker(ActiveQThread::createActiveQThread(10))
, m_active(false)
, m_img()
{
	m_capture.open(0);
}

AltVideo::AltVideo(const char* path, QObject* parent)
: QObject(parent)
, m_worker(ActiveQThread::createActiveQThread(10))
, m_active(false)
, m_img()
{
	m_capture.open(path);
}

void AltVideo::start(){
	if (!m_active){
		m_active = true;
		m_worker->send(std::bind(&AltVideo::fetch, this, std::ref(m_capture), std::ref(m_img)));
	}
}

void AltVideo::stop(){
	m_active = false;
}

void AltVideo::fetch(cv::VideoCapture& cap, cv::Mat& img){
	while (m_active){
		if (!cap.isOpened()){
			stop();
			continue;
		}
		if (!cap.read(img)){
			stop();
			continue;
		}
		emit frameArrived(img);
	}
}

AltVideo::~AltVideo(){
	m_capture.release();
	m_img.release();
}

