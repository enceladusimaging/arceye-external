#include "SettingsManager.h"

SettingsManager::SettingsManager(QString & profilesFileName, QString & camSerial, SettingsManager::CameraType cameraType, QObject * parent)
    : QObject(parent)
    , m_baseProfileName("base")
	, m_profilesFileName(profilesFileName)
	, m_camSerial(camSerial)
	, m_cameraType(cameraType)
	, m_userProfiles()
    , m_baseSettings(defaultSettings())
    , m_currentSettings(m_baseSettings)
{
    // load all profiles from file
	load();

	// set active settings to the base settings
    setCurrentSettings(m_userProfiles[m_baseProfileName]);
}

QStringList SettingsManager::profileNames() const
{
	LogService::log(LogMessage("SettingsManager::profileNames", LogMessage::Type::Trace));

    return m_userProfiles.keys();
}

ProfileSettings SettingsManager::getProfileSetting(const QString& profileName){
	ProfileSettings settings;
	if (m_userProfiles.contains(profileName)){
		settings = m_userProfiles.value(profileName);
	}
	return settings;
}

void SettingsManager::setProfileShortcutMap(const QString& profileName, const QString& actionName){
	if (m_userProfiles.contains(profileName)){
		m_userProfiles[profileName].shortcutMapping.ActionName = actionName;
	}
}

ProfileSettings SettingsManager::currentSettings() const
{
	LogService::log(LogMessage("SettingsManager::currentSettings", LogMessage::Type::Trace));

    return m_currentSettings;
}

ProfileSettings SettingsManager::defaultSettings()
{
	LogService::log(LogMessage("SettingsManager::defaultSettings", LogMessage::Type::Trace));

    CameraBasicSettings cbs;
    cbs.brightness = 2.9;
    cbs.exposure = -2.585;
    cbs.exposureAuto = cbs.exposureEnabled = false;
    cbs.sharpness = 1534;
    cbs.sharpnessAuto = cbs.sharpnessEnabled = false;
    cbs.hue = 0.0;
    cbs.hueEnabled = false;
    cbs.saturation = 124.902;
    cbs.saturationAuto = cbs.saturationEnabled = false;
    cbs.gamma = 1.0;
    cbs.gammaEnabled = false;
    cbs.whiteBalRed = 530;
    cbs.whiteBalBlue = 930;
    cbs.whiteBalAuto = false;
    cbs.whiteBalEnabled = true;
    cbs.frameRate = 10.0;
    cbs.frameRateAuto = false;
    cbs.frameRateEnabled = true;
    cbs.gain = 0;
    cbs.gainAuto = false;
    cbs.gainEnabled = true;
    cbs.shutter = 6.0;
    cbs.shutterAuto = false;
    cbs.shutterEnabled = true;

    CameraVideoMode cvm;
	CameraSettings cs;
	cs.serialNumber = m_camSerial.toInt();

	if (m_cameraType == SettingsManager::CameraType::CMOSIS) {
		// CMOSIS CMV 4000
		cvm.width = 1280;
		cvm.height = 720;
		cvm.left = 384;
		cvm.top = 664;
		cvm.mode = 0;
		cvm.pixelFormat = "RGB 8";

		cs.shutterConversionFactor = 0.01074; //CMOSIS CMV4000
		cs.gainConversionFactor = 1.443; //CMOSIS CMV4000 - capped at 10.1
	}
	else {
		// Sony IMX 174
		cvm.width = 1280;//1280;
		cvm.height = 720;// 720;
		cvm.left = 320;
		cvm.top = 240;
		cvm.mode = 7;
		cvm.pixelFormat = "RGB 8";

		cs.shutterConversionFactor = 0.005; // Sony IMX174
		cs.gainConversionFactor = 0.1; //Sony IMX174
	}
    CameraAdvancedSettings cas;
    cas.embeddedBrightness 
        = cas.embeddedExposure
        = cas.embeddedFrameCnt
        = cas.embeddedGain
        = cas.embeddedGpioPinState
        = cas.embeddedRoiPos
        = cas.embeddedShutter
        = cas.embeddedStrobe
        = cas.embeddedTimestamp
        = cas.embeddedWhiteBal
        = true;

    TriggeredCapture tCap;
    tCap.enabled = tCap.delayEnabled = false;
    tCap.mode = 14;
    tCap.param = 0;
    tCap.source = 3;
    tCap.polarity = 1;
    tCap.delayS= 0.0;

    QVector<TriggerStrobe> strobes;

    TriggerStrobe gpio0Strobe;
    gpio0Strobe.enabled = false;
    gpio0Strobe.source = 0;
    gpio0Strobe.polarity = 1; //high
    gpio0Strobe.delayMs = 0.0;
    gpio0Strobe.durationMs = 0.0;
    
    TriggerStrobe gpio1Strobe;
    gpio1Strobe.enabled = false;
    gpio1Strobe.source = 1;
    gpio1Strobe.polarity = 1; //high
    gpio1Strobe.delayMs = 0.0;
    gpio1Strobe.durationMs = 0.0;

    TriggerStrobe gpio2Strobe;
    gpio2Strobe.enabled = true;
    gpio2Strobe.source = 2;
    gpio2Strobe.polarity = 1; //high
    gpio2Strobe.delayMs = 0.0;
    gpio2Strobe.durationMs = 0.0;

    TriggerStrobe gpio3Strobe;
    gpio3Strobe.enabled = false;
    gpio3Strobe.source = 3;
    gpio3Strobe.polarity = 1; //high
    gpio3Strobe.delayMs = 0.0;
    gpio3Strobe.durationMs = 0.0;

    strobes << gpio0Strobe << gpio1Strobe << gpio2Strobe << gpio3Strobe;

    CameraTriggerSettings cts;
    cts.triggeredCapture = tCap;
    cts.strobeSettings = strobes;
    cts.reservedInputPins << 0;

    CameraHdrSettings chs;
    chs.enabled = true;
    chs.shutters << 185 << 37 << 7 << 1;
    chs.gains << 0 << 0 << 0 << 0;

    // Sony IMX 174 Gain range: 0 - 24 dB
    // CMOSIS CMV4000 Gain range: 0 - 10.1, increments of 1

    cs.initialShutter = 0.011;
    cs.deltaEv = 2.32;
   
    cs.basicSettings = cbs;
    cs.videoMode = cvm;
    cs.advancedSettings = cas;
    cs.triggerSettings = cts;
    cs.hdrSettings = chs;

    HdrSettings hdrSettings;
    hdrSettings.ccrfPath = "./CCRF_3.000000_1024_solver_channel_1.raw";
    hdrSettings.enabled = false;
    hdrSettings.exposurePref = 1.0;
    hdrSettings.height = cvm.height; //from camera
    hdrSettings.numColChannels = 3;
    hdrSettings.numFrames = 4;
    hdrSettings.ppemPath = "./PPEM_1024.raw";
    hdrSettings.shutters = chs.shutters;
	hdrSettings.gains = chs.gains;
    hdrSettings.width = cvm.width; // from camera

    RecordSettings rs;

    rs.targetDir = QStandardPaths::locate(QStandardPaths::MoviesLocation, QString(), QStandardPaths::LocateDirectory);
    rs.diskFullPercentThreshold = 95;
    rs.codec = "lossy";
    rs.frameRate = 30.0; 
    rs.frameRateOverride = true;
    rs.width = cvm.width; //from camera
    rs.height = cvm.height; //from camera
    rs.arcOnRecording = false;
    rs.arcOnRecordingPin = cts.reservedInputPins[0];
	rs.timeInterval = 15;
    
    ProfileSettings defaultProf;
    defaultProf.name = "base";
    defaultProf.removable = false;
    defaultProf.cameraSettings = cs;
    defaultProf.hdrSettings = hdrSettings;
    defaultProf.recordSettings = rs;

    return defaultProf;
}

void SettingsManager::setCurrentProfile(const QString & profileName)
{
	LogService::log(LogMessage("SettingsManager::setCurrentProfile", LogMessage::Type::Trace));

    setCurrentSettings(m_userProfiles[profileName]);
    
    emit profileChanged();
}

void SettingsManager::setCurrentSettings(const ProfileSettings & profileSettings)
{
	LogService::log(LogMessage("SettingsManager::setCurrentSettings", LogMessage::Type::Trace));

    m_currentSettings = profileSettings;

	ProfileSettings prof = m_userProfiles[m_currentSettings.name];
	QJsonObject settingsObj;
	prof.write(settingsObj);
	QJsonDocument settingsDoc(settingsObj);
	QByteArray settings = settingsDoc.toJson();
	LogService::log(LogMessage(settings, LogMessage::Type::Info));

    emit cameraSettingsChanged(m_currentSettings.cameraSettings);
    emit hdrSettingsChanged(m_currentSettings.hdrSettings);
    emit recordSettingsChanged(m_currentSettings.recordSettings);
}

void SettingsManager::setRecordSettings(const RecordSettings & settings)
{
	LogService::log(LogMessage("SettingsManager::setRecordSettings", LogMessage::Type::Trace));

    m_currentSettings.recordSettings = settings;

    emit recordSettingsChanged(m_currentSettings.recordSettings);
}

bool SettingsManager::isBaseProfile(const ProfileSettings &p)
{
	LogService::log(LogMessage("SettingsManager::isBaseProfile", LogMessage::Type::Trace));

    return p.name.compare(m_baseProfileName) == 0;
}

void SettingsManager::save(const QString & name)
{
	LogService::log(LogMessage("SettingsManager::save", LogMessage::Type::Trace));

    const QString profileName = name.isEmpty() ? m_currentSettings.name : name;
    m_currentSettings.name = profileName;
    m_currentSettings.removable = !isBaseProfile(m_currentSettings);
    
    m_userProfiles[profileName] = m_currentSettings;

    commit();

    setCurrentProfile(profileName);

    emit userProfileAvailabilityChanged();
}

void SettingsManager::removeProfile()
{
	LogService::log(LogMessage("SettingsManager::removeProfile", LogMessage::Type::Trace));
	LogService::log(LogMessage("Profile removed", LogMessage::Type::Info));

    // on removal, we drop the profile from the list if it was a user profile and link the current profile to the base profile
    m_userProfiles.remove(m_currentSettings.name);

    commit();

    setCurrentProfile(m_baseProfileName);

    emit userProfileAvailabilityChanged();
}

void SettingsManager::commit()
{
	LogService::log(LogMessage("SettingsManager::commit", LogMessage::Type::Trace));

    QFile profilesFile(m_profilesFileName);

    if (!profilesFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Unable to write profiles at: " << profilesFile.fileName();
        return;
    }

    QJsonObject root;
    
    foreach(QString name, profileNames()) {
		ProfileSettings s = m_userProfiles[name];
        QJsonObject prof;
        s.write(prof);
        root[name] = prof;
    }
    
    QJsonDocument saveDoc(root);
    profilesFile.write(saveDoc.toJson());
}

void SettingsManager::load()
{
	LogService::log(LogMessage("SettingsManager::load", LogMessage::Type::Trace));

	QFile profilesFile(m_profilesFileName);

    if (!profilesFile.open(QIODevice::ReadOnly)) {
        qDebug() << "No user profiles found at: " << profilesFile.fileName();	
		m_userProfiles[m_baseProfileName] = m_baseSettings;
        commit();
        return;
    }

    QString fileAsStr = profilesFile.readAll();

	QJsonDocument saveDoc = QJsonDocument::fromJson(fileAsStr.toUtf8());
	QJsonObject root = saveDoc.object();

	foreach(QString name, root.keys()) {
        // parse the profile
        QJsonObject prof = root[name].toObject();
        ProfileSettings ps;
		ps.read(prof);
		m_userProfiles[name] = ps;
	}
}