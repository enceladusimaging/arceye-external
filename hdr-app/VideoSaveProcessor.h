#pragma once

#include <memory>
#include <string>

// Qt
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QObject>
#include <QStandardPaths>
#include <QTimer>

// OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <Windows.h>

#include "ActiveQThread.h"
#include "AveragedElapsedTimer.h"
#include "FrameProcessor.h"
#include "SettingsManager.h"

// qt-logger
#include "../qt-logger/LogService.h"

#include "ActiveQThread.h"
#include "AveragedElapsedTimer.h"
#include "FrameProcessor.h"
#include "SettingsManager.h"

// boost
#include "boost/filesystem.hpp"

class VideoSaveProcessor : public FrameProcessor
{
	Q_OBJECT

public:
	static VideoSaveProcessor * createVideoSave(const RecordSettings &, QObject * parent = 0);

	virtual void setState(FrameProcessor::State);

signals:
	void recordError(const QString &);
	void recordStarted(const QString &);
	void recordStopped();

	public slots:
	void process(AnnotatedFrame);
	void settingsChanged(const RecordSettings &);
	void sceneFlipped(bool);
	void resetWriter();

private:
	explicit VideoSaveProcessor(const RecordSettings &, QObject * parent = 0);

	void openWriter();
	void closeWriter();
	void configWriter(QString &, int &, double &, cv::Size &);
	int videoWriteCodec();
	void recordDest(QString &);

	bool arcOnRecordingActive(const AnnotatedFrame::FrameMetadata &);

	RecordSettings m_settings;
	cv::VideoWriter m_vw;
	QString m_recordDest;
	bool m_flipped;

	// frame rate timing
	AveragedElapsedTimer m_frameRateTimer;

	std::unique_ptr<ActiveQThread> m_worker;

	// worker methods
	void writeFrameTask(cv::VideoWriter &, AnnotatedFrame);
	void openWriterTask(cv::VideoWriter &);
	void closeWriterTask(cv::VideoWriter &);
	void resetWriterTask(cv::VideoWriter &);
	bool atDiskLimit();
};