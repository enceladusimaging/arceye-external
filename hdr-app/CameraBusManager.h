#pragma once

#include <memory>

#include <QDebug>
#include <QObject>

#include "FlyCapture2.h"

#include "../qt-logger/LogService.h"

class CameraBusManager : public QObject
{
    Q_OBJECT

public:
    enum Error { InitError, ShutdownError, DeviceArrivalError, DeviceRemovalError };

    CameraBusManager(QObject * = 0);
    ~CameraBusManager(void);

    void discoverCameras();

    static void onBusArrival(void *, unsigned int);
    static void onBusRemoval(void *, unsigned int);
    static void onBusReset(void *, unsigned int);

signals:
    void cameraConnected(unsigned int, FlyCapture2::PGRGuid);
    void cameraDisconnected(unsigned int);
    void error(CameraBusManager::Error, const QString &);

private slots:
    void handleBusArrival(unsigned int);
    void handleBusRemoval(unsigned int);
    void handleBusReset(unsigned int);

private:
    std::unique_ptr<FlyCapture2::CallbackHandle> m_cbArrivalHandle;
    std::unique_ptr<FlyCapture2::CallbackHandle> m_cbRemovalHandle;
    std::unique_ptr<FlyCapture2::CallbackHandle> m_cbResetHandle;

    std::unique_ptr<FlyCapture2::BusManager> m_busMgr;
    std::shared_ptr<FlyCapture2::Camera> m_camera;
};

