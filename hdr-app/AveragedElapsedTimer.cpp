#include "AveragedElapsedTimer.h"

AveragedElapsedTimer::AveragedElapsedTimer(unsigned int numSamples, QObject *parent)
    : QObject(parent)
    , m_timer()
    , m_sampleIdx(0)
    , m_totalSamples(numSamples)
    , m_lastAvg(0)
    , m_valid(false)
{
    m_timer.invalidate();
}

void AveragedElapsedTimer::eventOccurred()
{
	LogService::log(LogMessage("AveragedElapsedTimer::eventOccurred", LogMessage::Type::Trace));
    if (!m_timer.isValid()) {
        m_timer.start();
    } else if (m_sampleIdx++ == m_totalSamples) {
        m_lastAvg = m_timer.restart() / (1000.0 * m_totalSamples);
        m_sampleIdx = 0;
        m_valid = true;
    }
}

double AveragedElapsedTimer::lastIntervalAverage() const 
{
	LogService::log(LogMessage("AveragedElapsedTimer::lastIntervalAverage", LogMessage::Type::Trace));
    return m_lastAvg;
}

bool AveragedElapsedTimer::isValid() const 
{
	LogService::log(LogMessage("AveragedElapsedTimer::isValid", LogMessage::Type::Trace));
    return m_valid;
}