#include "FrameProcessorWorker.h"

FrameProcessorWorker::FrameProcessorWorker(QPointer<FrameProcessor> processor) 
		: QObject()
		, frameProc(processor)
		, state(WorkerState::Uninitialized)
        , log(Logger::get())
{}

void FrameProcessorWorker::initialize(const WorkerSettings & settings)
{
	try {
        frameProc->initialize(settings);
		state = WorkerState::Ready;

		emit initialized();
	} catch(std::system_error & se) {
		QString errMsg("Initialization error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit initializationError(errMsg);
	}
}

void FrameProcessorWorker::start()
{
    try {
		frameProc->start();
        state = WorkerState::Started;

		emit started();
    } catch(std::system_error & se) {
		QString errMsg("Start error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit startError(errMsg);
    }
}

void FrameProcessorWorker::stop()
{
	try {
		frameProc->stop();
		state = WorkerState::Stopped;

		emit stopped();
	} catch(std::system_error se) {
		QString errMsg("Stop error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit stopError(errMsg);
	}
}

void FrameProcessorWorker::changeSettings(const WorkerSettings & newSettings)
{
	try {
		frameProc->settingsChanged(newSettings);

		emit settingsChanged(newSettings);
	} catch(std::system_error se) {
		QString errMsg("State change error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit settingsChangeError(errMsg);
	} catch (...) {
		emit settingsChangeError("Unexpected error occurred.");
	}
}

void FrameProcessorWorker::produce()
{
    if (state != WorkerState::Started) return;

	try {
		emit processingStarted();

		AnnotatedFrame result = frameProc->produce();
		
		emit processingEnded(result);
	} catch(std::system_error se) {
		QString errMsg("Processing error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit processingError(errMsg);
	} catch(...) {
		emit processingError("Unexpected error occurred.");
		throw;
	}
}

void FrameProcessorWorker::process(const AnnotatedFrame & inputFrame)
{
    if (state != WorkerState::Started) return;

	try {
		emit processingStarted();

		AnnotatedFrame input = inputFrame; //defensive copy
		AnnotatedFrame result = frameProc->process(input);

		emit processingEnded(result);
	} catch(std::system_error se) {
		QString errMsg("Processing error: ");
		errMsg.append(se.what());

        log.error(errMsg.toStdString());
		emit processingError(errMsg);
	} catch(...) {
		emit processingError(QString("Unexpected error occurred."));
		throw;
	}
}