#include "WeldInfoView.h"

#include "ui_WeldInfoView.h"

WeldInfoView::WeldInfoView(QString & serial, QWidget * parent)
	: QDialog(parent)
	, ui(new Ui::WeldInfoView)
	, m_targetDir(QStandardPaths::locate(QStandardPaths::MoviesLocation, QString(), QStandardPaths::LocateDirectory))
	, m_serial(serial)
{
	ui->setupUi(this);

	connect(ui->saveButton, SIGNAL(clicked()), this, SLOT(save()));
}


WeldInfoView::~WeldInfoView()
{
	delete ui;
}

void WeldInfoView::targetDirChanged(const RecordSettings & recordSettings)
{
	m_targetDir = recordSettings.targetDir;
}

QString WeldInfoView::makeFileName()
{
	QString fileName = m_targetDir
		+ tr("weldInfo") 
		+ tr("_") + QString::number(QDateTime::currentDateTime().date().year())
		+ tr("_") + QString::number(QDateTime::currentDateTime().date().month())
		+ tr("_") + QString::number(QDateTime::currentDateTime().date().day())
		+ tr("_") + QString::number(QDateTime::currentDateTime().time().msecsSinceStartOfDay())
		+ tr(".txt");

	return fileName;
}

void WeldInfoView::save()
{
	QString info = electricalCharacteristics() + variables();
	writeToFile(info);
}

void WeldInfoView::writeToFile(const QString & info)
{
	QString fileName = makeFileName();

	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream out(&file);
		out << tr("Camera serial: ") + m_serial << endl;
		out << info;
		out.flush();
	}
}

QString WeldInfoView::electricalCharacteristics()
{
	QString voltage = ui->voltageBox->text();
	QString current = ui->currentBox->text();
	QString wireFeed = ui->wireFeedBox->text();
	QString wireFeedUnit = ui->wireFeedUnitBox->currentText();
	QString polarity = ui->polarityBox->currentText();
	QString CTW = ui->CTWBox->text();
	QString CTWUnit = ui->CTWUnitBox->currentText();
	QString EE = ui->EEBox->text();
	QString EEUnit = ui->EEUnitBox->currentText();

	QString separator = tr("========================================");

	QString electricalChar = 
		separator + tr("\n")
		+ tr("ELECTRICAL CHARACTERISTICS") + tr("\n")
		+ separator + tr("\n")
		+ "Voltage: " + voltage + tr("\n")
		+ "Current: " + current + tr("\n")
		+ "Wire Feed Speed: " + wireFeed + tr(" ") + wireFeedUnit + tr("\n")
		+ "Polarity: " + polarity + tr("\n")
		+ "Contact Tip to Work Distance: " + CTW + tr(" ") + CTWUnit + tr("\n")
		+ "Electrode Extension: " + EE + tr(" ") + EEUnit + tr("\n");

	return electricalChar;
}

QString WeldInfoView::variables()
{
	QString process = ui->processBox->currentText();
	QString processMode = ui->processModeBox->currentText();
	QString metalTransfer = ui->metalTransferBox->currentText();
	QString gas = ui->gasBox->text();
	QString gasFlow = ui->gasFlowBox->text();
	QString nozzleDiameter = ui->nozzleDiameterBox->text();
	QString nozzleDiameterUnitBox = ui->nozzleDiameterUnitBox->currentText();
	QString electrodeComposition = ui->electrodeCompositionBox->text();
	QString electrodeDiameter = ui->electrodeDiameterBox->text();
	QString electrodeDiameterUnit = ui->electrodeDiameterUnitBox->currentText();
	QString metalMaterialSpec = ui->metalMaterialSpecBox->text();
	QString metalThickness = ui->metalThicknessBox->text();
	QString metalThicknessUnit = ui->metalThicknessUnitBox->currentText();
	QString position = ui->positionBox->currentText();
	QString backing = ui->backingMaterialSpecBox->text();
	QString backingThickness = ui->backingThicknessBox->text();
	QString backingThicknessUnit = ui->backingThicknessUnitBox->currentText();
	QString jointType = ui->jointTypeBox->currentText();
	QString jointPreparation = ui->jointPreparationBox->text();
	QString penetration = ui->penetrationBox->currentText();
	QString ETT = ui->ETTBox->text();
	QString ETTUnit = ui->ETTUnitBox->currentText();
	QString workAngle = ui->workAngleBox->text();
	QString travelAngle = ui->travelAngleBox->text();
	QString travelSpeed = ui->travelSpeedBox->text();
	QString travelSpeedUnit = ui->travelSpeedUnitBox->currentText();
	QString refWPS = ui->refWPSBox->text();
	QString specReq = ui->specReqBox->text();

	QString separator = tr("========================================");

	QString variables = 
		separator + tr("\n")
		+ "VARIABLES" + tr("\n")
		+ separator + tr("\n")
		+ "Process: " + process + tr("\n")
		+ "Process Mode: " + processMode + tr("\n")
		+ "Mode of Metal Transfer: " + metalTransfer + tr("\n")
		+ "Gas" + tr("\n")
		+ tr("\t") + "Type: " + gas + tr("\n")
		+ tr("\t") + "Flow Rate: " + gasFlow + tr("\n")
		+ tr("\t") + "Nozzle Diameter: " + nozzleDiameter + tr(" ") + nozzleDiameterUnitBox + tr("\n")
		+ "Electrode" + tr("\n")
		+ tr("\t") + "Composition: " + electrodeComposition + tr("\n")
		+ tr("\t") + "Diameter: " + electrodeDiameter + tr(" ") + electrodeDiameterUnit + tr("\n")
		+ "Base Metal" + tr("\n")
		+ tr("\t") + "Material Specification: " + metalMaterialSpec + tr("\n")
		+ tr("\t") + "Thickness: " + metalThickness + tr(" ") + metalThicknessUnit + tr("\n")
		+ "Backing" + tr("\n")
		+ tr("\t") + "Material Specification: " + backing + tr("\n")
		+ tr("\t") + "Thickness: " + backingThickness + tr(" ") + backingThicknessUnit + tr("\n")
		+ "Position: " + position + tr("\n")
		+ "Joint Type: " + jointType + tr("\n")
		+ "Joint Preparation: " + jointPreparation + tr("\n")
		+ "Penetration: " + penetration + tr("\n")
		+ "Effective Throat Thickness: " + ETT + tr(" ") + ETTUnit + tr("\n")
		+ "Work Angle: " + workAngle + tr("\n")
		+ "Travel Angle: " + travelAngle + tr("\n")
		+ "Travel Speed: " + travelSpeed + tr(" ") + travelSpeedUnit + tr("\n")
		+ "Reference WPS: " + refWPS + tr("\n")
		+ "Special Requirements: " + specReq + tr("\n");

	return variables;
}