#include "CameraBusManager.h"

void CameraBusManager::onBusArrival(void *pParam, unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::onBusArrival", LogMessage::Type::Trace));

    CameraBusManager *busMgr = static_cast<CameraBusManager *>(pParam);

    QMetaObject::invokeMethod(busMgr, "handleBusArrival", Qt::QueuedConnection,
        Q_ARG(unsigned int, serialNum));
}

void CameraBusManager::onBusRemoval(void *pParam, unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::onBusRemoval", LogMessage::Type::Trace));

    CameraBusManager *busMgr = static_cast<CameraBusManager *>(pParam);

    QMetaObject::invokeMethod(busMgr, "handleBusRemoval", Qt::QueuedConnection,
        Q_ARG(unsigned int, serialNum));
}

void CameraBusManager::onBusReset(void *pParam, unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::onBusReset", LogMessage::Type::Trace));

    CameraBusManager *busMgr = static_cast<CameraBusManager *>(pParam);

    QMetaObject::invokeMethod(busMgr, "handleBusReset", Qt::QueuedConnection,
        Q_ARG(unsigned int, serialNum));
}

CameraBusManager::CameraBusManager(QObject *parent)
    : QObject(parent)
    , m_busMgr(new FlyCapture2::BusManager)
    , m_camera(nullptr)
    , m_cbArrivalHandle(new FlyCapture2::CallbackHandle)
    , m_cbRemovalHandle(new FlyCapture2::CallbackHandle)
    , m_cbResetHandle(new FlyCapture2::CallbackHandle)
{
    FlyCapture2::Error err;
    
    // register callbacks
    err = m_busMgr->RegisterCallback(&CameraBusManager::onBusArrival, FlyCapture2::ARRIVAL, this, m_cbArrivalHandle.get());
    if (err != FlyCapture2::PGRERROR_OK) {
        QString msg = tr("Failed to register arrival callback: ") + tr(err.GetDescription());
        emit error(Error::InitError, msg);   
        return;
    } 

    err = m_busMgr->RegisterCallback(&CameraBusManager::onBusRemoval, FlyCapture2::REMOVAL, this, m_cbRemovalHandle.get());
    if (err != FlyCapture2::PGRERROR_OK) {
        QString msg = tr("Failed to register removal callback: ") + tr(err.GetDescription());
        emit error(Error::InitError, msg);    
        return;
    } 

    err = m_busMgr->RegisterCallback(&CameraBusManager::onBusReset, FlyCapture2::BUS_RESET, this, m_cbResetHandle.get());
    if (err != FlyCapture2::PGRERROR_OK) {
        QString msg = tr("Failed to register reset callback: ") + tr(err.GetDescription());
        emit error(Error::InitError, msg);     
        return;
    } 
}


CameraBusManager::~CameraBusManager(void)
{

}

void CameraBusManager::discoverCameras()
{
	LogService::log(LogMessage("CameraBusManager::discoverCameras", LogMessage::Type::Trace));

    FlyCapture2::Error err;
    unsigned int numCams;
    
    err = m_busMgr->GetNumOfCameras(&numCams);
    if (err != FlyCapture2::PGRERROR_OK) {
        QString msg = tr("Failed to get number of cameras connected: ") + tr(err.GetDescription());
        emit error(Error::InitError, msg);     
        return;
    }

#ifdef BYPASS_MODE
	FlyCapture2::PGRGuid guid;
	unsigned int serialNum;

	if (numCams > 0){
		for (int i = 0; i < numCams; i++) {
			m_busMgr->GetCameraFromIndex(i, &guid);
			m_busMgr->GetCameraSerialNumberFromIndex(i, &serialNum);

			emit cameraConnected(serialNum, guid);
		}
	}
	else{
		emit cameraConnected(serialNum, guid);
	}
#else
    for (int i = 0; i < numCams; i++) {
        FlyCapture2::PGRGuid guid;
        unsigned int serialNum;

        m_busMgr->GetCameraFromIndex(i, &guid);
        m_busMgr->GetCameraSerialNumberFromIndex(i, &serialNum);

        emit cameraConnected(serialNum, guid);
    }
#endif
}

void CameraBusManager::handleBusArrival(unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::handleBusArrival", LogMessage::Type::Trace));

    FlyCapture2::PGRGuid guid;
    FlyCapture2::Error err = m_busMgr->GetCameraFromSerialNumber(serialNum, &guid);

    if (err != FlyCapture2::PGRERROR_OK) {
        QString msg = tr("Failed to get camera GUID from serial number: ") + tr(err.GetDescription());
        emit error(Error::DeviceArrivalError, msg);     
        return;
    }

    emit cameraConnected(serialNum, guid);
}

void CameraBusManager::handleBusRemoval(unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::handleBusRemoval", LogMessage::Type::Trace));

    emit cameraDisconnected(serialNum);
}

void CameraBusManager::handleBusReset(unsigned int serialNum)
{
	LogService::log(LogMessage("CameraBusManager::handleBusReset", LogMessage::Type::Trace));

    qDebug() << "Bus reset for camera serial: " << serialNum;
}
