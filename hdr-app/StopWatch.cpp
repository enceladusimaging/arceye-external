#pragma once

#include "StopWatch.h"

#include <QDateTime>

#include <Windows.h>

StopWatch::StopWatch(QObject* parent)
:QObject(parent)
, m_time(0)
, m_active(false)
, m_reset_interval(INT64_MAX)
, m_worker(ActiveQThread::createActiveQThread(10))
{
}

void StopWatch::settingsChanged(const RecordSettings& settings){
	unsigned int value = settings.timeInterval;
	if (value > 0){
		m_reset_interval = (qint64)(value * 60 * 1000);
	}
}

void StopWatch::startTimer(){
	if (!m_active){
		m_active = true;
		m_worker->send(std::bind(&StopWatch::runTimer, this));
	}
}

void StopWatch::stopTimer(){
	m_active = false;
}

void StopWatch::resetTimer(){
	m_time = 0;
}

void StopWatch::startStop(const bool start){
	if (start)
		startTimer();
	else
		stopTimer();
}

void StopWatch::runTimer(){
	while (m_active){
		qint64 current_time = QDateTime::currentMSecsSinceEpoch();
		if (abs(current_time - m_time) > m_reset_interval){
			m_time = current_time;
			emit reset();
		}
		Sleep(10);
	}
	resetTimer();
}


