#pragma once

#include "AnnotatedFrame.h"

// Qt
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QResizeEvent>
#include <QSizePolicy>
#include <QTimer>

#include "../qt-logger/LogService.h"

namespace Ui {
    class CameraView;
}

class CameraView : public QWidget
{
    Q_OBJECT

public:
    CameraView(QWidget * = 0);

signals:
	void closed();
	void viewMouseWheelSignal(QWheelEvent*);
	void viewMouseMoveSignal(QMouseEvent*);

public slots:
    void display(const QImage &, bool);
	void viewMouseWheelSlot(QWheelEvent*);
	void viewMouseMoveSlot(QMouseEvent*);

protected:
    void resizeEvent(QResizeEvent *);
	bool eventFilter(QObject *obj, QEvent *event);

private:
    void fitGraphicsItem(QGraphicsView *, QGraphicsPixmapItem *);

	//Fields - pointer resources
	Ui::CameraView * ui;
	QGraphicsScene m_scene;
	QGraphicsPixmapItem * m_imageItem;
	
    QTimer m_displayRefreshTimer;

    bool m_ready;

private slots:
    void displayFrame();

};

