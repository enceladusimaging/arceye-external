#include "MainController.h"
#include "ui_MainWindow.h"

//#define META_RECORDER
#define USE_STOPWATCH

MainController::MainController(SettingsManager * settingsManager) : QObject()
	, m_mainWindow(new MainWindow())
	, m_startView(new StartView())
	, m_cameraView(new CameraView())
	, m_cameraProcessor()
    , m_hdrProcessor()
	, m_statusBar()
    , m_videoSaveProcessor()
	, m_settingsManager(settingsManager)
	, m_metadataRecorder(new VideoMetadataRecorder(m_settingsManager->currentSettings().cameraSettings, this))
	, m_baseProfileName("base")
	, m_settingsView(new PointGreyCtl(m_settingsManager, m_mainWindow))
	, m_weldInfoView(new WeldInfoView(QString::number(m_settingsManager->currentSettings().cameraSettings.serialNumber), m_mainWindow))
	, m_running(false)
	, m_stopwatch()
{
	// view connections
	connect(m_startView, SIGNAL(startClicked()), this, SLOT(start()));
	connect(this, SIGNAL(usbConnectionChanged(bool)), m_startView, SLOT(usbConnected(bool)));
	connect(m_mainWindow, SIGNAL(display(const QImage &, bool)), m_cameraView, SLOT(display(const QImage &, bool))); 

	connect(m_mainWindow, SIGNAL(closed()), this, SLOT(handleCameraViewCloseEvent()));

    connect(m_settingsManager, SIGNAL(userProfileAvailabilityChanged()), this, SLOT(settingsProfilesChanged()));
    buildSettingsMenu();

	connect(m_mainWindow->ui->actionProfile1, SIGNAL(triggered()), this, SLOT(set_profile_to_checked()));
	connect(m_mainWindow->ui->actionProfile2, SIGNAL(triggered()), this, SLOT(set_profile_to_checked()));
	connect(m_mainWindow->ui->actionProfile3, SIGNAL(triggered()), this, SLOT(set_profile_to_checked()));

	QString serial = QString::number(m_settingsManager->currentSettings().cameraSettings.serialNumber);
	QString mainWindowTitle = tr("arcEye - Enceladus Imaging Inc. - Camera Serial: ") + serial;
	m_mainWindow->setWindowTitle(mainWindowTitle);

	QString weldInfoViewTitle = tr("Weld Information - Camera Serial: ") + serial;
	m_weldInfoView->setWindowTitle(weldInfoViewTitle);
}


MainController::~MainController(void)
{

}

void MainController::show(FlyCapture2::PGRGuid camGuid)
{
	m_camGuid = camGuid;
	setMainView(m_startView, false);
	m_mainWindow->show();
}

void MainController::start()
{
	LogService::log(LogMessage("MainController::start", LogMessage::Type::Trace));

	CameraSettings cs = m_settingsManager->currentSettings().cameraSettings;
    HdrSettings hs = m_settingsManager->currentSettings().hdrSettings;
    RecordSettings rs = m_settingsManager->currentSettings().recordSettings;
	
	m_cameraProcessor = CaptureProcessor::createCapture(m_camGuid, cs, this);
    m_hdrProcessor = HdrProcessor::createHdr(hs, this);
    m_videoSaveProcessor = VideoSaveProcessor::createVideoSave(rs, this);

	m_stopwatch = new StopWatch(this);
	m_viewProcessor = new ViewProcessor(this, hs);
	
	m_settingsManager->setCurrentProfile(m_baseProfileName);
	
	// status bar
    m_statusBar = new MainWindowStatusBar(m_cameraProcessor, m_settingsManager->currentSettings().cameraSettings, m_mainWindow);
    connect(m_settingsManager, SIGNAL(cameraSettingsChanged(const CameraSettings &)), m_statusBar, SLOT(updateFromCameraSettings(const CameraSettings &)), Qt::QueuedConnection);
	connect(m_cameraProcessor, SIGNAL(temperatureChanged(double)), m_statusBar, SLOT(updateFromCameraTemperature(double)), Qt::QueuedConnection);
	m_mainWindow->statusBar()->addWidget(m_statusBar, 1);

	// Recording start stop control
    connect(m_mainWindow->ui->actionRecord, SIGNAL(toggled(bool)), m_settingsView, SLOT(recordStateToggled(bool)), Qt::QueuedConnection);
#ifdef USE_STOPWATCH
	connect(m_mainWindow->ui->actionRecord, SIGNAL(toggled(bool)), m_stopwatch, SLOT(startStop(bool)), Qt::UniqueConnection);
#else
	connect(m_mainWindow->ui->actionRecord, SIGNAL(toggled(bool)), m_cameraProcessor, SLOT(recordStateToggled(bool)), Qt::QueuedConnection);
#endif

    // Capture signals
    connect(m_cameraProcessor, SIGNAL(error(FrameProcessor::Error, const QString &)), this, SLOT(processorError(FrameProcessor::Error, const QString &)), Qt::QueuedConnection);
    connect(m_settingsManager, SIGNAL(cameraSettingsChanged(const CameraSettings &)), m_cameraProcessor, SLOT(settingsChanged(const CameraSettings &)), Qt::QueuedConnection);
        
    // HDR signals
    connect(m_hdrProcessor, SIGNAL(error(FrameProcessor::Error, const QString &)), this, SLOT(processorError(FrameProcessor::Error, const QString &)), Qt::QueuedConnection);
    connect(m_hdrProcessor, SIGNAL(warning(FrameProcessor::Warning, const QString &)), this, SLOT(hdrWarning(FrameProcessor::Warning, const QString &)));
    connect(m_settingsManager, SIGNAL(hdrSettingsChanged(const HdrSettings &)), m_hdrProcessor, SLOT(settingsChanged(const HdrSettings &)), Qt::QueuedConnection);

    // Video recording signals
    connect(m_settingsManager, SIGNAL(recordSettingsChanged(const RecordSettings &)), m_videoSaveProcessor, SLOT(settingsChanged(const RecordSettings &)), Qt::QueuedConnection);
    connect(m_mainWindow->ui->actionRecord, SIGNAL(toggled(bool)), this, SLOT(recordStateToggled(bool)), Qt::QueuedConnection);
    connect(m_mainWindow->ui->actionFlipImage, SIGNAL(toggled(bool)), m_videoSaveProcessor, SLOT(sceneFlipped(bool)), Qt::QueuedConnection);
    connect(m_videoSaveProcessor, SIGNAL(error(FrameProcessor::Error, const QString &)), this, SLOT(processorError(FrameProcessor::Error, const QString &)), Qt::QueuedConnection);
    connect(m_videoSaveProcessor, SIGNAL(error(FrameProcessor::Error, const QString &)), this, SLOT(recordError(FrameProcessor::Error, const QString &)), Qt::QueuedConnection);
    connect(m_videoSaveProcessor, SIGNAL(warning(FrameProcessor::Warning, const QString &)), this, SLOT(hdrWarning(FrameProcessor::Warning, const QString &)));
    connect(m_videoSaveProcessor, SIGNAL(stateChanged(FrameProcessor::State)), m_mainWindow, SLOT(recordStateChanged(FrameProcessor::State)), Qt::QueuedConnection);
    connect(m_videoSaveProcessor, SIGNAL(statusChanged(FrameProcessor::Status)), m_mainWindow, SLOT(recordStatusChanged(FrameProcessor::Status)), Qt::QueuedConnection);

	// Meta data of recorded video
#ifdef META_RECORDER
	connect(m_settingsManager, SIGNAL(cameraSettingsChanged(const CameraSettings &)), m_metadataRecorder, SLOT(settingsChanged(const CameraSettings &)), Qt::QueuedConnection);
	connect(m_videoSaveProcessor, SIGNAL(recordStarted(const QString &)), m_metadataRecorder, SLOT(recordStarted(const QString &)), Qt::QueuedConnection);
	connect(m_videoSaveProcessor, SIGNAL(recordStopped()), m_metadataRecorder, SLOT(recordStopped()), Qt::QueuedConnection);
#endif
        
    // frame pathways
    connect(m_cameraProcessor, SIGNAL(frameAvailable(AnnotatedFrame)), m_hdrProcessor, SLOT(process(AnnotatedFrame)), Qt::QueuedConnection);
    connect(m_hdrProcessor, SIGNAL(frameAvailable(AnnotatedFrame)), m_videoSaveProcessor, SLOT(process(AnnotatedFrame)), Qt::QueuedConnection);
	connect(m_hdrProcessor, SIGNAL(frameAvailable(AnnotatedFrame)), m_viewProcessor, SLOT(process(AnnotatedFrame)), Qt::QueuedConnection);
	connect(m_viewProcessor, SIGNAL(frameAvailable(AnnotatedFrame)), m_mainWindow, SLOT(displayFrame(AnnotatedFrame)), Qt::QueuedConnection);

	// view control
	connect(m_cameraView, SIGNAL(viewMouseWheelSignal(QWheelEvent*)), m_viewProcessor, SLOT(zoomControl(QWheelEvent*)), Qt::UniqueConnection);
	connect(m_cameraView, SIGNAL(viewMouseMoveSignal(QMouseEvent*)), m_viewProcessor, SLOT(panControl(QMouseEvent*)), Qt::UniqueConnection);

    // settings window
    connect(m_mainWindow->ui->actionSettings, SIGNAL(triggered()), this, SLOT(settingsInvoked()), Qt::UniqueConnection);

	// weld info view
	connect(m_mainWindow->ui->actionWeldInfo, SIGNAL(triggered()), this, SLOT(weldInfoInvoked()), Qt::UniqueConnection);
	connect(m_settingsManager, SIGNAL(recordSettingsChanged(const RecordSettings &)), m_weldInfoView, SLOT(targetDirChanged(const RecordSettings &)), Qt::UniqueConnection);

	// video management
#ifdef USE_STOPWATCH
	connect(m_settingsManager, SIGNAL(recordSettingsChanged(const RecordSettings &)), m_stopwatch, SLOT(settingsChanged(const RecordSettings &)), Qt::QueuedConnection);
	connect(m_stopwatch, SIGNAL(reset()), m_videoSaveProcessor, SLOT(resetWriter()), Qt::QueuedConnection);
	m_stopwatch->settingsChanged(rs);
#else
	connect(m_cameraProcessor, SIGNAL(resetVideoSaveProcessor()), m_videoSaveProcessor, SLOT(resetWriter()), Qt::UniqueConnection);
	connect(m_settingsManager, SIGNAL(recordSettingsChanged(const RecordSettings &)), m_cameraProcessor, SLOT(settingsChanged(const RecordSettings &)), Qt::QueuedConnection);
	m_cameraProcessor->settingsChanged(rs);
#endif

    m_cameraProcessor->setState(FrameProcessor::ActiveState);
    m_hdrProcessor->setState(m_settingsManager->currentSettings().hdrSettings.enabled ? FrameProcessor::ActiveState : FrameProcessor::LoadedState);

    setMainView(m_cameraView, true);
	m_running = true;
}

void MainController::stop()
{
	LogService::log(LogMessage("MainController::stop", LogMessage::Type::Trace));

    if (m_cameraProcessor) {
		m_cameraProcessor->setState(FrameProcessor::UnloadedState);
		delete m_cameraProcessor;
	}
    if (m_hdrProcessor) {
		m_hdrProcessor->setState(FrameProcessor::UnloadedState);
		delete m_hdrProcessor;
	}
    if (m_videoSaveProcessor) {
		m_videoSaveProcessor->setState(FrameProcessor::UnloadedState);
		delete m_videoSaveProcessor;
	}
	if (m_stopwatch){
		m_stopwatch->stopTimer();
		delete m_stopwatch;
	}

	setMainView(m_startView, false);
	m_running = false;
}

void MainController::handleCameraViewCloseEvent()
{
	m_settingsView->close();
	m_weldInfoView->close();
}

bool MainController::isRunning() {
	return m_running;
}

void MainController::changeUsbConnectionStatus(bool isConnected)
{
	emit usbConnectionChanged(isConnected);
}

PointGreyCtl * MainController::settingsView()
{
	return m_settingsView;
}

void MainController::hdrWarning(FrameProcessor::Warning warning, const QString & msg)
{
	LogService::log(LogMessage("MainController::hdrWarning", LogMessage::Type::Trace));

    m_statusBar->displayWarning(msg);
}

void MainController::setMainView(QWidget * view, bool showToolbars)
{
	LogService::log(LogMessage("MainController::setMainView", LogMessage::Type::Trace));

	m_mainWindow->takeCentralWidget();
    m_mainWindow->setCentralWidget(view);

    m_mainWindow->ui->toolBar->setVisible(showToolbars);
    m_mainWindow->ui->statusBar->setVisible(showToolbars);
}

void MainController::closeEvent(QCloseEvent *event)
{
	LogService::log(LogMessage("MainController::closeEvent", LogMessage::Type::Trace));

    if (m_cameraProcessor) m_cameraProcessor->setState(FrameProcessor::UnloadedState);
    
    if (m_hdrProcessor) m_hdrProcessor->setState(FrameProcessor::UnloadedState);

    if (m_videoSaveProcessor) m_videoSaveProcessor->setState(FrameProcessor::UnloadedState);
}

void MainController::recordStateToggled(bool started)
{
	LogService::log(LogMessage("MainController::recordStateToggled", LogMessage::Type::Trace));

    if (started) {
        m_videoSaveProcessor->setState(FrameProcessor::ActiveState); 
    } else { 
		//AAAAAAAAAAAAAAAAAAA
		m_videoSaveProcessor->setState(FrameProcessor::UnloadedState);
    }
}

void MainController::recordError(FrameProcessor::Error error, const QString & msg)
{
	LogService::log(LogMessage("MainController::recordError", LogMessage::Type::Trace));

    if (m_videoSaveProcessor->state() == FrameProcessor::UnloadedState) {
        m_mainWindow->ui->actionRecord->setChecked(false);
        m_mainWindow->ui->actionRecord->setEnabled(true);
    }
}

void MainController::settingsInvoked()
{
	LogService::log(LogMessage("MainController::settingsInvoked", LogMessage::Type::Trace));

    m_settingsView->setVisible(!m_settingsView->isVisible());
}

void MainController::weldInfoInvoked()
{
	LogService::log(LogMessage("MainController::weldInfoInvoked", LogMessage::Type::Trace));

	m_weldInfoView->setVisible(!m_weldInfoView->isVisible());
}

void MainController::settingsProfileSelected()
{
	LogService::log(LogMessage("MainController::settingsProfileSelected", LogMessage::Type::Trace));

    QAction * action = qobject_cast<QAction *>(sender());
    if (action) {
		QString profileName = action->text();
		QString actionName = action->data().toString();
		m_settingsManager->setProfileShortcutMap(profileName, actionName);
        qDebug() << "Setting profile selected: " << profileName;
        m_settingsManager->setCurrentProfile(profileName);
    }
}

void MainController::settingsProfilesChanged() 
{
	LogService::log(LogMessage("MainController::settingsProfilesChanged", LogMessage::Type::Trace));

    buildSettingsMenu();
}

void MainController::buildSettingsMenu()
{
	LogService::log(LogMessage("MainController::buildSettingsMenu", LogMessage::Type::Trace));

	Ui::MainWindow* ui = m_mainWindow->ui;
	build_drop_down_menu(ui->actionProfile1);
	build_drop_down_menu(ui->actionProfile2);
	build_drop_down_menu(ui->actionProfile3);
}

void MainController::build_drop_down_menu(QAction* profile_action)
{
	if (!profile_action)
		return;

	LogService::log(LogMessage("MainController::buildSettingsMenu", LogMessage::Type::Trace));

	QMenu * m = profile_action->menu();
	if (m != nullptr) {
		m->clear();
		delete m;
	}

	QMenu * settingsProfileMenu = new QMenu(m_mainWindow);
	QActionGroup * profilesGroup = new QActionGroup(settingsProfileMenu);

	//dynamically create action per profile based on the list of profiles in profile.json
	foreach(QString profileName, m_settingsManager->profileNames()) {
		QAction * action = new QAction(profileName, settingsProfileMenu);
		action->setCheckable(true);
		action->setObjectName(tr("action") + profileName);
		action->setData(profile_action->objectName());
		QString button_name = profile_action->objectName();
		connect(action, SIGNAL(triggered()), this, SLOT(settingsProfileSelected()));

		profilesGroup->addAction(action);
		settingsProfileMenu->addAction(action);

		ProfileSettings setting = m_settingsManager->getProfileSetting(profileName);
		if (setting.shortcutMapping.ActionName.compare(button_name) == 0){
			action->setChecked(true);
		}
	}

	QToolButton * settingsButton = (QToolButton *)m_mainWindow->ui->toolBar->widgetForAction(profile_action);

	if (settingsProfileMenu->actions().size() > 0) {
		settingsButton->setMenu(settingsProfileMenu);
		settingsButton->setPopupMode(QToolButton::MenuButtonPopup);
	}
	else {
		settingsButton->setMenu(nullptr);
		settingsButton->setPopupMode(QToolButton::DelayedPopup);
	}
}

void MainController::set_profile_to_checked()
{
	LogService::log(LogMessage("MainController::set_profile_to_checked", LogMessage::Type::Trace));

	QAction * action = qobject_cast<QAction *>(sender());
	if (action) {
		QToolButton * settingsButton = (QToolButton *)m_mainWindow->ui->toolBar->widgetForAction(action);
		QMenu* menu = settingsButton->menu();
		QList<QAction*> actions = menu->actions();
		foreach(QAction* one_action, actions){
			if (one_action->isChecked()){
				QString profileName = one_action->text();//one_action->data().toString();
				qDebug() << "Setting profile to checked: " << profileName;
				m_settingsManager->setCurrentProfile(profileName);
			}
		}
	}
}

void MainController::processorError(FrameProcessor::Error e, const QString & msg)
{
	LogService::log(LogMessage("MainController::processorError", LogMessage::Type::Trace));

    m_statusBar->displayError(msg);
}
