#pragma once

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QStandardPaths>

#include "Settings.h"

namespace Ui {
	class WeldInfoView;
}

class WeldInfoView : public QDialog
{
	Q_OBJECT

public:
	WeldInfoView(QString & serial, QWidget * parent = nullptr);
	~WeldInfoView();

public slots:
	void targetDirChanged(const RecordSettings &);

private slots:
	void save();

private:
	void writeToFile(const QString &);
	
	QString makeFileName();
	QString electricalCharacteristics();
	QString variables();

	Ui::WeldInfoView * ui;
	
	QString m_targetDir;

	QString m_serial;
};

