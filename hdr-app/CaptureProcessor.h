#pragma once

// Std lib
#include <iostream>
#include <fstream>
#include <functional> //for std::greater<> sort predicate
#include <memory>
#include <string>

// Qt
#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTimer>
#include <QDateTime>

// Local
#include "AnnotatedFrame.h"
#include "FlyCapture2.h"
#include "FrameProcessor.h"
#include "hdrDllExports.h"
#include "Settings.h"
#include "AltVideo.h"

// logger
#include "../qt-logger/LogService.h"

class CaptureProcessor : public FrameProcessor
{
	Q_OBJECT

public:
	static CaptureProcessor * createCapture(const FlyCapture2::PGRGuid &, const CameraSettings &, QObject * = 0);
	void pgFrameAvailable(FlyCapture2::Image img, FlyCapture2::ImageMetadata metadata);
	void setState(FrameProcessor::State state);

signals:
	void temperatureChanged(double);
	void resetVideoSaveProcessor();

	public slots:
	void settingsChanged(const CameraSettings &);
	void settingsChanged(const RecordSettings &);
	void recordStateToggled(const bool);

	private slots:
	void cameraTemperature();
	void convertFrame(const cv::Mat&);

private:
	explicit CaptureProcessor::CaptureProcessor(const FlyCapture2::PGRGuid &, const CameraSettings &, QObject *parent = 0);

	//Fields
	const static unsigned int k_HDRCtrl = 0x1800;

	const static unsigned int k_HDRShutter1 = 0x1820;
	const static unsigned int k_HDRShutter2 = 0x1840;
	const static unsigned int k_HDRShutter3 = 0x1860;
	const static unsigned int k_HDRShutter4 = 0x1880;

	const static unsigned int k_HDRGain1 = 0x1824;
	const static unsigned int k_HDRGain2 = 0x1844;
	const static unsigned int k_HDRGain3 = 0x1864;
	const static unsigned int k_HDRGain4 = 0x1884;

	const static unsigned int k_HDROn = 0x82000000;
	const static unsigned int k_HDROff = 0x80000000;

	const static unsigned int m_statsUpdateIntervalMs = 5000;

	std::unique_ptr<FlyCapture2::Camera> m_cam;
	FlyCapture2::PGRGuid m_camGuid;

	AnnotatedFrame m_lastFrame;
	CameraSettings m_settings;

	QTimer * m_cameraStatsUpdateTimer;

	//Methods
	void openCamera();
	void closeCamera();
	void startCapture();
	void stopCapture();
	void applyConfiguration();

	bool isError(const FlyCapture2::Error & err) const;
	void checkAndThrow(const FlyCapture2::Error &);
	void checkThrowChangeStatus(const FlyCapture2::Error &, const FrameProcessor::Status);

	void applyCameraProperties();
	void applyF7Settings();
	void applyEmbeddedInfoSettings();
	void applyRegisterSettings();
	void applyTriggerSettings();
	FlyCapture2::Property createProperty(FlyCapture2::PropertyType, double, bool = false, bool = false);
	FlyCapture2::Property createProperty(FlyCapture2::PropertyType, unsigned int, bool = false, bool = false);
	FlyCapture2::Property createProperty(FlyCapture2::PropertyType, unsigned int, unsigned int, bool = false, bool = false);
	void toggleHDRMode(bool hdrOn);

	void CaptureProcessor::printCameraInfo(const FlyCapture2::CameraInfo & pCamInfo);

	qint64 m_time;
	qint64 m_reset_interval;
	bool m_timer_active;
	//std::unique_ptr<AltVideo> m_alt;
	AltVideo* m_alt;
};