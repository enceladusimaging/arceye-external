#pragma once

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMap>
#include <QObject>
#include <QStandardPaths>
#include <QStringList>
#include <QString>

#include "Settings.h"

#include "../qt-logger/LogService.h"

class SettingsManager : public QObject
{
    Q_OBJECT

public:
	enum CameraType { SONY, CMOSIS };

    explicit SettingsManager(QString &, QString &, SettingsManager::CameraType, QObject * = 0);

    QStringList profileNames() const;
    ProfileSettings currentSettings() const;

    void setCurrentProfile(const QString &);
    void setCurrentSettings(const ProfileSettings &);
    void setRecordSettings(const RecordSettings &);

	//added when shortcuts to profile feature is requested
	ProfileSettings getProfileSetting(const QString &);
	void setProfileShortcutMap(const QString &, const QString &);

public slots:
    void save(const QString & = QString());
    void removeProfile();

signals:
    void profileChanged();
    void cameraSettingsChanged(const CameraSettings &);
    void hdrSettingsChanged(const HdrSettings &);
    void recordSettingsChanged(const RecordSettings &);
    void userProfileAvailabilityChanged();

private:
    void commit();
	void load();

    bool isBaseProfile(const ProfileSettings &);

    const QString m_baseProfileName;
	QString m_profilesFileName;
	QString m_camSerial;

	SettingsManager::CameraType m_cameraType;

    ProfileSettings defaultSettings();

    QMap<QString, ProfileSettings> m_userProfiles;
    ProfileSettings m_baseSettings, m_currentSettings;
};