#include "MainWindow.h"
#include "CentralController.h"

#include "../qt-logger/LogService.h"
#include "../qt-logger/LoggerFactory.h"
#include "../qt-logger/LatencyLoggerImpl.h"
#include "../qt-logger/ErrorLoggerImpl.h"
#include "../qt-logger/WarningLoggerImpl.h"
#include "../qt-logger/TraceLoggerImpl.h"
#include "../qt-logger/RuntimeStatsLoggerImpl.h"
#include "../qt-logger/SystemInfoLoggerImpl.h"

#include <QApplication>

#include <windows.h>

Q_DECLARE_METATYPE(AnnotatedFrame);
Q_DECLARE_METATYPE(FrameProcessor::State);
Q_DECLARE_METATYPE(FrameProcessor::Status);
Q_DECLARE_METATYPE(FrameProcessor::Error);
Q_DECLARE_METATYPE(FrameProcessor::Warning);
Q_DECLARE_METATYPE(RecordSettings);
Q_DECLARE_METATYPE(CameraSettings);
Q_DECLARE_METATYPE(HdrSettings);
Q_DECLARE_METATYPE(cv::Mat);

int main(int argc, char *argv[])
{

#ifdef _DEBUG
    AllocConsole();

    freopen("CONIN$", "r",stdin);
    freopen("CONOUT$", "w",stdout);
    freopen("CONOUT$", "w",stderr);
#endif

    // Register our types
    qRegisterMetaType<RecordSettings>();
    qRegisterMetaType<CameraSettings>();
    qRegisterMetaType<HdrSettings>();
	qRegisterMetaType<AnnotatedFrame>();
    qRegisterMetaType<FrameProcessor::State>();
    qRegisterMetaType<FrameProcessor::Status>();
    qRegisterMetaType<FrameProcessor::Error>();
    qRegisterMetaType<FrameProcessor::Warning>();
	qRegisterMetaType<cv::Mat>();

	// Show main window
	QApplication a(argc, argv);
	
	// set up log service
	LogService &ls = LogService::instance(&a);

	ILogger * infoLogger = LoggerFactory::createLogger(LoggerFactory::Type::InfoLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * errorLogger = LoggerFactory::createLogger(LoggerFactory::Type::ErrorLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * warningLogger = LoggerFactory::createLogger(LoggerFactory::Type::WarningLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * traceLogger = LoggerFactory::createLogger(LoggerFactory::Type::TraceLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * runtimeStatsLogger = LoggerFactory::createLogger(LoggerFactory::Type::RuntimeStatsLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * sysInfoLogger = LoggerFactory::createLogger(LoggerFactory::Type::SystemInfoLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);
	ILogger * latencyLogger = LoggerFactory::createLogger(LoggerFactory::Type::LatencyLoggerImpl, "log.txt", ILogger::LogLevel::Off, &ls);

	int ms = 5000;
	runtimeStatsLogger->setInterval(ms);
	sysInfoLogger->setInterval(ms);

	LogService::addLogger(infoLogger);
	LogService::addLogger(errorLogger);
	LogService::addLogger(warningLogger);
	LogService::addLogger(runtimeStatsLogger);
	LogService::addLogger(sysInfoLogger);
	LogService::addLogger(latencyLogger);
	LogService::addLogger(traceLogger);

	LogService::setOff();

	LogService::log(LogMessage("Runtime Stats", LogMessage::Type::RuntimeStats));

	CentralController centralController;

	// Start event loop
    a.setQuitOnLastWindowClosed(true);
	a.exec();
}