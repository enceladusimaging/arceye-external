#ifndef PROCESSINGTHREAD_H
#define PROCESSINGTHREAD_H

#include <memory>

// Qt
#include <QtCore/QThread>
#include <QPointer>

// Local
#include "Structures.h"
#include "Config.h"
#include "HdrProcessor.h"
#include "WindowedTimer.h"

class ProcessingWorker : public QObject
{
    Q_OBJECT

public:
	ProcessingWorker(QPointer<HdrProcessor> hdr) 
		: QObject(), 
		hdrProcessor(hdr) {};
    void stop();

private:
    QPointer<HdrProcessor> hdrProcessor;
	bool hdrEnabled;

public slots:
	void processFrame(QImage frame, unsigned int shutter);
	void hdrStateChanged(PointGreyOptions::HdrState state);

signals:
	void frameProcessed(const QImage frame);
};

#endif // PROCESSINGTHREAD_H
