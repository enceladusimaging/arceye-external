#pragma once

#include <QMutex>
#include <QMutexLocker>
#include <QVector>
#include <QWaitCondition>

// Macro that inserts copy constructor and operator= in private section of class
// Not ';' terminated to force a compile error if user doesn't correctly terminate line
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName &); \
    void operator=(const TypeName &)

template<typename T>
class ConcurrentQueue
{

public:
    explicit ConcurrentQueue(unsigned int capacity) 
        : m_queue()
        , m_capacity(capacity)
    {
        m_queue.reserve(m_capacity);
    }

    void waitAndPush(T item)
    {
        QMutexLocker lock(&m_mutex);
        while(m_queue.size() == m_queue.capacity()) {
            m_fullWaitCond.wait(&m_mutex);
        }

        m_queue.append(item);
        lock.unlock();

        m_emptyWaitCond.wakeOne();
    }

    bool tryPush(T item)
    {
        QMutexLocker lock(&m_mutex);
        bool success = (m_queue.size() < m_queue.capacity());

        if (success) m_queue.append(item);
        lock.unlock();
        m_emptyWaitCond.wakeOne();

        return success;
    }

    void waitAndPop(T &item)
    {
        QMutexLocker lock(&m_mutex);
        while (m_queue.size() == 0) {
            m_emptyWaitCond.wait(&m_mutex);
        }

        item = m_queue.takeFirst();
        lock.unlock();

        m_fullWaitCond.wakeOne();
    }

    bool tryPop(T &item)
    {
        QMutexLocker lock(&m_mutex);

        if (m_queue.isEmpty()) {
            return false;
        }

        item = m_queue.takeFirst();
        lock.unlock();
        m_fullWaitCond.wakeOne();

        return true;
    }

    void flush() {
        QMutexLocker lock(&m_mutex);

        m_queue.clear();
        m_queue.reserve(m_capacity);
    }

    bool full() const
    {
        QMutexLocker lock(&m_mutex);

        return m_queue.capacity() == m_queue.size();
    }

    bool empty() const
    {
        QMutexLocker lock(&m_mutex);

        return m_queue.isEmpty();
    }

    unsigned int size() const
    {
        QMutexLocker lock(&m_mutex);

        return m_queue.size();
    }

private:
    unsigned int m_capacity;
    QVector<T> m_queue;
    mutable QMutex m_mutex;
    QWaitCondition m_emptyWaitCond, m_fullWaitCond;

    DISALLOW_COPY_AND_ASSIGN(ConcurrentQueue);
};

