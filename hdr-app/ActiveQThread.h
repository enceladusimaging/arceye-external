#pragma once

#include <QMutex>
#include <QThread>
#include <QWaitCondition>

#include <functional>
#include <memory>

#include "../qt-logger/LogService.h"

#include "ConcurrentQueue.h"

// Macro that inserts copy constructor and operator= in private section of class
// Not ';' terminated to force a compile error if user doesn't correctly terminate line
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName &); \
    void operator=(const TypeName &)

typedef std::function<void()> ActiveCallback;

class ActiveQThread : public QThread
{
    Q_OBJECT

public:

    static std::unique_ptr<ActiveQThread> createActiveQThread(unsigned int bufferCapacity);

    virtual ~ActiveQThread();

    bool ready() const;

public slots:
    // blocking until it queues its item
    void send(ActiveCallback msg);
    // tries to queue and returns success
    bool trySend(ActiveCallback msg);
    // flush work queue and cancel all pending items
    void flush();

private:
    explicit ActiveQThread(unsigned int bufferCapacity = 100);

    // thread loop
    void run(); 
    // "end message" job - must be called by destructor
    void doDone();

    ConcurrentQueue<ActiveCallback> m_jobQueue;
    bool m_done;
    DISALLOW_COPY_AND_ASSIGN(ActiveQThread);
};

