#include "ViewControl.h"

ViewProcessor::ViewProcessor(QObject *parent, HdrSettings hs)
: FrameProcessor(parent)
, m_worker(ActiveQThread::createActiveQThread(10))
{
	m_param.centre = cv::Point(hs.width / 2, hs.height / 2);
	m_param.scale = 1.0f;
	m_param.img_size = cv::Size(hs.width, hs.height);
	m_param.img_rect = cv::Rect(0, 0, hs.width, hs.height);
}

void ViewProcessor::process(AnnotatedFrame f){
	m_worker->trySend(std::bind(&ViewProcessor::processAndReturn, this, f));
}

void ViewProcessor::processAndReturn(AnnotatedFrame frame){
	unsigned int size = frame.sizeBytes();

	m_param.TryInitialize(frame);

	cv::Mat tmp(frame.getHeight(), frame.getWidth(), CV_8UC3, frame.getConstBits().get(), frame.getWidth() * 3);
	cv::Mat cropped = tmp(m_param.img_rect);
	cv::Mat zoomed;
	cv::resize(cropped, zoomed, tmp.size(), 0, 0, CV_INTER_LINEAR);

	AnnotatedFrame::FrameMetadata metadata;
	unsigned int height = zoomed.rows, width = zoomed.cols;
	std::shared_ptr<unsigned char> imgData(new unsigned char[size]);
	std::memcpy(imgData.get(), zoomed.data, size);
	frame = AnnotatedFrame(imgData, size, width, height, metadata);

	emit frameAvailable(frame);
}


void ViewProcessor::zoomControl(QWheelEvent* e){
	if (e != nullptr){
		int value = e->delta() / 120;
		if (value == 1)
			m_param.ZoomIn();
		else if (value == -1)
			m_param.ZoomOut();
	}
}

void ViewProcessor::panControl(QMouseEvent* e){
	if (e->buttons() == Qt::MouseButton::LeftButton){
		const QPointF pos = e->localPos();

		if(m_x_pos != -1.0){
			if (m_x_pos < pos.x())
				m_param.PanLeft();
			else if (m_x_pos > pos.x())
				m_param.PanRight();
		}
		m_x_pos = pos.x();

		if (m_y_pos != -1.0){
			if (m_y_pos < pos.y())
				m_param.PanDown();
			else if (m_y_pos > pos.y())
				m_param.PanUp();
		}
		m_y_pos = pos.y();
	}
}

