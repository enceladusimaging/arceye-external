#pragma once

#include <memory>

#include <QImage>
#include <QList>

class AnnotatedFrame
{

public:

    struct FrameMetadata {
        unsigned int shutter;
        QList<bool> gpioPinStates;
    };

    AnnotatedFrame()
    {}

    AnnotatedFrame(std::shared_ptr<unsigned char> data, unsigned int size, unsigned int width, unsigned int height, const FrameMetadata & metadata)
        : m_data(data)
        , m_size(size)
        , m_width(width)
        , m_height(height)
        , m_metadata(metadata)
    {}

	inline QImage getFrame() const { return QImage(m_data.get(), m_width, m_height, QImage::Format::Format_RGB888); }
	inline unsigned int getWidth() const { return m_width; }
	inline unsigned int getHeight() const { return m_height; }
	inline FrameMetadata metadata() const { return m_metadata; }
    inline void setBits(unsigned char * data) { m_data.reset(data); }
    inline std::shared_ptr<unsigned char> getConstBits() const { return m_data; }
	inline unsigned int sizeBytes() const { return m_size; }

private:
    std::shared_ptr<unsigned char> m_data;
	unsigned int m_size, m_width, m_height, m_shutter;
    FrameMetadata m_metadata;
};

