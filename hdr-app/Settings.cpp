#include "Settings.h"

void CameraBasicSettings::read(const QJsonObject & json)
{
	brightness = json["brightness"].toDouble(); 

	exposure = json["exposure"].toDouble();
	exposureAuto = json["exposureAuto"].toBool();
	exposureEnabled = json["exposureEnabled"].toBool();

	sharpness = json["sharpness"].toInt();
	sharpnessAuto = json["sharpnessAuto"].toBool();
	sharpnessEnabled = json["sharpnessEnabled"].toBool();

	hue = json["hue"].toDouble();
	hueEnabled = json["hueEnabled"].toBool();

	saturation = json["saturation"].toDouble();
	saturationAuto = json["saturationAuto"].toBool();
	saturationEnabled = json["saturationEnabled"].toBool();

	gamma = json["gamma"].toDouble();
	gammaEnabled = json["gammaEnabled"].toBool();

	gain = json["gain"].toDouble();
	gainAuto = json["gainAuto"].toBool();
	gainEnabled = json["gainEnabled"].toBool();

	shutter = json["shutter"].toDouble();
	shutterAuto = json["shutterAuto"].toBool();
	shutterEnabled = json["shutterEnabled"].toBool();

	frameRate = json["frameRate"].toDouble();
	frameRateAuto = json["frameRateAuto"].toBool();
	frameRateEnabled = json["frameRateEnabled"].toBool();

	whiteBalRed = json["whiteBalRed"].toInt();
	whiteBalBlue = json["whiteBalBlue"].toInt();
	whiteBalAuto = json["whiteBalAuto"].toBool();
	whiteBalEnabled = json["whiteBalEnabled"].toBool();
}

void CameraBasicSettings::write(QJsonObject & json)
{
    json["brightness"] = brightness;
    
    json["exposure"] = exposure;
    json["exposureAuto"] = exposureAuto;
    json["exposureEnabled"] = exposureEnabled;

    json["sharpness"] = static_cast<int>(sharpness);
    json["sharpnessAuto"] = sharpnessAuto;
    json["sharpnessEnabled"] = sharpnessEnabled;

    json["hue"] = hue;
    json["hueEnabled"] = hueEnabled;

    json["saturation"] = saturation;
    json["saturationAuto"] = saturationAuto;
    json["saturationEnabled"] = saturationEnabled;

    json["gamma"] = gamma;
    json["gammaEnabled"] = gammaEnabled;

    json["gain"] = gain;
    json["gainAuto"] = gainAuto;
    json["gainEnabled"] = gainEnabled;

    json["shutter"] = shutter;
    json["shutterAuto"] = shutterAuto;
    json["shutterEnabled"] = shutterEnabled;

    json["frameRate"] = frameRate;
    json["frameRateAuto"] = frameRateAuto;
    json["frameRateEnabled"] = frameRateEnabled;

    json["whiteBalRed"] = static_cast<int>(whiteBalRed);
    json["whiteBalBlue"] = static_cast<int>(whiteBalBlue);
    json["whiteBalAuto"] = whiteBalAuto;
    json["whiteBalEnabled"] = whiteBalEnabled;
}

void CameraVideoMode::read(const QJsonObject & json) 
{
	width = json["width"].toInt();
	height = json["height"].toInt();
	left = json["left"].toInt();
	top = json["top"].toInt();
	mode = json["mode"].toInt();
    
	pixelFormat = QString(json["pixelFormat"].toString());
}

void CameraVideoMode::write(QJsonObject & json) 
{
	json["width"] = static_cast<int>(width);
	json["height"] = static_cast<int>(height);
	json["left"] = static_cast<int>(left);
	json["top"] = static_cast<int>(top);
	json["mode"] = static_cast<int>(mode);
    
	json["pixelFormat"] = pixelFormat;
}

void TriggeredCapture::read(const QJsonObject & json)
{
    enabled = json["enabled"].toBool();
	delayEnabled = json["delayEnabled"].toBool();

    mode = json["mode"].toInt();
	param = json["param"].toInt();
	source = json["source"].toInt();
	polarity = json["polarity"].toInt();

    delayS = json["delayS"].toDouble();
}

void TriggeredCapture::write(QJsonObject & json)
{
    json["enabled"] = enabled;
	json["delayEnabled"] = delayEnabled;

    json["mode"] = static_cast<int>(mode);
	json["param"] = static_cast<int>(param);
	json["source"] = static_cast<int>(source);
	json["polarity"] = static_cast<int>(polarity);

    json["delayS"] = delayS;
}

void TriggerStrobe::read(const QJsonObject & json)
{
    enabled = json["enabled"].toBool();

    source = json["source"].toInt();
	polarity = json["polarity"].toInt();

    delayMs = json["delayMs"].toDouble();
    durationMs = json["delayMs"].toDouble();
}

void TriggerStrobe::write(QJsonObject & json)
{
    json["enabled"] = enabled;

    json["source"] = static_cast<int>(source);
	json["polarity"] = static_cast<int>(polarity);

    json["delayMs"] = delayMs;
    json["durationMs"] = durationMs;
}

void CameraTriggerSettings::read(const QJsonObject & json) 
{
    TriggeredCapture tc;
    tc.read(json["triggeredCapture"].toObject());
    triggeredCapture = tc;

    strobeSettings.clear();
    QJsonArray strobesArr = json["strobeSettings"].toArray();
    for (int i = 0; i < strobesArr.size(); i++) {
        TriggerStrobe ts;
        ts.read(strobesArr[i].toObject());
        strobeSettings << ts;
    }

    reservedInputPins.clear();
    QJsonArray reservedInputs = json["reservedInputPins"].toArray();
    for (int i = 0; i < reservedInputs.size(); i++) {
        reservedInputPins << reservedInputs[i].toInt();
    }
}

void CameraTriggerSettings::write(QJsonObject & json)
{
    QJsonObject tc;
    triggeredCapture.write(tc);

    json["triggeredCapture"] = tc;

    QJsonArray strobesArr;
    foreach(TriggerStrobe ts, strobeSettings) {
        QJsonObject strobeObj;
        ts.write(strobeObj);
        strobesArr << strobeObj;
    }

    json["strobeSettings"] = strobesArr;

    QJsonArray reservedInputs;
    foreach(unsigned int resPin, reservedInputPins) reservedInputs << static_cast<int>(resPin);
    
    json["reservedInputPins"] = reservedInputs; 
}

void CameraAdvancedSettings::read(const QJsonObject & json)
{
	embeddedTimestamp = json["embeddedTimestamp"].toBool();
    embeddedGain = json["embeddedGain"].toBool();
    embeddedShutter = json["embeddedShutter"].toBool();
    embeddedBrightness = json["embeddedBrightness"].toBool();
    embeddedExposure = json["embeddedExposure"].toBool();
    embeddedWhiteBal = json["embeddedWhiteBal"].toBool();
    embeddedFrameCnt = json["embeddedFrameCnt"].toBool();
    embeddedStrobe = json["embeddedStrobe"].toBool();
    embeddedGpioPinState = json["embeddedGpioPinState"].toBool();
    embeddedRoiPos = json["embeddedRoiPos"].toBool();
}

void CameraAdvancedSettings::write(QJsonObject & json)
{
	json["embeddedTimestamp"] = embeddedTimestamp;
    json["embeddedGain"] = embeddedGain;
    json["embeddedShutter"] = embeddedShutter;
    json["embeddedBrightness"] = embeddedBrightness;
    json["embeddedExposure"] = embeddedExposure;
    json["embeddedWhiteBal"] = embeddedWhiteBal;
    json["embeddedFrameCnt"] = embeddedFrameCnt;
    json["embeddedStrobe"] = embeddedStrobe;
    json["embeddedGpioPinState"] = embeddedGpioPinState;
    json["embeddedRoiPos"] = embeddedRoiPos;
}

void CameraHdrSettings::read(const QJsonObject & json) 
{
	enabled = json["enabled"].toBool();

    shutters.clear();
    QJsonArray shuttersArr = json["shutters"].toArray();
    for (int i = 0; i < shuttersArr.size(); i++) {
        shutters.append(shuttersArr[i].toInt());
    }

    gains.clear();
    QJsonArray gainsArr = json["gains"].toArray();
    for (int i = 0; i < gainsArr.size(); i++) {
        gains.append(gainsArr[i].toInt());
    }
}

void CameraHdrSettings::write(QJsonObject & json) 
{
	json["enabled"] = enabled;

    QJsonArray shuttersArr;
    foreach (unsigned int shutter, shutters) {
        shuttersArr.append(static_cast<int>(shutter));
    }

    json["shutters"] = shuttersArr;

    QJsonArray gainArr;
    foreach (unsigned int gain, gains) {
        gainArr.append(static_cast<int>(gain));
    }

    json["gains"] = gainArr;
}

void CameraSettings::read(const QJsonObject & json)
{
    initialShutter = json["initialShutter"].toDouble();
    serialNumber = json["serial"].toInt();
	deltaEv = json["deltaEv"].toDouble();
    shutterConversionFactor = json["shutterConversionFactor"].toDouble();
    gainConversionFactor = json["gainConversionFactor"].toDouble();

	CameraBasicSettings cbs;
    cbs.read(json["cameraBasicSettings"].toObject());
	basicSettings = cbs;

	CameraVideoMode cvm;
    cvm.read(json["cameraVideoMode"].toObject());
	videoMode = cvm;

	CameraTriggerSettings cts;
    cts.read(json["cameraTriggerSettings"].toObject());
	triggerSettings = cts;

	CameraAdvancedSettings cas;
    cas.read(json["cameraAdvancedSettings"].toObject());
    advancedSettings = cas;

	CameraHdrSettings chs;
    chs.read(json["cameraHdrSettings"].toObject());
	hdrSettings = chs;
}

void CameraSettings::write(QJsonObject & json)
{
    json["serial"] = static_cast<int>(serialNumber);
    json["initialShutter"] = initialShutter;
    json["deltaEv"] = deltaEv;
    json["shutterConversionFactor"] = shutterConversionFactor;
    json["gainConversionFactor"] = gainConversionFactor;
    
	QJsonObject camBasicSettingsObj; 
	basicSettings.write(camBasicSettingsObj);
	json["cameraBasicSettings"] = camBasicSettingsObj;

	QJsonObject camVideoModeObj;
	videoMode.write(camVideoModeObj);
    json["cameraVideoMode"] = camVideoModeObj;

	QJsonObject camTriggerSettingsObj;
	triggerSettings.write(camTriggerSettingsObj);
    json["cameraTriggerSettings"] = camTriggerSettingsObj;

	QJsonObject camAdvancedSettingsObj;
    advancedSettings.write(camAdvancedSettingsObj);
    json["cameraAdvancedSettings"] = camAdvancedSettingsObj;

	QJsonObject camHdrSettingsObj;
	hdrSettings.write(camHdrSettingsObj);
    json["cameraHdrSettings"] = camHdrSettingsObj;
}

void HdrSettings::read(const QJsonObject & json)
{
    enabled = json["enabled"].toBool();
    numFrames = json["numFrames"].toInt();
    numColChannels = json["numColChannels"].toInt();
    width = json["width"].toInt();
    height = json["height"].toInt();
    ccrfPath = json["ccrfPath"].toString();
    ppemPath = json["ppemPath"].toString();
    exposurePref = json["expPref"].toDouble();

    shutters.clear();
    QJsonArray shuttersArr = json["shutters"].toArray();
    for (int i = 0; i < shuttersArr.size(); i++) {
        shutters.append(shuttersArr[i].toInt());
    }

	gains.clear();
	QJsonArray gainsArr = json["gains"].toArray();
	for (int i = 0; i < gainsArr.size(); i++) {
		gains.append(gainsArr[i].toInt());
	}
}

void HdrSettings::write(QJsonObject & json)
{
    json["enabled"] = enabled;
    json["numFrames"] = static_cast<int>(numFrames);
    json["numColChannels"] = static_cast<int>(numColChannels);
    json["width"] = static_cast<int>(width);
    json["height"] = static_cast<int>(height);
    json["ccrfPath"] = ccrfPath;
    json["ppemPath"] = ppemPath;
    json["expPref"] = exposurePref;

    QJsonArray shuttersArr;
    foreach (unsigned int shutter, shutters) {
        shuttersArr.append(static_cast<int>(shutter));
    }

    json["shutters"] = shuttersArr;

	QJsonArray gainsArr;
	foreach(unsigned int gain, gains) {
		gainsArr.append(static_cast<int>(gain));
	}

	json["gains"] = gainsArr;
}

void RecordSettings::read(const QJsonObject & json)
{
	targetDir = QString(json["targetDir"].toString());
	codec = QString(json["codec"].toString());

    diskFullPercentThreshold = json["diskFullPercentThreshold"].toInt();
	width = json["width"].toInt();
	height = json["height"].toInt();

    frameRate = json["frameRate"].toDouble();

    frameRateOverride = json["frameRateOverride"].toBool();

    arcOnRecording = json["arcOnRecording"].toBool();
    arcOnRecordingPin = json["arcOnRecordingPin"].toInt();

	timeInterval = json["timeInterval"].toInt();
	if (timeInterval < 1)
		timeInterval = 1;
	else if (timeInterval > 60)
		timeInterval = 60;
}

void RecordSettings::write(QJsonObject & json) 
{
	json["targetDir"] = targetDir;
	json["codec"] = codec;

    json["diskFullPercentThreshold"] = static_cast<int>(diskFullPercentThreshold);
	json["width"] = static_cast<int>(width);
	json["height"] = static_cast<int>(height);

    json["frameRate"] = frameRate;

    json["frameRateOverride"] = frameRateOverride;

    json["arcOnRecording"] = arcOnRecording;
    json["arcOnRecordingPin"] = static_cast<int>(arcOnRecordingPin);

	json["timeInterval"] = static_cast<int>(timeInterval);
}

void ShortcutMapping::read(const QJsonObject & json)
{
	ActionName = QString(json["ActionName"].toString());
}

void ShortcutMapping::write(QJsonObject & json)
{
	json["ActionName"] = ActionName;
}

void ProfileSettings::read(const QJsonObject & json)
{
    name = json["name"].toString();
    removable = json["removable"].toBool();

    CameraSettings cs;
    cs.read(json["cameraSettings"].toObject());
    cameraSettings = cs;

    HdrSettings hdr;
    hdr.read(json["hdrSettings"].toObject());
    hdrSettings = hdr;

	RecordSettings rec;
    rec.read(json["recordSettings"].toObject());
    recordSettings = rec;

	ShortcutMapping map;
	map.read(json["shortcutMapping"].toObject());
	shortcutMapping = map;
}

void ProfileSettings::write(QJsonObject & json)
{
    json["name"] = name;
    json["removable"] = removable;
    
	QJsonObject camSettingsObj;
    cameraSettings.write(camSettingsObj);
    json["cameraSettings"] = camSettingsObj;

    QJsonObject hdrSettingsObj;
    hdrSettings.write(hdrSettingsObj);
    json["hdrSettings"] = hdrSettingsObj;

	QJsonObject recordSettingsObj;
    recordSettings.write(recordSettingsObj);
    json["recordSettings"] = recordSettingsObj;

	QJsonObject shortcutMappingObj;
	shortcutMapping.write(shortcutMappingObj);
	json["shortcutMapping"] = shortcutMappingObj;
}