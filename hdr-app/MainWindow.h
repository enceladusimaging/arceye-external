#pragma once

// Qt
#include <QCloseEvent>
#include <QErrorMessage>
#include <QInputDialog>
#include <QMainWindow>
#include <QMenu>
#include <QPushButton>
#include <QToolButton>

// Local
#include "CameraBusManager.h"
#include "CameraView.h"
#include "CaptureProcessor.h"
#include "FrameProcessor.h"
#include "HdrProcessor.h"
#include "PointGreyCtl.h"
#include "SettingsManager.h"
#include "StartView.h"
#include "VideoMetadataRecorder.h"
#include "VideoSaveProcessor.h"
#include "Settings.h"

// logger
#include "../qt-logger/LogService.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

	Ui::MainWindow * ui;

signals:
    void display(const QImage &, bool);
	void closed();

protected:
    void mouseDoubleClickEvent(QMouseEvent *);
	void closeEvent(QCloseEvent *);

public slots:
    void displayFrame(AnnotatedFrame);
    void recordStateChanged(FrameProcessor::State);
    void recordStatusChanged(FrameProcessor::Status);
};


