#ifndef POINTGREYCTL_H
#define POINTGREYCTL_H

#include <QAbstractButton>
#include <QCheckBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QDebug>
#include <QKeyEvent>
#include <QMap>
#include <QLabel>
#include <QSpinBox>
#include <QVector>

#include "SettingsManager.h"
#include "Settings.h"

#include "../qt-logger/LogService.h"

namespace Ui {
class PointGreyCtl;
}

class PointGreyCtl : public QDialog
{
    Q_OBJECT

public:
    explicit PointGreyCtl(SettingsManager *, QWidget * = 0);
	~PointGreyCtl();

    void updateSettings(const ProfileSettings &);

public slots:
    void activeProfileChanged();
    void recordStateToggled(bool);

private slots:
    // trigger
    void triggerFunctionalityEnabled(bool);
    void triggeredCapturePinChanged(int);
    void triggeredRecordPinChanged(int);
    void adjustStrobePinStates();

    void triggerDelaySliderValueChanged(int);
    void triggerDelaySpinValueChanged(double);

	void linkedHdrControlsValueChanged(double = 0);

    void mainShutterChanged(double);
    void shutterChanged(int);
    void gainChanged(int);
    void hdrCameraToggled(bool);
    void sliderLockToggled(bool);

    // record
    void recordFolderClicked();

	// profile removal
	void removeProfileClicked();

    // control button box
    void saveProfile();
    void saveProfileAs();
    void applySettings();

private:
    Ui::PointGreyCtl *ui;
    
    SettingsManager * m_settingsManager;

    QList<QSpinBox *> m_shutterSpins, m_gainSpins;
    QList<QLabel *> m_shutterLabels, m_gainLabels;

    const QString GAIN_LABEL_TEMPLATE, SHUTTER_LABEL_TEMPLATE, FPS_LABEL_TEMPLATE, WINDOW_TITLE_TEMPLATE
        , TRIGGER_STROBE_ENABLED_NAME, TRIGGER_STROBE_POLARITY_NAME, TRIGGER_STROBE_DELAY_NAME, TRIGGER_STROBE_DURATION_NAME;

	inline double maxFpsFromShutterMs(double shutterVal) { return 1.0 / (shutterVal / 1000); };
    inline double msShutterFromIntegral(int integralShutter) { return m_settingsManager->currentSettings().cameraSettings.shutterConversionFactor * integralShutter; } ;
    inline double dbGainFromIntegral(int integralGain) { return m_settingsManager->currentSettings().cameraSettings.gainConversionFactor * integralGain; };
    inline int integralShutterFromMs(double shutterMs) { return shutterMs / m_settingsManager->currentSettings().cameraSettings.shutterConversionFactor; } ;
    inline int integralGainFromDb(double gainDb) { return gainDb / m_settingsManager->currentSettings().cameraSettings.gainConversionFactor; };
	
    void clampFpsOnMaxShutter();
    void setupHdrControls(const QVector<unsigned int> &, const QVector<unsigned int> &); 
};

#endif // POINTGREYCTL_H
