#include "HdrProcessor.h"

static unsigned int cudaStreamId = 0;

HdrProcessor * HdrProcessor::createHdr(const HdrSettings & settings, QObject * parent)
{
	LogService::log(LogMessage("HdrProcessor::createHdr", LogMessage::Type::Trace));

    const size_t ccrf_dim = 1024;
	const size_t ppem_dim = 1024;
	const size_t conv_dim = 256;

	auto conversion_values = HdrContext::responseConversion(HdrContext::cameraType_t::SONY, HdrContext::cameraType_t::CANON, 0.5f, conv_dim);
	auto conversionTable = Table(std::move(conversion_values), conv_dim, 1);

	auto ppemTable = Table{ settings.ppemPath.toStdString(), ppem_dim, ppem_dim };

    hdrSpec_t hdrSpec;
    hdrSpec.width = settings.width;
    hdrSpec.height = settings.height;
    hdrSpec.colDepth = settings.numColChannels;
    hdrSpec.nFrames = settings.numFrames;
    hdrSpec.streamId = ++cudaStreamId;
	hdrSpec.cameraResponse = HdrContext::getCameraResponse(HdrContext::cameraType_t::CANON);
    hdrSpec.ccrf = nullptr;
	hdrSpec.fastPpem = nullptr;
	hdrSpec.ppem = &ppemTable;
	hdrSpec.ev = 1.0f;
	hdrSpec.ccrf_length = ccrf_dim;
	hdrSpec.ppem_length = ppem_dim;
	hdrSpec.converter = &conversionTable;

    HdrContext hdrCtxt(hdrSpec);

	hdrCtxt.allocate(ccrf_dim*ccrf_dim*sizeof(float), ppem_dim*ppem_dim*sizeof(float), 256*sizeof(float));
	hdrCtxt.copyTable(hdrSpec.ppem->data(), hdrSpec.ppem->sizeBytes(), HdrContext::tableType_t::PPEM);
	hdrCtxt.copyTable(hdrSpec.converter->data(), hdrSpec.converter->sizeBytes(), HdrContext::tableType_t::CONVERTER);

    return new HdrProcessor(settings, std::move(hdrCtxt), parent);
}

HdrProcessor::HdrProcessor(const HdrSettings & settings, HdrContext hdrContext, QObject * parent) 
    : FrameProcessor(parent)
    , m_worker(ActiveQThread::createActiveQThread(FRAME_BUFFER_CAPACITY))
    , m_settings(settings)
    , m_context(std::move(hdrContext))
{
	createCompositingTableGPU(m_context);
	createFastPPEMTableGPU(m_context);
}

void HdrProcessor::setState(FrameProcessor::State state)
{
	LogService::log(LogMessage("HdrProcessor::setState", LogMessage::Type::Trace));

    FrameProcessor::State previousState = this->state(), nextState;

    if (previousState == state)
        return;

    if (previousState == FrameProcessor::UnloadedState) {
        if (openHdr()) {
            nextState = FrameProcessor::LoadedState;
            applySettings();
        }

        if (state == FrameProcessor::ActiveState) {
            nextState = FrameProcessor::ActiveState;
        }
    } else if (previousState == FrameProcessor::LoadedState) {
        if (state == FrameProcessor::UnloadedState) {
            closeHdr(); //clean up HDR memory
        } else if (state == FrameProcessor::ActiveState) {
            // nothing to do
        }

        nextState = state;
    } else if (previousState == FrameProcessor::ActiveState) {
        stopHdr();
        if (state == FrameProcessor::UnloadedState) {
            closeHdr();
        } else if (state == FrameProcessor::LoadedState) {
           // nothing specific to do
        }

        nextState = state;
    }

    changeState(nextState);
}


bool HdrProcessor::openHdr()
{
	LogService::log(LogMessage("HdrProcessor::openHdr", LogMessage::Type::Trace));

    return true;
}

void HdrProcessor::stopHdr()
{
	LogService::log(LogMessage("HdrProcessor::stopHdr", LogMessage::Type::Trace));

    m_worker->flush();
}

void HdrProcessor::closeHdr()
{
	LogService::log(LogMessage("HdrProcessor::closeHdr", LogMessage::Type::Trace));

    while (!m_worker->trySend(std::bind(&HdrProcessor::shutdownGpu, this)));  
}

void HdrProcessor::applySettings()
{
	LogService::log(LogMessage("HdrProcessor::applySettings", LogMessage::Type::Trace));

#ifdef false
 //   const unsigned int resolutionX = m_settings.width;
 //   const unsigned int resolutionY = m_settings.height;
 //   const unsigned int numHdrFrames = m_settings.numFrames;
 //   const unsigned int numColorChannels = m_settings.numColChannels;
 //   const double expPref = m_settings.exposurePref;

	//bool initReq = false;

	//if (resolutionX != m_width) {
	//	m_width = resolutionX;
	//	initReq = true;
	//}

	//if (resolutionY != m_height) {
	//	m_height = resolutionY;
	//	initReq = true;
	//}

	//if (numHdrFrames != m_numFrames) {
	//	m_numFrames = numHdrFrames;
	//	initReq = true;
	//}

	//if (numColorChannels != m_colChannels) {
	//	m_colChannels = numColorChannels;
	//	initReq = true;
	//}

 //   if (expPref != m_expPref) {
 //       m_expPref = expPref;
 //   }
#endif

	//Grab the HDR shutters from the struct and sort them
	QVector<uint> new_shutters = m_settings.shutters;
	std::sort(new_shutters.begin(), new_shutters.end(), std::greater<unsigned int>());

	QVector<uint> new_gains = m_settings.gains;
	std::sort(new_gains.begin(), new_gains.end(), std::greater<unsigned int>());

	bool setting_changed = false;
	for (unsigned int i = 0; i < new_shutters.length(); i++){
		setting_changed |= (m_shutters.value(i) != new_shutters.value(i));
		setting_changed |= (m_gains.value(i) != new_gains.value(i));
	}

	m_shutters = new_shutters;
	m_gains = new_gains;

	//AAAAAA TODO: recalculate for change in delta ev

 //   if(initReq) {
	//	std::vector<float> v(resolutionX * 720, 3.0);
 //       hdrInit(m_width, m_height, m_colChannels, m_numFrames, m_CCRF.get(), m_PPEM.get(), v.data());
	//}
}

void HdrProcessor::process(AnnotatedFrame f) 
{
	LogService::log(LogMessage("HdrProcessor::process", LogMessage::Type::Trace));

    if (FrameProcessor::ActiveState == state()) {
        unsigned int shutter = f.metadata().shutter;
        unsigned char * imgData = f.getConstBits().get();

        if (shutter <= 0) {
			QString errorMsg = QString::fromUtf8("HDR processing: shutter cannot be <= 0.");
            emit error(FrameProcessor::InvalidRequestError, errorMsg);
			LogService::log(LogMessage(errorMsg, LogMessage::Type::Error));
            return;
        }

        auto frameIdx = m_shutters.indexOf(shutter);

	    if(frameIdx >= 0) {
            if (m_worker->ready()) {
                // copy the data we're given straight to the GPU
                frameCopyToDevice(m_context, imgData, f.sizeBytes(), frameIdx);
            } else {
				QString warningMsg = "Frame dropped in HDR processing.";
                emit warning(FrameProcessor::UnableToProcessFrameWarning, warningMsg);
				LogService::log(LogMessage(warningMsg, LogMessage::Type::Warning));
                return;
            }
    
		    // process and return the data in the provided pointer
            m_worker->trySend(std::bind(&HdrProcessor::processAndReturn, this, &m_context, f, frameIdx));
        }
    } else if (FrameProcessor::LoadedState == state()) {
        // if we are the Loaded state it means that HDR processing is disabled
        // short circuit the input frame
        emit frameAvailable(f);
    }


}

//AAAAAAAAAAAAAAAAAAAA
void HdrProcessor::settingsChanged(const HdrSettings & settings)
{
	LogService::log(LogMessage("HdrProcessor::settingsChanged", LogMessage::Type::Trace));

    m_settings = settings;

    this->setState(FrameProcessor::LoadedState);
    applySettings();

    if (m_settings.enabled) {
        this->setState(FrameProcessor::ActiveState);
    }
}

void HdrProcessor::processAndReturn(HdrContext * p_context, AnnotatedFrame frame, unsigned int frameIndex)
{
	LogService::log(LogMessage("HdrProcessor::processAndReturn", LogMessage::Type::Trace));
	unsigned int size = frame.sizeBytes();

    renderHDR(*p_context, frameIndex, (float)m_settings.exposurePref);

    unsigned char* hdrData = new unsigned char[frame.sizeBytes()];
    frameCopyToHost(*p_context, hdrData, size);

    frame.setBits(hdrData);

    // fire signal from worker thread - should be safe as Qt signals are thread safe
    emit frameAvailable(frame);
}

void HdrProcessor::shutdownGpu()
{
	LogService::log(LogMessage("HdrProcessor::shutdownGpu", LogMessage::Type::Trace));
}