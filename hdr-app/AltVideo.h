#pragma once
#include "ActiveQThread.h"

#include <QObject>

#include "boost/filesystem.hpp"

#include <opencv/cv.h>
#include <opencv/highgui.h>

class AltVideo : public QObject{

	Q_OBJECT

public:
	AltVideo(QObject* parent);
	AltVideo(const char* path, QObject* parent);
	~AltVideo();

	void start();
	void stop();

	void fetch(cv::VideoCapture& cap, cv::Mat& img);

signals:
	void frameArrived(const cv::Mat&);

private:
	std::unique_ptr<ActiveQThread> m_worker;
	cv::VideoCapture m_capture;
	bool m_active;
	cv::Mat m_img;
};