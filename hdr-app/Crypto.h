#pragma once

// OpenSSL
#include <openssl/sha.h>

// Qt
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QChar>

class Crypto : public QObject
{
	Q_OBJECT

public:
	Crypto(QString &, QObject * parent = nullptr);
	~Crypto();
	
	void createHashStorageFile(QString &);
	QString fileName();
	bool authenticate(QString &);

private:
	QString createHash(QString &);

	QString m_fileName;
};

