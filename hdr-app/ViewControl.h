#pragma once

// Std lib
#include <iostream>
#include <fstream>
#include <functional> //for std::greater<> sort predicate
#include <memory>

// Qt
#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTimer>
#include <QDateTime>
#include <QWheelEvent>
#include <QMouseEvent>

// opencv
#include <opencv/cv.h>

// Local
#include "ActiveQThread.h"
#include "AnnotatedFrame.h"
#include "FrameProcessor.h"
#include "Settings.h"

class ViewProcessor : public FrameProcessor{
	Q_OBJECT

public:
	ViewProcessor(QObject *parent, HdrSettings hs);

	void setState(FrameProcessor::State state){}

signals:

public slots:
	void process(AnnotatedFrame);
	void zoomControl(QWheelEvent*);
	void panControl(QMouseEvent*);

private:
	void processAndReturn(AnnotatedFrame);

	ZoomParam m_param;
	std::unique_ptr<ActiveQThread> m_worker;

	double m_x_pos = -1.0;
	double m_y_pos = -1.0;
};