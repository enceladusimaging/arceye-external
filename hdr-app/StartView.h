#pragma once

#include <QWidget>

#include "../qt-logger/LogService.h"

namespace Ui {
    class StartView;
}

class StartView : public QWidget
{
    Q_OBJECT

public:
    StartView(QWidget* = 0);

public slots:
	void usbConnected(bool isConnected);

private:
    Ui::StartView *ui;

signals:
    void startClicked();

};

