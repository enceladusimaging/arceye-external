#pragma once

#include <QElapsedTimer>
#include <QObject>

#include "../qt-logger/LogService.h"

class AveragedElapsedTimer : QObject
{
public:
    AveragedElapsedTimer(unsigned int, QObject *);

    double lastIntervalAverage() const;
    bool isValid() const;

public slots:
    void start();
    void eventOccurred();

private:
    QElapsedTimer m_timer;
    unsigned int m_sampleIdx, m_totalSamples;
    double m_lastAvg;
    bool m_valid;
};

