#pragma once

#include <QObject>

#include "CameraBusManager.h"
#include "SettingsManager.h"
#include "MainController.h"
#include "Crypto.h"

class CentralController : public QObject
{
	Q_OBJECT

public:
	CentralController(QObject *parent = nullptr);
	~CentralController();

private slots:
	void cameraConnectedUsb(unsigned int, FlyCapture2::PGRGuid);
	void cameraDisconnectedUsb(unsigned int);
	void busManagerError(CameraBusManager::Error, const QString &);

private:
	CameraBusManager * m_busManager;

	QString m_camera1Serial;
	QString m_camera2Serial;

	SettingsManager * m_settingsManager1;
	SettingsManager * m_settingsManager2;

	Crypto * m_authenticator1;
	Crypto * m_authenticator2;

	MainController * m_mainController1;
	MainController * m_mainController2;

	QMap<unsigned int, MainController *> * m_cameraMap;

	QErrorMessage m_errorMsgbox;
};
