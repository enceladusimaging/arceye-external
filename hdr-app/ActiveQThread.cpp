#include "ActiveQThread.h"

// Factory: safe construction of object before thread start
std::unique_ptr<ActiveQThread> ActiveQThread::createActiveQThread(unsigned int bufferCapacity)
{
    std::unique_ptr<ActiveQThread> active(new ActiveQThread(bufferCapacity));

    active->start();

    return active;
}

// graceful exit - put end message and let the queue empty out
ActiveQThread::~ActiveQThread(void)
{
    ActiveCallback quitMsg = std::bind(&ActiveQThread::doDone, this);
    send(quitMsg);
    wait();
}

bool ActiveQThread::ready() const
{
	LogService::log(LogMessage("ActiveQThread::ready", LogMessage::Type::Trace));
    return !m_jobQueue.full();
}

// background job to queue
void ActiveQThread::send(ActiveCallback msg)
{
	LogService::log(LogMessage("ActiveQThread::send", LogMessage::Type::Trace));
    m_jobQueue.waitAndPush(msg);
}

// background job to queue
bool ActiveQThread::trySend(ActiveCallback msg)
{
	LogService::log(LogMessage("ActiveQThread::trySend", LogMessage::Type::Trace));
    return m_jobQueue.tryPush(msg);
}

void ActiveQThread::flush()
{
	LogService::log(LogMessage("ActiveQThread::flush", LogMessage::Type::Trace));
    m_jobQueue.flush();
}

// private - only called thru factory function
ActiveQThread::ActiveQThread(unsigned int bufferCapacity)
    : QThread()
    , m_jobQueue(bufferCapacity)
    , m_done(false)
{}

void ActiveQThread::run()
{
	LogService::log(LogMessage("ActiveQThread::run", LogMessage::Type::Trace));
    while(!m_done) {
        ActiveCallback func;
        m_jobQueue.waitAndPop(func);
        func();
    }
}

void ActiveQThread::doDone()
{
	LogService::log(LogMessage("ActiveQThread::doDone", LogMessage::Type::Trace));
    m_done = true;
}
