#pragma once

#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QMap>
#include <QString>
#include <QTextStream>
#include <QVariant>
#include <QVector>

//opencv
#include <opencv/cv.h>

//local
#include "AnnotatedFrame.h"

class JsonOutput
{
    virtual void read(const QJsonObject &) = 0;
    virtual void write(QJsonObject &) = 0;
};

struct CameraBasicSettings : JsonOutput
{
    double brightness;
    double exposure;
    bool exposureAuto, exposureEnabled;

    unsigned int sharpness;
    bool sharpnessAuto, sharpnessEnabled;

    double hue;
    bool hueEnabled;

    double saturation;
    bool saturationAuto, saturationEnabled;

    double gamma;
    bool gammaEnabled;

    double gain;
    bool gainAuto, gainEnabled;

    double shutter;
    bool shutterAuto, shutterEnabled;

    double frameRate;
    bool frameRateAuto, frameRateEnabled;

    unsigned int whiteBalRed, whiteBalBlue;
    bool whiteBalAuto, whiteBalEnabled;

    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct CameraVideoMode : JsonOutput
{
    unsigned int width, height, left, top, mode;
    QString pixelFormat;

	virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct TriggeredCapture : JsonOutput
{
    bool enabled, delayEnabled;
    unsigned int mode, param, source, polarity;
    double delayS;

    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct TriggerStrobe : JsonOutput
{
    bool enabled;
    unsigned int source, polarity;
    double delayMs, durationMs;
    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct CameraTriggerSettings : JsonOutput
{
    TriggeredCapture triggeredCapture;
    QVector<TriggerStrobe> strobeSettings;
    QVector<unsigned int> reservedInputPins; 

	virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct CameraAdvancedSettings : JsonOutput
{
    bool embeddedTimestamp
        , embeddedGain
        , embeddedShutter
        , embeddedBrightness
        , embeddedExposure
        , embeddedWhiteBal
        , embeddedFrameCnt
        , embeddedStrobe
        , embeddedGpioPinState
        , embeddedRoiPos;

	virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct CameraHdrSettings : JsonOutput
{
    bool enabled;
    QVector<unsigned int> shutters;
    QVector<unsigned int> gains;

	virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct CameraSettings : public JsonOutput
{
    unsigned int serialNumber;
    double initialShutter, deltaEv;
    double shutterConversionFactor, gainConversionFactor;

    CameraBasicSettings basicSettings;
    CameraVideoMode videoMode;
    CameraTriggerSettings triggerSettings;
    CameraAdvancedSettings advancedSettings;
    CameraHdrSettings hdrSettings;

    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct HdrSettings : JsonOutput
{
    bool enabled;
    QVector<unsigned int> shutters;
	QVector<unsigned int> gains;
    unsigned int numFrames, numColChannels, width, height;
    QString ccrfPath, ppemPath;
    double exposurePref;

    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct RecordSettings : JsonOutput
{
    QString targetDir, codec;
    unsigned int diskFullPercentThreshold, width, height;
    double frameRate;
    bool frameRateOverride, arcOnRecording;
    unsigned int arcOnRecordingPin; 
	unsigned int timeInterval;

	virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct ShortcutMapping : JsonOutput{
	QString ActionName;
	virtual void read(const QJsonObject &);
	virtual void write(QJsonObject &);
};

struct ProfileSettings : JsonOutput
{
    QString name;
    bool removable;
    CameraSettings cameraSettings;
    HdrSettings hdrSettings;
	RecordSettings recordSettings;
	ShortcutMapping shortcutMapping;

    virtual void read(const QJsonObject &);
    virtual void write(QJsonObject &);
};

struct ZoomParam{
public:
	float scale;
	cv::Point centre;
	cv::Size img_size;
	cv::Rect img_rect;

	void Reset(AnnotatedFrame f){
		centre = cv::Point(f.getWidth() / 2, f.getHeight() / 2);
		scale = 1.0f;
		img_size = cv::Size(f.getWidth(), f.getHeight());
		img_rect = cv::Rect(0, 0, f.getWidth(), f.getHeight());
	}

	void TryInitialize(AnnotatedFrame f){
		if (!m_initialized){
			Reset(f);
			m_initialized = true;
		}
	}

	void ZoomIn(){
		scale -= zoom_step;
		bound_scale();
		update_rect();
		validate_rect();
	}

	void ZoomOut(){
		scale += zoom_step;
		bound_scale();
		update_rect();
		validate_rect();
	}

	void UpdateCentre(int x, int y){
		centre.x = x;
		centre.y = y;
		validate_rect();
	}

	void PanLeft(){
		UpdateCentre(centre.x - pan_step, centre.y);
	}

	void PanRight(){
		UpdateCentre(centre.x + pan_step, centre.y);
	}

	void PanUp(){
		UpdateCentre(centre.x, centre.y + pan_step);
	}

	void PanDown(){
		UpdateCentre(centre.x, centre.y - pan_step);
	}

private:
	void bound_scale(){
		if (scale > 1.0f)
			scale = 1.0f;
		else if (scale < 0.01f)
			scale = 0.01f;
	}

	void update_rect(){
		img_rect.width = img_size.width*scale + 0.5f;
		img_rect.height = img_size.height*scale + 0.5f;
	}

	void validate_rect(){
		img_rect.x = centre.x - img_rect.width / 2;
		img_rect.y = centre.y - img_rect.height / 2;

		int rx1 = img_rect.x + img_rect.width;
		int ry1 = img_rect.y + img_rect.height;

		//check if rectangle's xmin and xmax values are within the image
		if (img_rect.x < 0)
			img_rect.x = 0;
		else if (rx1 > img_size.width)
			img_rect.x -= (rx1 - img_size.width);

		//check if rectangle's ymin and ymax values are within the image
		if (img_rect.y < 0)
			img_rect.y = 0;
		else if (ry1 > img_size.height)
			img_rect.y -= (ry1 - img_size.height);

		//sync the centre if rectangle crosses outside of image
		centre.x = img_rect.x + img_rect.width / 2;
		centre.y = img_rect.y + img_rect.height / 2;
	}

	float zoom_step = 0.05f;
	int pan_step = 5;
	bool m_initialized = false;
};