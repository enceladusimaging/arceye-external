#pragma once

#include <QObject>
#include <QString>

#include "AnnotatedFrame.h"

//#define PRINT

class FrameProcessor : public QObject
{
    Q_OBJECT

public:
	enum State { UnloadedState, LoadedState, ActiveState, ResetState };
    enum Status { UnavailableStatus, UnloadedStatus, UnloadingStatus, LoadingStatus, LoadedStatus, StartingStatus, StoppingStatus, ActiveStatus };
    enum Warning { UnableToProcessFrameWarning };
    enum Error { NoError, CameraError, InvalidRequestError, ServiceMissingError, NotSupportedFeatureError };

    FrameProcessor(QObject * parent = 0) 
        : QObject(parent)
        , m_state(UnloadedState)
        , m_status(UnavailableStatus)
    {};
	virtual ~FrameProcessor(void) = 0;

    virtual void setState(State) = 0;
    virtual State state() const { return m_state; }

    virtual Status status() const { return m_status; }

protected:
    void changeStatus(Status status)
    {
        if (m_status != status) {
#ifdef PRINT
			printf("----Status to %d\n", status);
#endif
            m_status = status;
            emit statusChanged(m_status);
        }
    }

    void changeState(State state) {
        if (m_state != state) {
#ifdef PRINT
			printf("****State to %d\n", state);
#endif
            m_state = state;
            emit stateChanged(m_state);
        }
    }

public slots:
    virtual void process(AnnotatedFrame) {};

signals:
    void stateChanged(FrameProcessor::State);
    void statusChanged(FrameProcessor::Status);
    void warning(FrameProcessor::Warning, const QString &);
    void error(FrameProcessor::Error, const QString &);
    void frameAvailable(AnnotatedFrame);

private:
    State m_state;
    Status m_status;

};

inline FrameProcessor::~FrameProcessor() {}

