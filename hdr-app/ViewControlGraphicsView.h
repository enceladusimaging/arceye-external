#pragma once
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QWidget>
#include <QWheelEvent>
#include <QMouseEvent>

class ViewControlGraphicsView : public QGraphicsView{

	Q_OBJECT

public:
	ViewControlGraphicsView(QWidget* parent) : QGraphicsView(parent){}

	void wheelEvent(QWheelEvent *e){ emit relayMouseWheelEvent(e); }
	void mouseMoveEvent(QMouseEvent* e){ emit relayMouseMoveEvent(e); }

signals:
	void relayMouseWheelEvent(QWheelEvent *e);
	void relayMouseMoveEvent(QMouseEvent *e);
};