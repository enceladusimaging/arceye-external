#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) 
	: QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Setup UI
    ui->setupUi(this);
}

void MainWindow::displayFrame(AnnotatedFrame frame)
{
	LogService::log(LogMessage("MainWindow::displayFrame", LogMessage::Type::Trace));

    bool flip = ui->actionFlipImage->isChecked();
    
    emit display(frame.getFrame(), flip);
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *e) {
	LogService::log(LogMessage("MainWindow::mouseDoubleClickEvent", LogMessage::Type::Trace));

    QWidget::mouseDoubleClickEvent(e);

    this->setWindowState(isFullScreen() ? Qt::WindowNoState : Qt::WindowFullScreen);
}

void MainWindow::recordStateChanged(FrameProcessor::State state)
{
	LogService::log(LogMessage("MainWindow::recordStateChanged", LogMessage::Type::Trace));

    ui->actionRecord->setChecked(state == FrameProcessor::ActiveState);
}

void MainWindow::recordStatusChanged(FrameProcessor::Status status)
{
	LogService::log(LogMessage("MainWindow::recordStatusChanged", LogMessage::Type::Trace));

    ui->actionRecord->setEnabled(status != FrameProcessor::UnloadingStatus);
}

void MainWindow::closeEvent(QCloseEvent * event)
{
	emit closed();
}
