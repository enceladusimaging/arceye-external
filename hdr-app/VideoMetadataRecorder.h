#pragma once

#include <QElapsedTimer>
#include <QObject>
#include <QString>
#include <QTextStream>
#include <QTime>

#include "Settings.h"

#include "../qt-logger/LogService.h"

class VideoMetadataRecorder : public QObject
{
    Q_OBJECT

public:
    VideoMetadataRecorder(const CameraSettings &, QObject * parent = 0);

    void setOutputPath(const QString &);
    
public slots:
    void recordStarted(const QString &);
    void recordStopped();
    void settingsChanged(const CameraSettings &);

private:
    QElapsedTimer m_recTimer;
    QString m_path, m_buf;
    QTextStream m_bufStream;
    CameraSettings m_settings;

    void printSettings(QTextStream &, const CameraSettings &);
    QString formattedTimestampFromMs(unsigned long);
};

