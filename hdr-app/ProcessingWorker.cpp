#include "ProcessingWorker.h"

void ProcessingWorker::processFrame(QImage frame, unsigned int shutter)
{
    // This thread holds reference to the image buffer through its QImage
	// Passed by value to this function
	QImage hdrFrame = hdrEnabled ? hdrProcessor->processFrame(frame, shutter) : frame;

	emit frameProcessed(hdrFrame);
}

void ProcessingWorker::hdrStateChanged(PointGreyOptions::HdrState state)
{
	hdrEnabled = (state == PointGreyOptions::HdrState::Enabled);
}
