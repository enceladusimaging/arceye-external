#include "CameraView.h"
#include "ui_CameraView.h"

CameraView::CameraView(QWidget *parent) 
    : QWidget(parent)
    , ui(new Ui::CameraView)
	, m_scene(this)
	, m_imageItem(new QGraphicsPixmapItem)
    , m_displayRefreshTimer(this)
    , m_ready(true)
{
    // Setup UI
    ui->setupUi(this);
    ui->frameView->setScene(&m_scene);

	//qApp->installEventFilter(this);

	//relay the ui control on frameView to CameraView's signal
	connect(ui->frameView, SIGNAL(relayMouseWheelEvent(QWheelEvent*)), this, SLOT(viewMouseWheelSlot(QWheelEvent*)), Qt::QueuedConnection);
	connect(ui->frameView, SIGNAL(relayMouseMoveEvent(QMouseEvent*)), this, SLOT(viewMouseMoveSlot(QMouseEvent*)), Qt::QueuedConnection);

	// frame item
    m_scene.addItem(m_imageItem);
    fitGraphicsItem(ui->frameView, m_imageItem);

    // start the display timer
    connect(&m_displayRefreshTimer, SIGNAL(timeout()), this, SLOT(displayFrame()), Qt::QueuedConnection);
    m_displayRefreshTimer.start(1.0/60.0 * 1000); //60 fps
}

void CameraView::resizeEvent(QResizeEvent * event)
{
	LogService::log(LogMessage("CameraView::resizeEvent", LogMessage::Type::Trace));

    fitGraphicsItem(ui->frameView, m_imageItem);
}

void CameraView::fitGraphicsItem(QGraphicsView * graphicsView, QGraphicsPixmapItem * imageItem)
{
	LogService::log(LogMessage("CameraView::fitGraphicsItem", LogMessage::Type::Trace));

    graphicsView->fitInView(imageItem, Qt::AspectRatioMode::KeepAspectRatio);
}

void CameraView::display(const QImage & frame, bool flipped)
{
	LogService::log(LogMessage("CameraView::display", LogMessage::Type::Trace));

    if (m_ready) {
        QImage f = flipped ? frame.mirrored(true, true) : frame.copy();

        m_imageItem->setPixmap(QPixmap::fromImage(f, Qt::NoFormatConversion));

        m_ready = false;
    }
}

void CameraView::displayFrame()
{
	LogService::log(LogMessage("CameraView::displayFrame", LogMessage::Type::Trace));

    m_ready = true;
}

bool CameraView::eventFilter(QObject *obj, QEvent *event)
{
	/*
	if (event->type() == QMouseEvent::GraphicsSceneMouseMove)
	{
		QMouseEvent* mouse_event = static_cast<QMouseEvent *>(event);
		emit viewMouseMoveSignal(mouse_event);
	}
	else if (event->type() == QWheelEvent::Wheel){
		QWheelEvent* wheel_event = static_cast<QWheelEvent *>(event);
		emit viewMouseWheelSignal(wheel_event);
	}
	*/
	return QObject::eventFilter(obj, event);
}

void CameraView::viewMouseWheelSlot(QWheelEvent* e){
	emit viewMouseWheelSignal(e);
}

void CameraView::viewMouseMoveSlot(QMouseEvent* e){
	emit viewMouseMoveSignal(e);
}
