#include "VideoMetadataRecorder.h"


VideoMetadataRecorder::VideoMetadataRecorder(const CameraSettings & initialSettings, QObject * parent)
    : QObject(parent)
    , m_path(), m_buf()
    , m_bufStream(&m_buf)
    , m_recTimer()
    , m_settings(initialSettings)
{
}

void VideoMetadataRecorder::recordStarted(const QString & videoFilePath)
{
	LogService::log(LogMessage("VideoMetadataRecorder::recordStarted", LogMessage::Type::Trace));

    m_path = videoFilePath + tr(".metadata");
    m_recTimer.restart();
    m_buf.clear();

    m_bufStream << tr("------------") << endl;
    m_bufStream << tr("Initial settings:") << endl;
    printSettings(m_bufStream, m_settings);
    m_bufStream << tr("------------") << endl;
    m_bufStream.flush();
}

void VideoMetadataRecorder::recordStopped()
{
	LogService::log(LogMessage("VideoMetadataRecorder::recordStopped", LogMessage::Type::Trace));

    m_bufStream << tr("------------") << endl;
    m_bufStream << tr("Recording stopped at ") << formattedTimestampFromMs(m_recTimer.elapsed()) << endl;
    m_bufStream << tr("------------") << endl;
    m_bufStream.flush();

    QFile outFile(m_path);
    QTextStream out(&outFile);
    if (outFile.open(QIODevice::WriteOnly)) {
        out << m_buf;    
        out.flush();

        outFile.close();
    }
}

void VideoMetadataRecorder::settingsChanged(const CameraSettings & settings)
{
	LogService::log(LogMessage("VideoMetadataRecorder::settingsChanged", LogMessage::Type::Trace));

    m_settings = settings;

    m_bufStream << tr("------------") << endl;
    m_bufStream << tr("Settings changed at ") << formattedTimestampFromMs(m_recTimer.elapsed()) << endl;
    printSettings(m_bufStream, m_settings);
    m_bufStream << tr("------------") << endl;
    m_bufStream.flush();
}

void VideoMetadataRecorder::printSettings(QTextStream & s, const CameraSettings & settings)
{
	LogService::log(LogMessage("VideoMetadataRecorder::printSettings", LogMessage::Type::Trace));

    s << "---Shutter Set Mode---" << endl;
    s << "Initial shutter: " << settings.initialShutter << endl;
    s << "Delta EV: " << settings.deltaEv << endl;
    s << "---End Shutter Set Mode---" << endl;

    s << "---Basic Settings---" << endl;
    s << "frame rate: " << settings.basicSettings.frameRate << endl;
    s << "gain: " << settings.basicSettings.gain << endl;
    s << "shutter: " << settings.basicSettings.shutter << endl;
    s << "---End Basic Settings---" << endl;
    s << endl;

    s << "---Video Mode Settings---" << endl;
    s << "width: " << settings.videoMode.width << endl;
    s << "height: " << settings.videoMode.height << endl;
    s << "left: " << settings.videoMode.left << endl;
    s << "top: " << settings.videoMode.top << endl;
    s << "mode: " << settings.videoMode.mode << endl;
    s << "pixelFormat: " << settings.videoMode.pixelFormat << endl;
    s << "---End Video Mode Settings---" << endl;
    s << endl;

    s << "---Advanced Settings---" << endl;
    s << "embeddedBrightness: " << settings.advancedSettings.embeddedBrightness << endl;
    s << "embeddedExposure: " << settings.advancedSettings.embeddedExposure << endl;
    s << "embeddedFrameCnt: " << settings.advancedSettings.embeddedFrameCnt << endl;
    s << "embeddedGain: " << settings.advancedSettings.embeddedGain << endl;
    s << "embeddedGpioPinState: " << settings.advancedSettings.embeddedGpioPinState << endl;
    s << "embeddedRoiPos: " << settings.advancedSettings.embeddedRoiPos << endl;
    s << "embeddedShutter: " << settings.advancedSettings.embeddedShutter << endl;
    s << "embeddedStrobe: " << settings.advancedSettings.embeddedStrobe << endl;
    s << "embeddedTimestamp: " << settings.advancedSettings.embeddedTimestamp << endl;
    s << "embeddedWhiteBal: " << settings.advancedSettings.embeddedWhiteBal << endl;
    s << "---End Advanced Settings---" << endl;
    s << endl;

    s << "---Triggered Capture Settings---" << endl;
    s << "Enabled: " << settings.triggerSettings.triggeredCapture.enabled << endl;
    s << "Delay enabled: " << settings.triggerSettings.triggeredCapture.delayEnabled << endl;
    s << "Mode: " << settings.triggerSettings.triggeredCapture.mode << endl;
    s << "Param: " << settings.triggerSettings.triggeredCapture.param << endl;
    s << "Polarity: " << settings.triggerSettings.triggeredCapture.polarity << endl;
    s << "Source: " << settings.triggerSettings.triggeredCapture.source << endl;
    s << "Delay (s): " << settings.triggerSettings.triggeredCapture.delayS << endl;
    s << "---End Trigger Settings---" << endl;
    s << endl;

    s << "---Strobe Settings---" << endl;

    foreach(TriggerStrobe ts, settings.triggerSettings.strobeSettings) {
        s << endl;
        s << "Enabled: " << ts.enabled << endl;
        s << "GPIO Pin: " << ts.source << endl;
        s << "Polarity: " << ts.polarity << endl;
        s << "Delay (ms): " << ts.delayMs << endl;
        s << "Duration (ms): " << ts.durationMs << endl;
        s << endl;
    }
    s << "---End Strobe Settings---" << endl;
    s << endl;

    s << "---HDR Settings---" << endl;
    s << "Enabled: " << settings.hdrSettings.enabled << endl;
    s << "Shutters: ";
    foreach(unsigned int shutter, settings.hdrSettings.shutters) {
         s << shutter << " ";
    }
    s << endl;

    s << "Gains: ";
    foreach(unsigned int gain, settings.hdrSettings.gains) {
         s << gain << " ";
    }
    s << endl;
    s << "---End Hdr Settings---" << endl;
    s << endl;
}

QString VideoMetadataRecorder::formattedTimestampFromMs(unsigned long ms)
{
	LogService::log(LogMessage("VideoMetadataRecorder::formattedTimestampFromMs", LogMessage::Type::Trace));

    return QTime::fromMSecsSinceStartOfDay(ms).toString("hh:mm:ss.zzz");
}
