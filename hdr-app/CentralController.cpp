#include "CentralController.h"

CentralController::CentralController(QObject *parent)
	: QObject(parent)
	, m_busManager(new CameraBusManager(this))
	, m_camera1Serial(tr("17088637"))
	, m_camera2Serial(tr("17105597"))
	, m_settingsManager1(new SettingsManager(tr("profiles1.json"), m_camera1Serial, SettingsManager::SONY, this))
	, m_settingsManager2(new SettingsManager(tr("profiles2.json"), m_camera2Serial, SettingsManager::SONY, this))
	, m_authenticator1(new Crypto(tr("profiles1"), this))
	, m_authenticator2(new Crypto(tr("profiles2"), this))
	, m_mainController1(new MainController(m_settingsManager1))
	, m_mainController2(new MainController(m_settingsManager2))
	, m_cameraMap(new QMap<unsigned int, MainController *>())
{
	connect(m_busManager, SIGNAL(cameraConnected(unsigned int, FlyCapture2::PGRGuid)), this, SLOT(cameraConnectedUsb(unsigned int, FlyCapture2::PGRGuid)));
	connect(m_busManager, SIGNAL(cameraDisconnected(unsigned int)), this, SLOT(cameraDisconnectedUsb(unsigned int)));
	connect(m_busManager, SIGNAL(error(CameraBusManager::Error, const QString &)), this, SLOT(busManagerError(CameraBusManager::Error, const QString &)));

	m_authenticator1->createHashStorageFile(m_camera1Serial);
	m_authenticator2->createHashStorageFile(m_camera2Serial);

	m_errorMsgbox.showMessage(tr("Please connect a camera to start."));

	m_busManager->discoverCameras();
}

CentralController::~CentralController()
{

}

void CentralController::cameraConnectedUsb(unsigned int serialNumber, FlyCapture2::PGRGuid guid)
{
	m_errorMsgbox.close();

	int desiredSerial1 = m_camera1Serial.toInt();
	int desiredSerial2 = m_camera2Serial.toInt();

	bool camera1Authenticated = m_authenticator1->authenticate(QString::number(serialNumber));
	bool camera2Authenticated = m_authenticator2->authenticate(QString::number(serialNumber));

	bool camera1Connected = (camera1Authenticated);
	bool camera2Connected = (camera2Authenticated);
	bool oneCamera = ((desiredSerial1 == desiredSerial2) && camera1Authenticated);

	if (oneCamera || camera1Connected) {
		if (m_mainController1) {
			m_mainController1->show(guid);
			m_mainController1->changeUsbConnectionStatus(true);
			m_cameraMap->insert(serialNumber, m_mainController1);
		}
	}
	else if (camera2Connected) {
		if (m_mainController2) {
			m_mainController2->show(guid);
			m_mainController2->changeUsbConnectionStatus(true);
			m_cameraMap->insert(serialNumber, m_mainController2);
		}
	}
	else {
#ifdef BYPASS_MODE
		m_mainController1->show(guid);
		m_mainController1->changeUsbConnectionStatus(false);
		m_cameraMap->insert(serialNumber, m_mainController1);
#else
		QString serial1 = QString::number(desiredSerial1);
		QString serial2 = QString::number(desiredSerial2);
		m_errorMsgbox.showMessage(tr("Please connect camera with serial number ") + serial1 + tr(" or ") + serial2 + tr(" to start."));
#endif
	}
}

void CentralController::cameraDisconnectedUsb(unsigned int serialNumber)
{
	if (m_cameraMap->contains(serialNumber)) {
		MainController * mc = m_cameraMap->find(serialNumber).value();

		if (mc->isRunning()) mc->stop();
		if (mc->settingsView()->isVisible()) mc->settingsView()->close();
		mc->usbConnectionChanged(false);
	}
}

void CentralController::busManagerError(CameraBusManager::Error, const QString & msg)
{
	m_errorMsgbox.showMessage(msg);
}
