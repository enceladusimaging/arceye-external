#pragma once

#include "CaptureProcessor.h"
#include "Settings.h"

#include "../qt-logger/LogService.h"

#include <QLabel>
#include <QWidget>

namespace Ui {
    class MainWindowStatusBar;
}

class MainWindowStatusBar : public QWidget
{
    Q_OBJECT

public:
    MainWindowStatusBar(CaptureProcessor * cap, const CameraSettings &, QWidget * parent = 0);

public slots:
    void updateFromCameraSettings(const CameraSettings &);
    void displayWarning(const QString &);
    void displayError(const QString &);
	void updateFromCameraTemperature(double);

private:
    enum LedState { Critical, Warning, Normal };

	void message(const QString &, QLabel *, LedState = LedState::Normal);

    Ui::MainWindowStatusBar * ui;
    CaptureProcessor * m_cap;
    QTimer * m_clearErrorTimer;

private slots:
	void receiveLogMessage(LogMessage);
	void clearError();
};

