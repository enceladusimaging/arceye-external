#pragma once

#define _VARIADIC_MAX 10

// STD Lib
#include <exception>
#include <memory>
#include <functional> //for std::greater<> sort predicate

// Qt
#include <QDebug>
#include <QObject>

// qt-logger
#include "../qt-logger/LogService.h"

// Local
#include "hdrDllExports.h"
#include "HdrContext.h"
#include "hdr-table.h"

#include "ActiveQThread.h"
#include "AnnotatedFrame.h"
#include "ConcurrentQueue.h"
#include "FrameProcessor.h"
#include "Settings.h"

namespace Hdr {
    class HdrProcessor;
}

class HdrProcessor : public FrameProcessor {

    Q_OBJECT

public:
    static HdrProcessor * createHdr(const HdrSettings &, QObject * = 0);

    void setState(FrameProcessor::State state);

	const float MinValue = 1.0e-4f;
	const float MaxValue = 1.0f - MinValue;

public slots:
    void process(AnnotatedFrame);
    void settingsChanged(const HdrSettings &);

private:
    explicit HdrProcessor(const HdrSettings &, HdrContext, QObject * parent = 0);

    void applySettings();

    void stopHdr();
    bool openHdr();
    void closeHdr();

    static const unsigned int FRAME_BUFFER_CAPACITY = 10;

    // HDR Processor-related members
    HdrSettings m_settings;

    int m_width, m_height, m_numFrames, m_colChannels;

    QVector<unsigned int> m_shutters;
	QVector<unsigned int> m_gains;
    double m_expPref;

    HdrContext m_context;

    std::unique_ptr<ActiveQThread> m_worker;

    void processAndReturn(HdrContext *, AnnotatedFrame, unsigned int);
    void shutdownGpu();
};
